
$(document).ready(function () {
  $('.home-carousel').owlCarousel({
    loop: true,
    margin: 0,
    nav: false,
    items: 1
  });

  $('.video-link').magnificPopup({
    removalDelay: 300,
    mainClass: 'mfp-fade',
    type: 'iframe',
    iframe: {
      patterns: {
        youtube: {
          index: 'youtube.com/',
          id: function (url) {
            var m = url.match(/[\\?\\&]v=([^\\?\\&]+)/);
            if (!m || !m[1]) return null;
            return m[1];
          },
          src: '//www.youtube.com/embed/%id%?autoplay=1'
        },
        vimeo: {
          index: 'vimeo.com/',
          id: function (url) {
            var m = url.match(/(https?:\/\/)?(www.)?(player.)?vimeo.com\/([a-z]*\/)*([0-9]{6,11})[?]?.*/);
            if (!m || !m[5]) return null;
            return m[5];
          },
          src: '//player.vimeo.com/video/%id%?autoplay=1'
        }
      }
    }
  });

  $('.photo-link').magnificPopup({
    removalDelay: 300,
    mainClass: 'mfp-fade',
    type: 'image',
    image: {
      markup: '<div class="mfp-figure">' +
        '<div class="mfp-close"></div>' +
        '<div class="mfp-img"></div>' +
        '<div class="mfp-bottom-bar">' +
        '<div class="mfp-title"></div>' +
        '<div class="mfp-counter"></div>' +
        '</div>' +
        '</div>', // Popup HTML markup. `.mfp-img` div will be replaced with img tag, `.mfp-close` by close button

      cursor: 'mfp-zoom-out-cur', // Class that adds zoom cursor, will be added to body. Set to null to disable zoom out cursor.

      titleSrc: 'title',

      verticalFit: true, // Fits image in area vertically

      tError: '<a href="%url%">The image</a> could not be loaded.' // Error message
    }
  });

  // search
  $('.search-btn').click(function () {
    $(this).fadeOut(100, function () {
      $(this).removeClass('show');
      $('.nav').hide();
    });
    $('.close-btn').fadeIn(300, function () {
      $('.close-btn').addClass('show');
      $('.search-section').addClass('show');
    });
  }); 

  $('.close-btn').click(function(){
    $(this).fadeOut(100, function () {
      $(this).removeClass('show');
      $('.search-section').removeClass('show');
    });
    $('.search-btn').fadeIn(300, function () {
      $('.nav').show();      
      $('.search-btn').addClass('show');      
    });
  });

  var wow = new WOW(
    {
      boxClass:     'wow',      // animated element css class (default is wow)
      animateClass: 'animated', // animation css class (default is animated)
      offset:       0,          // distance to the element when triggering the animation (default is 0)
      mobile:       true,       // trigger animations on mobile devices (default is true)
      live:         true,       // act on asynchronously loaded content (default is true)
      callback:     function(box) {
        // the callback is fired every time an animation is started
        // the argument that is passed in is the DOM node being animated
      },
      scrollContainer: null,    // optional scroll container selector, otherwise use window,
      resetAnimation: true,     // reset animation on end (default is true)
    }
  );
  wow.init();

  $('.drop').click(function(){
    if($(this).find('.dropdownContain').is('.mobile-dropdown')){
      $(this).find('.dropdownContain').removeClass('mobile-dropdown');      
    }else{
      $('.dropdownContain').removeClass('mobile-dropdown');
      $(this).find('.dropdownContain').addClass('mobile-dropdown');
    }
  });

  $('.navbar-toggle').click(function(){
    console.log('sa');
  });

});

