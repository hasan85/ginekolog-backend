# Generated by Django 2.0.1 on 2018-03-06 04:19

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ginekolog_app', '0006_remove_menubaseitems_item_url'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='menubaseitems',
            options={'ordering': ('id',), 'verbose_name': 'Header base menu item', 'verbose_name_plural': 'Header base menu items'},
        ),
        migrations.RemoveField(
            model_name='menusubitems',
            name='has_own_child',
        ),
    ]
