# Generated by Django 2.0.1 on 2018-03-08 02:01

import ckeditor_uploader.fields
from django.db import migrations, models
import django.db.models.deletion
import ginekolog_app.tools.custom_tools


class Migration(migrations.Migration):

    dependencies = [
        ('ginekolog_app', '0021_countingservices_more'),
    ]

    operations = [
        migrations.CreateModel(
            name='Picture',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255, verbose_name='Picture title')),
                ('description', ckeditor_uploader.fields.RichTextUploadingField(default='', verbose_name='Picture description')),
                ('image', models.ImageField(upload_to=ginekolog_app.tools.custom_tools.get_picture_path, verbose_name='Image file')),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('category_belongs_to', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ginekolog_app.Category', verbose_name='Belongs to category ')),
                ('doctor_belongs_to', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ginekolog_app.Doctors', verbose_name='Belongs to DR. ')),
            ],
            options={
                'verbose_name': 'Picture',
                'ordering': ('id',),
                'verbose_name_plural': 'Pictures',
            },
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255, verbose_name='Post title')),
                ('content', ckeditor_uploader.fields.RichTextUploadingField(default='', verbose_name='Post description')),
                ('cover', models.ImageField(blank=True, null=True, upload_to=ginekolog_app.tools.custom_tools.get_post_cover_path, verbose_name='Post cover')),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ginekolog_app.Doctors', verbose_name='Author DR. ')),
                ('category', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ginekolog_app.Category', verbose_name='Category')),
            ],
            options={
                'verbose_name': 'Post',
                'ordering': ('id',),
                'verbose_name_plural': 'Posts',
            },
        ),
    ]
