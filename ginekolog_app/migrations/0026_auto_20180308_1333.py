# Generated by Django 2.0.1 on 2018-03-08 13:33

from django.db import migrations, models
import django.db.models.deletion
import ginekolog_app.tools.custom_tools


class Migration(migrations.Migration):

    dependencies = [
        ('flatpages', '0001_initial'),
        ('ginekolog_app', '0025_auto_20180308_1308'),
    ]

    operations = [
        migrations.CreateModel(
            name='FlatpageImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(blank=True, null=True, upload_to=ginekolog_app.tools.custom_tools.get_flatpage_cover_path)),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='FooterItems',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=128, verbose_name='Menu title')),
                ('order', models.IntegerField(default=0, verbose_name='Menu order')),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
            ],
            options={
                'verbose_name_plural': 'Footer menu items',
                'ordering': ('-order',),
                'verbose_name': 'Footer menu item',
            },
        ),
        migrations.CreateModel(
            name='MyFlatPage',
            fields=[
                ('flatpage_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='flatpages.FlatPage')),
                ('cover', models.ImageField(upload_to=ginekolog_app.tools.custom_tools.get_flatpage_cover_path, verbose_name='Page cover')),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('updated_at', models.DateTimeField(auto_now=True, null=True)),
            ],
            options={
                'verbose_name_plural': 'Quick pages',
                'ordering': ('-id',),
                'verbose_name': 'Quick page',
            },
            bases=('flatpages.flatpage',),
        ),
        migrations.AddField(
            model_name='flatpageimage',
            name='flat_page',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ginekolog_app.MyFlatPage'),
        ),
    ]
