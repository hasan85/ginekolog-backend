# Generated by Django 2.0.1 on 2018-03-09 11:48

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ginekolog_app', '0029_auto_20180308_1514'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='menubaseitems',
            options={'ordering': ('order',), 'verbose_name': 'Header base-menu item', 'verbose_name_plural': 'Header base-menu items'},
        ),
        migrations.AlterModelOptions(
            name='menusubitems',
            options={'ordering': ('order',), 'verbose_name': 'Header sub-menu item', 'verbose_name_plural': 'Header sub-menu items'},
        ),
        migrations.AddField(
            model_name='doctors',
            name='description',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Short description'),
        ),
        migrations.AddField(
            model_name='doctors',
            name='work_grafic',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Work days'),
        ),
        migrations.AlterField(
            model_name='menusubitems',
            name='parent_sub_item',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='ginekolog_app.MenuSubItems', verbose_name='Parent menu'),
        ),
    ]
