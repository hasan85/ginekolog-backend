from django.db import models
from time import time


class IntegerRangeField(models.IntegerField):
    def __init__(self, verbose_name=None, name=None, min_value=None, max_value=None, **kwargs):
        self.min_value, self.max_value = min_value, max_value
        models.IntegerField.__init__(self, verbose_name, name, **kwargs)

    def formfield(self, **kwargs):
        defaults = {'min_value': self.min_value, 'max_value': self.max_value}
        defaults.update(kwargs)
        return super(IntegerRangeField, self).formfield(**defaults)



def get_doctor_image_path(instance, filename):
    return "doctors/%s_%s" % (str(time()).replace('.', '_'), filename.replace(' ', '_'))


def get_service_icon_path(instance, filename):
    return "icons/%s_%s" % (str(time()).replace('.', '_'), filename.replace(' ', '_'))


def get_picture_path(instance, filename):
    return "picture/%s_%s" % (str(time()).replace('.', '_'), filename.replace(' ', '_'))


def get_post_cover_path(instance, filename):
    return "post/%s_%s" % (str(time()).replace('.', '_'), filename.replace(' ', '_'))


def get_flatpage_cover_path(instance, filename):
    return "flatpage/%s_%s" % (str(time()).replace('.', '_'), filename.replace(' ', '_'))


def get_page_cover_path(instance, filename):
    return "page_covers/%s_%s" % (str(time()).replace('.', '_'), filename.replace(' ', '_'))


def slugify(title):
    symbol_mapping = (
        (' ', '-'),
        ('.', '-'),
        (',', '-'),
        ('!', '-'),
        ('?', '-'),
        ("'", '-'),
        ('"', '-'),
        ('ə', 'e'),
        ('ı', 'i'),
        ('ö', 'o'),
        ('ğ', 'g'),
        ('ü', 'u'),
        ('ş', 's'),
        ('ç', 'c'),
    )

    title_url = title.strip().lower()

    for before, after in symbol_mapping:
        title_url = title_url.replace(before, after)

    return title_url
