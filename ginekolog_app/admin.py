from django.contrib import admin
from ginekolog_app.models import Doctors, MenuBaseItems, MenuSubItems, Services, CountingServices, \
    SocialNetworkAddresses, Video, Category, Picture, Post, MyFlatPage, FlatpageImage, VideoPage, \
    PicturePage, PostPage, Slider
from .forms import FlatpageFormNew
from django.contrib.flatpages.admin import FlatPageAdmin
from django.utils.translation import gettext_lazy as _


class SocialNetworkAddressesAdmin(admin.TabularInline):
    model = SocialNetworkAddresses
    extra = 1
    fields = ('title', 'url', 'order',)


class DoctorsAdmin(admin.ModelAdmin):
    readonly_fields = ('slug',)
    list_display = ('get_image_thumbnail', 'name', 'surname', 'slug',)
    fields = ('name', 'surname', 'profession', 'phone_number', 'email', 'address',
              'description', 'work_grafic', 'profile_image', 'about', 'status',)
    inlines = [SocialNetworkAddressesAdmin]


class MenuSubItemsAdmin(admin.TabularInline):
    model = MenuSubItems
    extra = 1
    fields = ('title', 'url', 'base_header_item', 'parent_sub_item', 'order',)


class MenuBaseItemsAdmin(admin.ModelAdmin):
    list_display = ('title',)
    fields = ('title', 'order',)
    inlines = [MenuSubItemsAdmin]


class ServicesAdmin(admin.ModelAdmin):
    list_display = ('title', 'content',)
    fields = ('title', 'content', 'icon',)


class CountingServicesAdmin(admin.ModelAdmin):
    list_display = ('title', 'count',)
    fields = ('title', 'count', 'more', 'icon')


class VideoAdmin(admin.ModelAdmin):
    list_display = ('get_video_thumb_admin', 'title', 'category_belongs_to')
    fields = ('title', 'description', 'url', 'doctor_belongs_to', 'category_belongs_to')


class PictureAdmin(admin.ModelAdmin):
    list_display = ('get_picture_thumbnail_url', 'title', 'category_belongs_to')
    fields = ('title', 'description', 'image', 'doctor_belongs_to', 'category_belongs_to')


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('title', 'get_description', 'slug')
    fields = ('title', 'description', 'order',)


class PostAdmin(admin.ModelAdmin):
    list_display = ('get_post_cover_thumb', 'title', 'category')
    fields = ('title', 'content', 'cover', 'author', 'category')


class FlatpageImagesAdmin(admin.TabularInline):
    model = FlatpageImage
    extra = 1


class CustomFlatpageAdmin(FlatPageAdmin):
    form = FlatpageFormNew
    inlines = [FlatpageImagesAdmin]
    fieldsets = (
        (None, {'fields': ('title', 'url', 'cover', 'content', 'sites')}),
        (_('Advanced options'), {
            'classes': ('collapse',),
            'fields': ('registration_required', 'template_name'),
        }),
    )


class VideoPageAdmin(admin.ModelAdmin):
    list_display = ['get_video_page_cover_thumb', ]
    fields = ['cover', 'description', ]


class PicturePageAdmin(admin.ModelAdmin):
    list_display = ['get_picture_page_cover_thumb', ]
    fields = ['cover', 'description', ]


class PostPageAdmin(admin.ModelAdmin):
    list_display = ['get_post_page_cover_thumb', ]
    fields = ['cover', 'description', ]


class SliderAdmin(admin.ModelAdmin):
    list_display = ['get_slider_cover', ]


admin.site.register(Doctors, DoctorsAdmin)
admin.site.register(MenuBaseItems, MenuBaseItemsAdmin)
admin.site.register(Services, ServicesAdmin)
admin.site.register(CountingServices, CountingServicesAdmin)
admin.site.register(Video, VideoAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Picture, PictureAdmin)
admin.site.register(Post, PostAdmin)
admin.site.register(MyFlatPage, CustomFlatpageAdmin)
admin.site.register(VideoPage, VideoPageAdmin)
admin.site.register(PicturePage, PicturePageAdmin)
admin.site.register(PostPage, PostPageAdmin)
admin.site.register(Slider, SliderAdmin)
