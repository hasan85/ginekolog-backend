from django.contrib.flatpages.views import render_flatpage
from django.contrib.sites.shortcuts import get_current_site
from django.db.models import Q
from django.http import Http404, HttpResponsePermanentRedirect
from django.shortcuts import get_object_or_404, render
from django.views import generic
from ginekolog_app.models import Doctors, MenuBaseItems, Services, CountingServices, \
    Video, Picture, Post, MyFlatPage, Category, VideoPage, PicturePage, PostPage, Slider


class BaseContext(generic.View):

    def get_context_data(self, **kwargs):
        context = super(BaseContext, self).get_context_data(**kwargs)
        context["doctors"] = Doctors.objects.all()
        context["sliders"] = Slider.objects.all()
        context["header_menu"] = MenuBaseItems.objects.all()
        context["services"] = Services.objects.all()
        context["c_services"] = CountingServices.objects.all()
        context["videos"] = Video.objects.all()
        context["pictures"] = Picture.objects.all()
        context["posts"] = Post.objects.all()
        return context


class BaseIndexView(BaseContext, generic.TemplateView):
    template_name = 'Base/index.html'


# overrided flatpage view
def flatpage(request, url):
    """
    Public interface to the flat page view.

    Models: `flatpages.flatpages`
    Templates: Uses the template defined by the ``template_name`` field,
        or :template:`flatpages/default.html` if template_name is not defined.
    Context:
        flatpage
            `flatpages.flatpages` object
    """
    if not url.startswith('/'):
        url = '/' + url
    site_id = get_current_site(request).id
    try:
        f = get_object_or_404(MyFlatPage, url=url, sites=site_id)
    except Http404:
        if not url.endswith('/') and settings.APPEND_SLASH:
            url += '/'
            f = get_object_or_404(MyFlatPage, url=url, sites=site_id)
            return HttpResponsePermanentRedirect('%s/' % request.path)
        else:
            raise
    return render_flatpage(request, f)


class AboutPageView(generic.DetailView):
    template_name = 'Pages/about/index.html'
    model = Doctors
    context_object_name = "doctor"

    def get_queryset(self):
        return self.model.objects.filter(status=True)


class VideoListView(generic.ListView):
    template_name = 'Pages/videos_list/index.html'
    model = Video
    context_object_name = "videos"
    paginate_by = 6

    def get_queryset(self):
        category_slug = self.request.GET.get('category', "all")
        if category_slug == "all":
            videos = self.model.objects.all()
        else:
            category = Category.objects.filter(slug=category_slug).last()
            videos = self.model.objects.filter(category_belongs_to=category)
        return videos

    def get_context_data(self, **kwargs):
        context = super(VideoListView, self).get_context_data(**kwargs)
        context["categories"] = Category.objects.all()
        context["page_options"] = VideoPage.objects.first()
        return context


class VideoDetailView(generic.DetailView):
    model = Video
    template_name = "Pages/video-detail/index.html"
    context_object_name = "video"

    def get_queryset(self):
        category_slug = self.request.GET.get('category', "all")
        if category_slug == "all":
            videos = self.model.objects.all()
        else:
            category = Category.objects.filter(slug=category_slug).last()
            videos = self.model.objects.filter(category_belongs_to=category)
        return videos

    def get_context_data(self, **kwargs):
        context = super(VideoDetailView, self).get_context_data(**kwargs)
        context["categories"] = Category.objects.all()
        context["page_options"] = VideoPage.objects.first()
        return context


class PictureListView(generic.ListView):
    template_name = 'Pages/pictures_list/index.html'
    model = Picture
    context_object_name = "pictures"
    paginate_by = 6

    def get_queryset(self):
        category_slug = self.request.GET.get('category', "all")
        if category_slug == "all":
            pictures = self.model.objects.all()
        else:
            category = Category.objects.filter(slug=category_slug).last()
            pictures = self.model.objects.filter(category_belongs_to=category)
        return pictures

    def get_context_data(self, **kwargs):
        context = super(PictureListView, self).get_context_data(**kwargs)
        context["categories"] = Category.objects.all()
        context["page_options"] = PicturePage.objects.first()
        return context


class PictureDetailView(generic.DetailView):
    model = Picture
    template_name = "Pages/picture-detail/index.html"
    context_object_name = "picture"

    def get_queryset(self):
        category_slug = self.request.GET.get('category', "all")
        if category_slug == "all":
            pictures = self.model.objects.all()
        else:
            category = Category.objects.filter(slug=category_slug).last()
            pictures = self.model.objects.filter(category_belongs_to=category)
        return pictures

    def get_context_data(self, **kwargs):
        context = super(PictureDetailView, self).get_context_data(**kwargs)
        context["categories"] = Category.objects.all()
        context["page_options"] = PicturePage.objects.first()
        return context


class PostListView(generic.ListView):
    template_name = 'Pages/posts_list/index.html'
    model = Post
    context_object_name = "posts"
    paginate_by = 6

    def get_queryset(self):
        category_slug = self.request.GET.get('category', "all")
        if category_slug == "all":
            posts = self.model.objects.all()
        else:
            category = Category.objects.filter(slug=category_slug).last()
            posts = self.model.objects.filter(category=category)
        return posts

    def get_context_data(self, **kwargs):
        context = super(PostListView, self).get_context_data(**kwargs)
        context["categories"] = Category.objects.all()
        context["page_options"] = PostPage.objects.first()
        return context


class PostDetailView(generic.DetailView):
    model = Post
    template_name = "Pages/post-detail/index.html"
    context_object_name = "post"

    def get_queryset(self):
        category_slug = self.request.GET.get('category', "all")
        if category_slug == "all":
            posts = self.model.objects.all()
        else:
            category = Category.objects.filter(slug=category_slug).last()
            posts = self.model.objects.filter(category=category)
        return posts

    def get_context_data(self, **kwargs):
        context = super(PostDetailView, self).get_context_data(**kwargs)
        context["categories"] = Category.objects.all()
        context["page_options"] = PostPage.objects.first()
        return context


class SearchView(generic.TemplateView):
    template_name = "Pages/search/index.html"
    paginate_by = 8

    def get_queryset(self):
        category_query = self.request.GET.get('category')
        query = self.request.GET.get('q')

        context = super(SearchView, self).get_context_data()

        if not category_query:
            categories = list()
            category = dict()
            context["doctor_result"], category = self.search_doctor(query)
            if category:
                categories.append(category)

            context["flatpage_result"], category = self.search_flat(query)
            if category:
                categories.append(category)

            context["video_result"], category = self.search_video(query)
            if category:
                categories.append(category)

            context["picture_result"], category = self.search_picture(query)
            if category:
                categories.append(category)

            context["post_result"], category = self.search_post(query)
            if category:
                categories.append(category)

            search_results = 0
            search_results += context["doctor_result"].__len__()
            search_results += context["flatpage_result"].__len__()
            search_results += context["video_result"].__len__()
            search_results += context["picture_result"].__len__()
            search_results += context["post_result"].__len__()
            context["search_results"] = search_results
            context["categories"] = categories
        else:
            categories = list()
            categories = context["categories"]
            if category_query == "doctor":
                context["doctor_results"] = self.search_doctor(query)
            if category_query == "flat":
                context["flatpage_results"] = self.search_flat(query)
            if category_query == "video":
                context["video_results"] = self.search_video(query)
            if category_query == "picture":
                context["picture_results"] = self.search_picture(query)
            if category_query == "post":
                context["post_results"] = self.search_post(query)
        return context

    def search_doctor(self, query):
        result = Doctors.objects.filter(
            Q(name__icontains=query) |
            Q(surname__icontains=query) |
            Q(description__icontains=query) |
            Q(profession__icontains=query) |
            Q(about__icontains=query) |
            Q(email__icontains=query) |
            Q(address__icontains=query)
        )
        category = None
        if result:
            category = {"type": "doctor", "text": "Həkimlər"}
        return (result, category)

    def search_flat(self, query):
        result = MyFlatPage.objects.filter(Q(title__icontains=query) | Q(content__icontains=query))
        category = None
        if result:
            category = {"type": "flat", "text": "Səhifələr"}
        return (result, category)

    def search_video(self, query):
        result = Video.objects.filter(
            Q(title__icontains=query) |
            Q(description__icontains=query) |
            Q(category_belongs_to__title__icontains=query) |
            Q(category_belongs_to__description__icontains=query)
        )
        category = None
        if result:
            category = {"type": "video", "text": "Videolar"}
        return (result, category)

    def search_picture(self, query):
        result = Picture.objects.filter(
            Q(title__icontains=query) |
            Q(description__icontains=query) |
            Q(category_belongs_to__title__icontains=query) |
            Q(category_belongs_to__description__icontains=query))

        category = None
        if result:
            category = {"type": "picture", "text": "Şəkillər"}
        return (result, category)

    def search_post(self, query):
        result = Post.objects.filter(
            Q(title__icontains=query) |
            Q(content__icontains=query) |
            Q(category__title__icontains=query) |
            Q(category__description__icontains=query)
        )
        category = None
        if result:
            category = {"type": "post", "text": "Məqalələr"}
        return (result, category)

    def get_context_data(self, **kwargs):
        category_query = self.request.GET.get('category')
        query = self.request.GET.get('q')

        context = super(SearchView, self).get_context_data()

        if not category_query:
            categories = list()
            category = dict()
            context["doctor_result"], category = self.search_doctor(query)
            if category:
                categories.append(category)

            context["flatpage_result"], category = self.search_flat(query)
            if category:
                categories.append(category)

            context["video_result"], category = self.search_video(query)
            if category:
                categories.append(category)

            context["picture_result"], category = self.search_picture(query)
            if category:
                categories.append(category)

            context["post_result"], category = self.search_post(query)
            if category:
                categories.append(category)

        else:
            categories = list()
            category = dict()
            doctor_result, category = self.search_doctor(query)
            if category:
                categories.append(category)
            flatpage_result, category = self.search_flat(query)
            if category:
                categories.append(category)
            video_result, category = self.search_video(query)
            if category:
                categories.append(category)
            picture_result, category = self.search_picture(query)
            if category:
                categories.append(category)
            post_result, category = self.search_post(query)
            if category:
                categories.append(category)

            if category_query == "doctor":
                context["doctor_result"] = doctor_result
            elif category_query == "flat":
                context["flatpage_result"] = flatpage_result
            elif category_query == "video":
                context["video_result"] = video_result
            elif category_query == "picture":
                context["picture_result"] = picture_result
            elif category_query == "post":
                context["post_result"] = post_result
        search_results = 0
        search_results += context.get("doctor_result").__len__() if context.get("doctor_result") else 0
        search_results += context.get("flatpage_result").__len__() if context.get("flatpage_result") else 0
        search_results += context.get("video_result").__len__() if context.get("video_result") else 0
        search_results += context.get("picture_result").__len__() if context.get("picture_result") else 0
        search_results += context.get("post_result").__len__() if context.get("post_result") else 0
        context["search_results"] = search_results
        context["categories"] = categories
        return context


# Error handling
def handle_404(request):
    context = dict()
    return render(request, 'Pages/404.html', context)

def handle_500(request):
    context = dict()
    return render(request, 'Pages/500.html', context)
