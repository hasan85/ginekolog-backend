from ckeditor_uploader.fields import RichTextUploadingField
from django.contrib.flatpages.models import FlatPage
from django.db import models
from django.urls import reverse
from django.utils.html import mark_safe
from .tools.custom_tools import get_doctor_image_path, slugify, get_service_icon_path, \
    get_picture_path, get_post_cover_path, get_flatpage_cover_path, get_page_cover_path


# doctors model
class Doctors(models.Model):
    name = models.CharField(max_length=128, verbose_name="Name")
    surname = models.CharField(max_length=128, verbose_name="Surname")
    profession = models.CharField(max_length=128, verbose_name="Profession")
    phone_number = models.CharField(max_length=20)
    email = models.EmailField(max_length=128, null=True, blank=True, verbose_name="Email")
    address = models.CharField(null=True, blank=True, max_length=255, verbose_name="Address")
    profile_image = models.ImageField(null=True, upload_to=get_doctor_image_path, blank=True,
                                      verbose_name="Profile Photo")
    about = RichTextUploadingField(default='')
    description = models.CharField(max_length=255, default="", verbose_name="Short description", null=True, blank=True)
    work_grafic = models.CharField(max_length=255, default="", verbose_name="Work days", null=True, blank=True)
    status = models.BooleanField(default=True)
    slug = models.SlugField(max_length=255, null=True, blank=True)
    order = models.IntegerField(default=0)

    # logs
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, null=True, blank=True)

    def __str__(self):
        return "Dr. %s %s" % (self.name, self.surname)

    def get_doctor_name(self):
        return "Dr. %s %s" % (self.name, self.surname)

    class Meta:
        ordering = ('order',)
        verbose_name = 'Doctor'
        verbose_name_plural = 'Doctors'

    def save(self, *args, **kwargs):
        super(Doctors, self).save(*args, **kwargs)
        self.slug = slugify("%s_%s" % (self.name, self.surname))
        super(Doctors, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('about', kwargs={'slug': self.slug})

    def get_image(self):
        if self.profile_image:
            return mark_safe("<img class='img-responsive' src='%s' alt="">" % self.profile_image.url)
        else:
            return mark_safe(
                "<img src='https://crestaproject.com/demo/nucleare-pro/wp-content/themes/nucleare-pro/images/no-image"
                "-box.png' alt='image' />")

    def get_image_thumbnail(self):
        if self.profile_image:
            return mark_safe("<img src='%s' style='width:300px;height:auto' alt='image' />" % self.profile_image.url)
        else:
            return mark_safe(
                "<img src='https://crestaproject.com/demo/nucleare-pro/wp-content/themes/nucleare-pro/images/no-image"
                "-box.png' alt='image' />")

    profile_image.short_description = "Profile Photo"
    profile_image.allow_tags = True


# Header menu base items model
class MenuBaseItems(models.Model):
    title = models.CharField(max_length=128, verbose_name="Menu title")
    order = models.IntegerField(default=0, verbose_name="Menu order")

    # logs
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, null=True, blank=True)

    class Meta:
        ordering = ('order',)
        verbose_name = 'Header base-menu item'
        verbose_name_plural = 'Header base-menu items'

    def __str__(self):
        return "%s" % self.title

    # return true if has child sub-menus
    def contain_dropdown(self):
        childCount = self.menusubitems_set.all().count()
        return True if childCount > 0 else False

    def get_childs(self):
        return self.menusubitems_set.all()


# Header menu sub-items model
class MenuSubItems(models.Model):
    title = models.CharField(max_length=128, verbose_name="Menu title")
    url = models.CharField(max_length=255, verbose_name="Redirect url")
    order = models.IntegerField(default=0, verbose_name="Menu order")
    base_header_item = models.ForeignKey("MenuBaseItems", null=True, blank=True, on_delete=models.CASCADE,
                                         verbose_name="Parent header menu")
    parent_sub_item = models.ForeignKey('self', null=True, blank=True, on_delete=models.CASCADE,
                                        verbose_name="Parent menu")

    # logs
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, null=True, blank=True)

    class Meta:
        ordering = ('order',)
        verbose_name = 'Header sub-menu item'
        verbose_name_plural = 'Header sub-menu items'

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        super(MenuSubItems, self).save(*args, **kwargs)
        if not self.url.startswith("/") or not self.url[::-1].startswith("/"):
            url = ""
            for item in self.url.split("/"):
                if item != '':
                    url += item + "/"
            self.url = (url[::-1] + "/")[::-1]
            super(MenuSubItems, self).save(*args, **kwargs)

    def has_own_child(self):
        count = MenuSubItems.objects.filter(parent_sub_item=self).count()
        return True if count > 0 else False

    def has_own_parent(self):
        return True if self.parent_sub_item else False

    def get_childs(self):
        return MenuSubItems.objects.filter(parent_sub_item=self)

    def get_absolute_url(self):
        # return reverse('about', kwargs={'slug': self.slug})
        pass


# Services model
class Services(models.Model):
    title = models.CharField(max_length=128, verbose_name="Service title")
    content = models.CharField(max_length=128, verbose_name="Service content")
    icon = models.FileField(null=True, blank=True, upload_to=get_service_icon_path, verbose_name="Icon")
    order = models.IntegerField(default=0, verbose_name="Service order")

    # logs
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, null=True, blank=True)

    class Meta:
        ordering = ('-order',)
        verbose_name = 'Service'
        verbose_name_plural = 'Services'

    def __str__(self):
        return "%s" % self.title


# Services which count is assending
class CountingServices(models.Model):
    title = models.CharField(max_length=128, verbose_name="Service title")
    icon = models.FileField(null=True, blank=True, upload_to=get_service_icon_path, verbose_name="Icon")
    count = models.IntegerField(default=0, verbose_name="Service count")
    more = models.BooleanField(default=False, verbose_name="And more")
    order = models.IntegerField(default=0, verbose_name="Service order")

    # logs
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, null=True, blank=True)

    class Meta:
        ordering = ('-order',)
        verbose_name = 'Services with count'
        verbose_name_plural = 'Services with count'

    def __str__(self):
        return "%s" % self.title


# social network addresses
class SocialNetworkAddresses(models.Model):
    title = models.CharField(max_length=255, verbose_name="Social network address")
    url = models.URLField(max_length=255, verbose_name="Social network url")
    doctor_belongs_to = models.ForeignKey('Doctors', on_delete=models.CASCADE, verbose_name="Belongs to doctor")
    order = models.IntegerField(default=0, verbose_name="Icon order")
    # logs
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, null=True, blank=True)

    class Meta:
        ordering = ('-order',)
        verbose_name = 'Social Network Address'
        verbose_name_plural = 'Social Network Addresses'

    def __str__(self):
        return "%s" % self.title


# Videos model
class Video(models.Model):
    title = models.CharField(max_length=255, verbose_name="Video title")
    slug = models.SlugField(null=True, blank=True, )
    description = RichTextUploadingField(default='', verbose_name="Video description")
    url = models.URLField(max_length=255, verbose_name="Youtube URL")
    doctor_belongs_to = models.ForeignKey('Doctors', on_delete=models.CASCADE, verbose_name="Belongs to DR. ")
    category_belongs_to = models.ForeignKey("Category", null=True, blank=True, on_delete=models.CASCADE,
                                            verbose_name="Belongs to category ")

    # logs
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, null=True, blank=True)

    def save(self, *args, **kwargs):
        super(Video, self).save(*args, **kwargs)
        self.slug = slugify("%s" % self.title)
        time_index = self.url.find('&t=')
        if time_index != -1:
            self.url = self.url[:time_index]
        super(Video, self).save(*args, **kwargs)

    class Meta:
        ordering = ('id',)
        verbose_name = 'Video'
        verbose_name_plural = 'Videos'

    def __str__(self):
        return "%s" % self.title

    def get_video_thumbnail_url(self):
        video_id_index = self.url.find("v=")
        video_id = self.url[ video_id_index+2:video_id_index+13 ]
        # https://i.ytimg.com/vi/YB2Gcpyjn1Q/hqdefault.jpg
        return "http://i3.ytimg.com/vi/%s/hqdefault.jpg" % (video_id)

    def get_video_thumb_admin(self):
        if self.url:
            video_id = self.url[::-1][:11]
            url = "http://i3.ytimg.com/vi/%s/hqdefault.jpg" % (video_id[::-1])
            return mark_safe("<img src='%s' style='width:300px;height:auto' alt='image' />" % url)
        else:
            return mark_safe(
                "<img src='https://crestaproject.com/demo/nucleare-pro/wp-content/themes/nucleare-pro/images/no-image"
                "-box.png' alt='image' />")

    def get_absolute_url(self):
        return reverse('video-detail', kwargs={'slug': self.slug})


# Category model
class Category(models.Model):
    title = models.CharField(max_length=255, verbose_name="Category title")
    description = RichTextUploadingField(default='', verbose_name="Category description")
    order = models.IntegerField(default=0, verbose_name="Category order")
    slug = models.SlugField(null=True, blank=True)

    # logs
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, null=True, blank=True)

    class Meta:
        ordering = ('-order',)
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'

    def __str__(self):
        return "%s" % self.title

    def save(self, *args, **kwargs):
        super(Category, self).save(*args, **kwargs)
        self.slug = slugify("%s" % self.title)
        super(Category, self).save(*args, **kwargs)

    def get_description(self):
        return mark_safe("<p>%s</p>" % self.description)

    description.allow_tags = True


# Picture model
class Picture(models.Model):
    title = models.CharField(max_length=255, verbose_name="Picture title")
    slug = models.SlugField(null=True, blank=True, )
    description = RichTextUploadingField(default='', verbose_name="Picture description")
    image = models.ImageField(upload_to=get_picture_path, verbose_name="Image file")
    doctor_belongs_to = models.ForeignKey('Doctors', on_delete=models.CASCADE, verbose_name="Belongs to DR. ")
    category_belongs_to = models.ForeignKey("Category", null=True, blank=True, on_delete=models.CASCADE,
                                            verbose_name="Belongs to category ")

    # logs
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, null=True, blank=True)

    class Meta:
        ordering = ('id',)
        verbose_name = 'Picture'
        verbose_name_plural = 'Pictures'

    def __str__(self):
        return "%s" % self.title

    def get_picture_thumbnail_url(self):
        if self.image.url:
            return mark_safe("<img src='%s' style='width:300px;height:auto' alt='image' />" % self.image.url)
        else:
            return mark_safe(
                "<img src='https://crestaproject.com/demo/nucleare-pro/wp-content/themes/nucleare-pro/images/no-image"
                "-box.png' alt='image' />")

    def save(self, *args, **kwargs):
        super(Picture, self).save(*args, **kwargs)
        self.slug = slugify("%s" % self.title)
        super(Picture, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('picture-detail', kwargs={'slug': self.slug})


# Post model
class Post(models.Model):
    title = models.CharField(max_length=255, verbose_name="Post title")
    slug = models.SlugField(null=True, blank=True, )
    content = RichTextUploadingField(default='', verbose_name="Post description")
    cover = models.ImageField(upload_to=get_post_cover_path, null=True, blank=True, verbose_name="Post cover")
    author = models.ForeignKey('Doctors', on_delete=models.CASCADE, verbose_name="Author DR. ")
    category = models.ForeignKey("Category", null=True, blank=True, on_delete=models.CASCADE, verbose_name="Category")

    # logs
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, null=True, blank=True)

    class Meta:
        ordering = ('id',)
        verbose_name = 'Post'
        verbose_name_plural = 'Posts'


    def __str__(self):
        return "%s" % self.title

    def get_absolute_url(self):
        return reverse('post-detail', kwargs={'slug': self.slug})

    def get_post_cover_thumb(self):
        if self.cover.url:
            return mark_safe("<img src='%s' style='width:300px;height:auto' alt='cover' />" % self.cover.url)
        else:
            return mark_safe(
                "<img src='https://crestaproject.com/demo/nucleare-pro/wp-content/themes/nucleare-pro/images/no-image"
                "-box.png' alt='image' />")

    def save(self, *args, **kwargs):
        super(Post, self).save(*args, **kwargs)
        self.slug = slugify("%s" % self.title)
        super(Post, self).save(*args, **kwargs)

    # def get_search_result(self):
    #     result = list()
    #     result.append(slugify(self.title))
    #     result.append(slugify(self.content))
    #     result.append(slugify(self.category.title))
    #     result.append(slugify(self.category.description))
    #     result.append(slugify(self.author.name))
    #     return result


# Override Flatpage model has ckEditor with image upload
class MyFlatPage(FlatPage):
    cover = models.ImageField(upload_to=get_flatpage_cover_path, verbose_name="Page cover")

    # logs
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, null=True, blank=True)

    class Meta:
        ordering = ("-id",)
        verbose_name = "Quick page"
        verbose_name_plural = "Quick pages"

    def __str__(self):
        return "%s" % self.title


# custom image model for flatpages
class FlatpageImage(models.Model):
    flat_page = models.ForeignKey('MyFlatPage', on_delete=models.CASCADE)
    image = models.ImageField(upload_to=get_flatpage_cover_path, null=True, blank=True)

    # logs
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, null=True, blank=True)


# Models for Page covers and texts
class VideoPage(models.Model):
    cover = models.ImageField(upload_to=get_page_cover_path, verbose_name="Page cover")
    description = models.CharField(max_length=255, verbose_name="Page description")

    # logs
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, null=True, blank=True)

    def __str__(self):
        return "%s" % self.description

    class Meta:
        ordering = ('-id',)
        verbose_name = 'Video Page Option'
        verbose_name_plural = 'Video Page Options'

    def get_video_page_cover_thumb(self):
        if self.cover.url:
            return mark_safe("<img src='%s' style='width:300px;height:auto' alt='cover' />" % self.cover.url)
        else:
            return mark_safe(
                "<img src='https://crestaproject.com/demo/nucleare-pro/wp-content/themes/nucleare-pro/images/no-image"
                "-box.png' alt='image' />")


class PicturePage(models.Model):
    cover = models.ImageField(upload_to=get_page_cover_path, verbose_name="Page cover")
    description = models.CharField(max_length=255, verbose_name="Page description")

    # logs
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, null=True, blank=True)

    def __str__(self):
        return "%s" % self.description

    class Meta:
        ordering = ('-id',)
        verbose_name = 'Picture Page Option'
        verbose_name_plural = 'Picture Page Options'

    def get_picture_page_cover_thumb(self):
        if self.cover.url:
            return mark_safe("<img src='%s' style='width:300px;height:auto' alt='cover' />" % self.cover.url)
        else:
            return mark_safe(
                "<img src='https://crestaproject.com/demo/nucleare-pro/wp-content/themes/nucleare-pro/images/no-image"
                "-box.png' alt='image' />")


class PostPage(models.Model):
    cover = models.ImageField(upload_to=get_page_cover_path, verbose_name="Page cover")
    description = models.CharField(max_length=255, verbose_name="Page description")

    # logs
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, null=True, blank=True)

    def __str__(self):
        return "%s" % self.description

    class Meta:
        ordering = ('-id',)
        verbose_name = 'Post Page Option'
        verbose_name_plural = 'Post Page Options'

    def get_post_page_cover_thumb(self):
        if self.cover.url:
            return mark_safe("<img src='%s' style='width:300px;height:auto' alt='cover' />" % self.cover.url)
        else:
            return mark_safe(
                "<img src='https://crestaproject.com/demo/nucleare-pro/wp-content/themes/nucleare-pro/images/no-image"
                "-box.png' alt='image' />")


class Slider(models.Model):
    title = models.CharField(max_length=255, verbose_name="Slider title")
    header = models.CharField(max_length=255, null=True, blank=True, verbose_name="Main header")
    sub_header = models.CharField(max_length=255, null=True, blank=True, verbose_name="Secondary header")
    description = models.TextField(max_length=512, null=True, blank=True, verbose_name="Description")
    button_text = models.CharField(max_length=255, null=True, blank=True, verbose_name="Button text")
    button_url = models.CharField(max_length=255, null=True, blank=True, verbose_name="Button url")
    slider_image = models.ImageField(upload_to=get_flatpage_cover_path, verbose_name="Slider cover")
    order = models.IntegerField(default=0, verbose_name="Slider order")

    # logs
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, null=True, blank=True)

    class Meta:
        ordering = ('order',)
        verbose_name = "Slider"
        verbose_name_plural = "Sliders"

    def __str__(self):
        return self.title

    def get_slider_cover(self):
        if self.slider_image:
            return mark_safe("<img src='%s' style='width:300px;height:auto' alt='image' />" % self.slider_image.url)
        else:
            return mark_safe(
                "<img src='https://crestaproject.com/demo/nucleare-pro/wp-content/themes/nucleare-pro/images/no-image"
                "-box.png' alt='image' />")

    slider_image.allow_tags = True