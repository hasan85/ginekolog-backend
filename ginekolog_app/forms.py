from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django.contrib.flatpages.forms import FlatpageForm

from .models import MyFlatPage


class FlatpageFormNew(FlatpageForm):
    class Meta:
        model = MyFlatPage
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        super(FlatpageFormNew, self).__init__(*args, **kwargs)
        self.fields['content'].widget = CKEditorUploadingWidget()

