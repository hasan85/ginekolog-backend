from django.apps import AppConfig

class GinekologAppConfig(AppConfig):
    name = 'ginekolog_app'
    verbose_name = 'DOCTOR SITE'
