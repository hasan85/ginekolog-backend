from django.urls import path

from ginekolog_app.views import BaseIndexView, AboutPageView, VideoListView, VideoDetailView, \
     PictureListView, PictureDetailView, PostListView, PostDetailView, SearchView

urlpatterns = [
    path('', BaseIndexView.as_view(), name='index'),
    path('about/<str:slug>/', AboutPageView.as_view(), name='about'),
    path('videos/', VideoListView.as_view(), name='videos'),
    path('video/<str:slug>/', VideoDetailView.as_view(), name='video-detail'),
    path('pictures/', PictureListView.as_view(), name='pictures'),
    path('picture/<str:slug>/', PictureDetailView.as_view(), name='picture-detail'),
    path('posts/', PostListView.as_view(), name='posts'),
    path('post/<str:slug>/', PostDetailView.as_view(), name='post-detail'),
    path('search/', SearchView.as_view(), name='search'),
]
