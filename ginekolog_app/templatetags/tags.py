from django import template
from ginekolog_app.models import MenuBaseItems, Doctors

register = template.Library()


@register.simple_tag
def get_header_data():
    data = dict()
    data["header_menu"] = MenuBaseItems.objects.all()  # have to change from models.py
    data["doctor_left"] = Doctors.objects.all()[:2][0]
    data["doctor_right"] = Doctors.objects.all()[:2][1]
    return data


@register.simple_tag
def get_footer_data():
    data = dict()
    data["footer_menu"] = MenuBaseItems.objects.all()  # have to change from models.py
    return data


@register.filter(name='data_wow_delay')
def data_wow_delay(index):
    value = float(index) / 10 * index
    return value
