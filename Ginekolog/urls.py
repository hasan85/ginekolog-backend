"""Ginekolog URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
# from django.conf import settings
# from django.conf.urls.static import static
# from django.conf.urls.i18n import i18n_patterns  # for url translation
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path, include

from Ginekolog import settings
from ginekolog_app.views import flatpage

urlpatterns = [
    path('dashboard/', admin.site.urls),
    path('', include("ginekolog_app.urls")),
    path('ckeditor/', include('ckeditor_uploader.urls')),
    path('page/<path:url>', flatpage, name='django.contrib.flatpages.views.flatpage'),
]
if settings.DEBUG:
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
# if not settings.DEBUG:
#     urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
#     urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

# handler404 = app_view.handle_404
# handler500 = app_view.handle_500
# Multilanguage --------------------------------------------------------------------------


# urlpatterns += i18n_patterns(
#     path('', include("ginekolog_app.urls")),
# ) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# ----------------------------------------------------------------------------------------

# This is change default admin panel Headers and titles  ---------------------------------

admin.site.site_header = 'ADMINISTRATION'
admin.site.site_title = 'ADMINISTRATION'
admin.site.index_title = 'ADMINISTRATION'

# ----------------------------------------------------------------------------------------
