--
-- PostgreSQL database cluster dump
--

SET default_transaction_read_only = off;

SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;

--
-- Drop databases
--

DROP DATABASE db_rds;




--
-- Drop roles
--

DROP ROLE db_user;
DROP ROLE postgres;


--
-- Roles
--

CREATE ROLE db_user;
ALTER ROLE db_user WITH SUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION NOBYPASSRLS PASSWORD 'md5f2f83c96fa601782f572fda4767c3038';
CREATE ROLE postgres;
ALTER ROLE postgres WITH SUPERUSER INHERIT CREATEROLE CREATEDB LOGIN REPLICATION BYPASSRLS;






--
-- Database creation
--

CREATE DATABASE db_rds WITH TEMPLATE = template0 OWNER = postgres;
REVOKE CONNECT,TEMPORARY ON DATABASE template1 FROM PUBLIC;
GRANT CONNECT ON DATABASE template1 TO PUBLIC;


\connect db_rds

SET default_transaction_read_only = off;

--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.8
-- Dumped by pg_dump version 9.6.8

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: db_user
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO db_user;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: db_user
--

CREATE SEQUENCE public.auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO db_user;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: db_user
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: db_user
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO db_user;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: db_user
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO db_user;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: db_user
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: db_user
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO db_user;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: db_user
--

CREATE SEQUENCE public.auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO db_user;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: db_user
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: db_user
--

CREATE TABLE public.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO db_user;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: db_user
--

CREATE TABLE public.auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO db_user;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: db_user
--

CREATE SEQUENCE public.auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO db_user;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: db_user
--

ALTER SEQUENCE public.auth_user_groups_id_seq OWNED BY public.auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: db_user
--

CREATE SEQUENCE public.auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO db_user;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: db_user
--

ALTER SEQUENCE public.auth_user_id_seq OWNED BY public.auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: db_user
--

CREATE TABLE public.auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO db_user;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: db_user
--

CREATE SEQUENCE public.auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO db_user;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: db_user
--

ALTER SEQUENCE public.auth_user_user_permissions_id_seq OWNED BY public.auth_user_user_permissions.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: db_user
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO db_user;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: db_user
--

CREATE SEQUENCE public.django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO db_user;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: db_user
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: db_user
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO db_user;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: db_user
--

CREATE SEQUENCE public.django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO db_user;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: db_user
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_flatpage; Type: TABLE; Schema: public; Owner: db_user
--

CREATE TABLE public.django_flatpage (
    id integer NOT NULL,
    url character varying(100) NOT NULL,
    title character varying(200) NOT NULL,
    content text NOT NULL,
    enable_comments boolean NOT NULL,
    template_name character varying(70) NOT NULL,
    registration_required boolean NOT NULL
);


ALTER TABLE public.django_flatpage OWNER TO db_user;

--
-- Name: django_flatpage_id_seq; Type: SEQUENCE; Schema: public; Owner: db_user
--

CREATE SEQUENCE public.django_flatpage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_flatpage_id_seq OWNER TO db_user;

--
-- Name: django_flatpage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: db_user
--

ALTER SEQUENCE public.django_flatpage_id_seq OWNED BY public.django_flatpage.id;


--
-- Name: django_flatpage_sites; Type: TABLE; Schema: public; Owner: db_user
--

CREATE TABLE public.django_flatpage_sites (
    id integer NOT NULL,
    flatpage_id integer NOT NULL,
    site_id integer NOT NULL
);


ALTER TABLE public.django_flatpage_sites OWNER TO db_user;

--
-- Name: django_flatpage_sites_id_seq; Type: SEQUENCE; Schema: public; Owner: db_user
--

CREATE SEQUENCE public.django_flatpage_sites_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_flatpage_sites_id_seq OWNER TO db_user;

--
-- Name: django_flatpage_sites_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: db_user
--

ALTER SEQUENCE public.django_flatpage_sites_id_seq OWNED BY public.django_flatpage_sites.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: db_user
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO db_user;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: db_user
--

CREATE SEQUENCE public.django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO db_user;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: db_user
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: db_user
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO db_user;

--
-- Name: django_site; Type: TABLE; Schema: public; Owner: db_user
--

CREATE TABLE public.django_site (
    id integer NOT NULL,
    domain character varying(100) NOT NULL,
    name character varying(50) NOT NULL
);


ALTER TABLE public.django_site OWNER TO db_user;

--
-- Name: django_site_id_seq; Type: SEQUENCE; Schema: public; Owner: db_user
--

CREATE SEQUENCE public.django_site_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_site_id_seq OWNER TO db_user;

--
-- Name: django_site_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: db_user
--

ALTER SEQUENCE public.django_site_id_seq OWNED BY public.django_site.id;


--
-- Name: ginekolog_app_category; Type: TABLE; Schema: public; Owner: db_user
--

CREATE TABLE public.ginekolog_app_category (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    "order" integer NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    description text NOT NULL,
    slug character varying(50)
);


ALTER TABLE public.ginekolog_app_category OWNER TO db_user;

--
-- Name: ginekolog_app_category_id_seq; Type: SEQUENCE; Schema: public; Owner: db_user
--

CREATE SEQUENCE public.ginekolog_app_category_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ginekolog_app_category_id_seq OWNER TO db_user;

--
-- Name: ginekolog_app_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: db_user
--

ALTER SEQUENCE public.ginekolog_app_category_id_seq OWNED BY public.ginekolog_app_category.id;


--
-- Name: ginekolog_app_countingservices; Type: TABLE; Schema: public; Owner: db_user
--

CREATE TABLE public.ginekolog_app_countingservices (
    id integer NOT NULL,
    title character varying(128) NOT NULL,
    icon character varying(100),
    count integer NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    "order" integer NOT NULL,
    more boolean NOT NULL
);


ALTER TABLE public.ginekolog_app_countingservices OWNER TO db_user;

--
-- Name: ginekolog_app_countingservices_id_seq; Type: SEQUENCE; Schema: public; Owner: db_user
--

CREATE SEQUENCE public.ginekolog_app_countingservices_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ginekolog_app_countingservices_id_seq OWNER TO db_user;

--
-- Name: ginekolog_app_countingservices_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: db_user
--

ALTER SEQUENCE public.ginekolog_app_countingservices_id_seq OWNED BY public.ginekolog_app_countingservices.id;


--
-- Name: ginekolog_app_doctors; Type: TABLE; Schema: public; Owner: db_user
--

CREATE TABLE public.ginekolog_app_doctors (
    id integer NOT NULL,
    name character varying(128) NOT NULL,
    surname character varying(128) NOT NULL,
    profession character varying(128) NOT NULL,
    phone_number character varying(20) NOT NULL,
    image character varying(100),
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    about text NOT NULL,
    slug character varying(50),
    status boolean NOT NULL,
    profile_image character varying(100),
    address character varying(255),
    email character varying(128),
    description character varying(255),
    work_grafic character varying(255)
);


ALTER TABLE public.ginekolog_app_doctors OWNER TO db_user;

--
-- Name: ginekolog_app_doctors_id_seq; Type: SEQUENCE; Schema: public; Owner: db_user
--

CREATE SEQUENCE public.ginekolog_app_doctors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ginekolog_app_doctors_id_seq OWNER TO db_user;

--
-- Name: ginekolog_app_doctors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: db_user
--

ALTER SEQUENCE public.ginekolog_app_doctors_id_seq OWNED BY public.ginekolog_app_doctors.id;


--
-- Name: ginekolog_app_flatpageimage; Type: TABLE; Schema: public; Owner: db_user
--

CREATE TABLE public.ginekolog_app_flatpageimage (
    id integer NOT NULL,
    image character varying(100),
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    flat_page_id integer NOT NULL
);


ALTER TABLE public.ginekolog_app_flatpageimage OWNER TO db_user;

--
-- Name: ginekolog_app_flatpageimage_id_seq; Type: SEQUENCE; Schema: public; Owner: db_user
--

CREATE SEQUENCE public.ginekolog_app_flatpageimage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ginekolog_app_flatpageimage_id_seq OWNER TO db_user;

--
-- Name: ginekolog_app_flatpageimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: db_user
--

ALTER SEQUENCE public.ginekolog_app_flatpageimage_id_seq OWNED BY public.ginekolog_app_flatpageimage.id;


--
-- Name: ginekolog_app_menubaseitems; Type: TABLE; Schema: public; Owner: db_user
--

CREATE TABLE public.ginekolog_app_menubaseitems (
    id integer NOT NULL,
    title character varying(128) NOT NULL,
    "order" integer NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);


ALTER TABLE public.ginekolog_app_menubaseitems OWNER TO db_user;

--
-- Name: ginekolog_app_menubaseitems_id_seq; Type: SEQUENCE; Schema: public; Owner: db_user
--

CREATE SEQUENCE public.ginekolog_app_menubaseitems_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ginekolog_app_menubaseitems_id_seq OWNER TO db_user;

--
-- Name: ginekolog_app_menubaseitems_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: db_user
--

ALTER SEQUENCE public.ginekolog_app_menubaseitems_id_seq OWNED BY public.ginekolog_app_menubaseitems.id;


--
-- Name: ginekolog_app_menusubitems; Type: TABLE; Schema: public; Owner: db_user
--

CREATE TABLE public.ginekolog_app_menusubitems (
    id integer NOT NULL,
    title character varying(128) NOT NULL,
    url character varying(255) NOT NULL,
    "order" integer NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    parent_sub_item_id integer,
    base_header_item_id integer
);


ALTER TABLE public.ginekolog_app_menusubitems OWNER TO db_user;

--
-- Name: ginekolog_app_menusubitems_id_seq; Type: SEQUENCE; Schema: public; Owner: db_user
--

CREATE SEQUENCE public.ginekolog_app_menusubitems_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ginekolog_app_menusubitems_id_seq OWNER TO db_user;

--
-- Name: ginekolog_app_menusubitems_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: db_user
--

ALTER SEQUENCE public.ginekolog_app_menusubitems_id_seq OWNED BY public.ginekolog_app_menusubitems.id;


--
-- Name: ginekolog_app_myflatpage; Type: TABLE; Schema: public; Owner: db_user
--

CREATE TABLE public.ginekolog_app_myflatpage (
    flatpage_ptr_id integer NOT NULL,
    cover character varying(100) NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);


ALTER TABLE public.ginekolog_app_myflatpage OWNER TO db_user;

--
-- Name: ginekolog_app_picture; Type: TABLE; Schema: public; Owner: db_user
--

CREATE TABLE public.ginekolog_app_picture (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    description text NOT NULL,
    image character varying(100) NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    category_belongs_to_id integer,
    doctor_belongs_to_id integer NOT NULL,
    slug character varying(50)
);


ALTER TABLE public.ginekolog_app_picture OWNER TO db_user;

--
-- Name: ginekolog_app_picture_id_seq; Type: SEQUENCE; Schema: public; Owner: db_user
--

CREATE SEQUENCE public.ginekolog_app_picture_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ginekolog_app_picture_id_seq OWNER TO db_user;

--
-- Name: ginekolog_app_picture_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: db_user
--

ALTER SEQUENCE public.ginekolog_app_picture_id_seq OWNED BY public.ginekolog_app_picture.id;


--
-- Name: ginekolog_app_picturepage; Type: TABLE; Schema: public; Owner: db_user
--

CREATE TABLE public.ginekolog_app_picturepage (
    id integer NOT NULL,
    cover character varying(100) NOT NULL,
    description character varying(255) NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);


ALTER TABLE public.ginekolog_app_picturepage OWNER TO db_user;

--
-- Name: ginekolog_app_picturepage_id_seq; Type: SEQUENCE; Schema: public; Owner: db_user
--

CREATE SEQUENCE public.ginekolog_app_picturepage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ginekolog_app_picturepage_id_seq OWNER TO db_user;

--
-- Name: ginekolog_app_picturepage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: db_user
--

ALTER SEQUENCE public.ginekolog_app_picturepage_id_seq OWNED BY public.ginekolog_app_picturepage.id;


--
-- Name: ginekolog_app_post; Type: TABLE; Schema: public; Owner: db_user
--

CREATE TABLE public.ginekolog_app_post (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    content text NOT NULL,
    cover character varying(100),
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    author_id integer NOT NULL,
    category_id integer,
    slug character varying(50)
);


ALTER TABLE public.ginekolog_app_post OWNER TO db_user;

--
-- Name: ginekolog_app_post_id_seq; Type: SEQUENCE; Schema: public; Owner: db_user
--

CREATE SEQUENCE public.ginekolog_app_post_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ginekolog_app_post_id_seq OWNER TO db_user;

--
-- Name: ginekolog_app_post_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: db_user
--

ALTER SEQUENCE public.ginekolog_app_post_id_seq OWNED BY public.ginekolog_app_post.id;


--
-- Name: ginekolog_app_postpage; Type: TABLE; Schema: public; Owner: db_user
--

CREATE TABLE public.ginekolog_app_postpage (
    id integer NOT NULL,
    cover character varying(100) NOT NULL,
    description character varying(255) NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);


ALTER TABLE public.ginekolog_app_postpage OWNER TO db_user;

--
-- Name: ginekolog_app_postpage_id_seq; Type: SEQUENCE; Schema: public; Owner: db_user
--

CREATE SEQUENCE public.ginekolog_app_postpage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ginekolog_app_postpage_id_seq OWNER TO db_user;

--
-- Name: ginekolog_app_postpage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: db_user
--

ALTER SEQUENCE public.ginekolog_app_postpage_id_seq OWNED BY public.ginekolog_app_postpage.id;


--
-- Name: ginekolog_app_services; Type: TABLE; Schema: public; Owner: db_user
--

CREATE TABLE public.ginekolog_app_services (
    id integer NOT NULL,
    title character varying(128) NOT NULL,
    content character varying(128) NOT NULL,
    icon character varying(100),
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    "order" integer NOT NULL
);


ALTER TABLE public.ginekolog_app_services OWNER TO db_user;

--
-- Name: ginekolog_app_services_id_seq; Type: SEQUENCE; Schema: public; Owner: db_user
--

CREATE SEQUENCE public.ginekolog_app_services_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ginekolog_app_services_id_seq OWNER TO db_user;

--
-- Name: ginekolog_app_services_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: db_user
--

ALTER SEQUENCE public.ginekolog_app_services_id_seq OWNED BY public.ginekolog_app_services.id;


--
-- Name: ginekolog_app_socialnetworkaddresses; Type: TABLE; Schema: public; Owner: db_user
--

CREATE TABLE public.ginekolog_app_socialnetworkaddresses (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    url character varying(255) NOT NULL,
    "order" integer NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    doctor_belongs_to_id integer NOT NULL
);


ALTER TABLE public.ginekolog_app_socialnetworkaddresses OWNER TO db_user;

--
-- Name: ginekolog_app_socialnetworkaddresses_id_seq; Type: SEQUENCE; Schema: public; Owner: db_user
--

CREATE SEQUENCE public.ginekolog_app_socialnetworkaddresses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ginekolog_app_socialnetworkaddresses_id_seq OWNER TO db_user;

--
-- Name: ginekolog_app_socialnetworkaddresses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: db_user
--

ALTER SEQUENCE public.ginekolog_app_socialnetworkaddresses_id_seq OWNED BY public.ginekolog_app_socialnetworkaddresses.id;


--
-- Name: ginekolog_app_video; Type: TABLE; Schema: public; Owner: db_user
--

CREATE TABLE public.ginekolog_app_video (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    description text NOT NULL,
    url character varying(255) NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone,
    doctor_belongs_to_id integer NOT NULL,
    category_belongs_to_id integer,
    slug character varying(50)
);


ALTER TABLE public.ginekolog_app_video OWNER TO db_user;

--
-- Name: ginekolog_app_video_id_seq; Type: SEQUENCE; Schema: public; Owner: db_user
--

CREATE SEQUENCE public.ginekolog_app_video_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ginekolog_app_video_id_seq OWNER TO db_user;

--
-- Name: ginekolog_app_video_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: db_user
--

ALTER SEQUENCE public.ginekolog_app_video_id_seq OWNED BY public.ginekolog_app_video.id;


--
-- Name: ginekolog_app_videopage; Type: TABLE; Schema: public; Owner: db_user
--

CREATE TABLE public.ginekolog_app_videopage (
    id integer NOT NULL,
    cover character varying(100) NOT NULL,
    description character varying(255) NOT NULL,
    created_at timestamp with time zone,
    updated_at timestamp with time zone
);


ALTER TABLE public.ginekolog_app_videopage OWNER TO db_user;

--
-- Name: ginekolog_app_videopage_id_seq; Type: SEQUENCE; Schema: public; Owner: db_user
--

CREATE SEQUENCE public.ginekolog_app_videopage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ginekolog_app_videopage_id_seq OWNER TO db_user;

--
-- Name: ginekolog_app_videopage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: db_user
--

ALTER SEQUENCE public.ginekolog_app_videopage_id_seq OWNED BY public.ginekolog_app_videopage.id;


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: auth_user id; Type: DEFAULT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.auth_user ALTER COLUMN id SET DEFAULT nextval('public.auth_user_id_seq'::regclass);


--
-- Name: auth_user_groups id; Type: DEFAULT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.auth_user_groups ALTER COLUMN id SET DEFAULT nextval('public.auth_user_groups_id_seq'::regclass);


--
-- Name: auth_user_user_permissions id; Type: DEFAULT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_user_user_permissions_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: django_flatpage id; Type: DEFAULT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.django_flatpage ALTER COLUMN id SET DEFAULT nextval('public.django_flatpage_id_seq'::regclass);


--
-- Name: django_flatpage_sites id; Type: DEFAULT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.django_flatpage_sites ALTER COLUMN id SET DEFAULT nextval('public.django_flatpage_sites_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Name: django_site id; Type: DEFAULT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.django_site ALTER COLUMN id SET DEFAULT nextval('public.django_site_id_seq'::regclass);


--
-- Name: ginekolog_app_category id; Type: DEFAULT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.ginekolog_app_category ALTER COLUMN id SET DEFAULT nextval('public.ginekolog_app_category_id_seq'::regclass);


--
-- Name: ginekolog_app_countingservices id; Type: DEFAULT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.ginekolog_app_countingservices ALTER COLUMN id SET DEFAULT nextval('public.ginekolog_app_countingservices_id_seq'::regclass);


--
-- Name: ginekolog_app_doctors id; Type: DEFAULT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.ginekolog_app_doctors ALTER COLUMN id SET DEFAULT nextval('public.ginekolog_app_doctors_id_seq'::regclass);


--
-- Name: ginekolog_app_flatpageimage id; Type: DEFAULT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.ginekolog_app_flatpageimage ALTER COLUMN id SET DEFAULT nextval('public.ginekolog_app_flatpageimage_id_seq'::regclass);


--
-- Name: ginekolog_app_menubaseitems id; Type: DEFAULT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.ginekolog_app_menubaseitems ALTER COLUMN id SET DEFAULT nextval('public.ginekolog_app_menubaseitems_id_seq'::regclass);


--
-- Name: ginekolog_app_menusubitems id; Type: DEFAULT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.ginekolog_app_menusubitems ALTER COLUMN id SET DEFAULT nextval('public.ginekolog_app_menusubitems_id_seq'::regclass);


--
-- Name: ginekolog_app_picture id; Type: DEFAULT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.ginekolog_app_picture ALTER COLUMN id SET DEFAULT nextval('public.ginekolog_app_picture_id_seq'::regclass);


--
-- Name: ginekolog_app_picturepage id; Type: DEFAULT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.ginekolog_app_picturepage ALTER COLUMN id SET DEFAULT nextval('public.ginekolog_app_picturepage_id_seq'::regclass);


--
-- Name: ginekolog_app_post id; Type: DEFAULT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.ginekolog_app_post ALTER COLUMN id SET DEFAULT nextval('public.ginekolog_app_post_id_seq'::regclass);


--
-- Name: ginekolog_app_postpage id; Type: DEFAULT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.ginekolog_app_postpage ALTER COLUMN id SET DEFAULT nextval('public.ginekolog_app_postpage_id_seq'::regclass);


--
-- Name: ginekolog_app_services id; Type: DEFAULT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.ginekolog_app_services ALTER COLUMN id SET DEFAULT nextval('public.ginekolog_app_services_id_seq'::regclass);


--
-- Name: ginekolog_app_socialnetworkaddresses id; Type: DEFAULT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.ginekolog_app_socialnetworkaddresses ALTER COLUMN id SET DEFAULT nextval('public.ginekolog_app_socialnetworkaddresses_id_seq'::regclass);


--
-- Name: ginekolog_app_video id; Type: DEFAULT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.ginekolog_app_video ALTER COLUMN id SET DEFAULT nextval('public.ginekolog_app_video_id_seq'::regclass);


--
-- Name: ginekolog_app_videopage id; Type: DEFAULT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.ginekolog_app_videopage ALTER COLUMN id SET DEFAULT nextval('public.ginekolog_app_videopage_id_seq'::regclass);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: db_user
--

COPY public.auth_group (id, name) FROM stdin;
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: db_user
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 1, false);


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: db_user
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: db_user
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 1, false);


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: db_user
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can add user	2	add_user
5	Can change user	2	change_user
6	Can delete user	2	delete_user
7	Can add group	3	add_group
8	Can change group	3	change_group
9	Can delete group	3	delete_group
10	Can add permission	4	add_permission
11	Can change permission	4	change_permission
12	Can delete permission	4	delete_permission
13	Can add content type	5	add_contenttype
14	Can change content type	5	change_contenttype
15	Can delete content type	5	delete_contenttype
16	Can add session	6	add_session
17	Can change session	6	change_session
18	Can delete session	6	delete_session
19	Can add site	7	add_site
20	Can change site	7	change_site
21	Can delete site	7	delete_site
22	Can add flat page	8	add_flatpage
23	Can change flat page	8	change_flatpage
24	Can delete flat page	8	delete_flatpage
25	Can add Header sub-menu item	9	add_menusubitems
26	Can change Header sub-menu item	9	change_menusubitems
27	Can delete Header sub-menu item	9	delete_menusubitems
28	Can add Social Network Address	10	add_socialnetworkaddresses
29	Can change Social Network Address	10	change_socialnetworkaddresses
30	Can delete Social Network Address	10	delete_socialnetworkaddresses
31	Can add Services with count	11	add_countingservices
32	Can change Services with count	11	change_countingservices
33	Can delete Services with count	11	delete_countingservices
34	Can add Service	12	add_services
35	Can change Service	12	change_services
36	Can delete Service	12	delete_services
37	Can add Category	13	add_category
38	Can change Category	13	change_category
39	Can delete Category	13	delete_category
40	Can add Picture	14	add_picture
41	Can change Picture	14	change_picture
42	Can delete Picture	14	delete_picture
43	Can add Post	15	add_post
44	Can change Post	15	change_post
45	Can delete Post	15	delete_post
46	Can add Video	16	add_video
47	Can change Video	16	change_video
48	Can delete Video	16	delete_video
49	Can add Quick page	17	add_myflatpage
50	Can change Quick page	17	change_myflatpage
51	Can delete Quick page	17	delete_myflatpage
52	Can add flatpage image	18	add_flatpageimage
53	Can change flatpage image	18	change_flatpageimage
54	Can delete flatpage image	18	delete_flatpageimage
55	Can add Doctor	19	add_doctors
56	Can change Doctor	19	change_doctors
57	Can delete Doctor	19	delete_doctors
58	Can add Header base-menu item	20	add_menubaseitems
59	Can change Header base-menu item	20	change_menubaseitems
60	Can delete Header base-menu item	20	delete_menubaseitems
61	Can add Picture Page Option	21	add_picturepage
62	Can change Picture Page Option	21	change_picturepage
63	Can delete Picture Page Option	21	delete_picturepage
64	Can add Post Page Option	22	add_postpage
65	Can change Post Page Option	22	change_postpage
66	Can delete Post Page Option	22	delete_postpage
67	Can add Video Page Option	23	add_videopage
68	Can change Video Page Option	23	change_videopage
69	Can delete Video Page Option	23	delete_videopage
\.


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: db_user
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 69, true);


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: db_user
--

COPY public.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
1	pbkdf2_sha256$100000$SecPHaXVZzlE$aPJ/xklvDQFXFwiCUZINQqzRbrLYdDcYmrEX2UQjobQ=	2018-03-14 16:29:05.813955+00	t	root				t	t	2018-03-11 14:03:45.589473+00
2	pbkdf2_sha256$100000$FCwpeQNUHOKV$uwLgWRgqV/v6cCvmr8twQoV8N06rrA37pMNyiBgb4qA=	2018-04-16 12:05:11.918969+00	t	mahwd				t	t	2018-04-15 17:08:09.498678+00
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: db_user
--

COPY public.auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: db_user
--

SELECT pg_catalog.setval('public.auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: db_user
--

SELECT pg_catalog.setval('public.auth_user_id_seq', 2, true);


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: db_user
--

COPY public.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: db_user
--

SELECT pg_catalog.setval('public.auth_user_user_permissions_id_seq', 1, false);


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: db_user
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2018-03-11 14:04:30.856878+00	2	46.101.109.121	1	[{"added": {}}]	7	1
2	2018-03-11 14:21:29.290343+00	1	Dr. Rəşad İsmayılzadə	1	[{"added": {}}, {"added": {"name": "Social Network Address", "object": "facebook"}}, {"added": {"name": "Social Network Address", "object": "instagram"}}, {"added": {"name": "Social Network Address", "object": "twitter"}}, {"added": {"name": "Social Network Address", "object": "linkedIn"}}]	19	1
3	2018-03-11 14:25:23.022162+00	1	Dr. Rəşad İsmayılzadə	2	[{"changed": {"fields": ["image", "profile_image"]}}]	19	1
4	2018-03-11 14:27:42.795264+00	2	Dr. Natı Musalı	1	[{"added": {}}, {"added": {"name": "Social Network Address", "object": "facebook"}}, {"added": {"name": "Social Network Address", "object": "instagram"}}, {"added": {"name": "Social Network Address", "object": "twitter"}}, {"added": {"name": "Social Network Address", "object": "linkedIn"}}]	19	1
5	2018-03-11 14:29:17.924499+00	2	Dr. Natı Musalı	2	[{"changed": {"fields": ["image"]}}]	19	1
6	2018-03-11 15:30:09.379486+00	1	Dr. Rəşad İsmayılzadə	2	[{"changed": {"fields": ["image"]}}]	19	1
7	2018-03-11 15:31:47.394444+00	1	Dr. Rəşad İsmayılzadə	2	[{"changed": {"fields": ["profile_image"]}}]	19	1
8	2018-03-12 07:06:13.635385+00	1	Demo	1	[{"added": {}}]	13	1
9	2018-03-12 07:06:29.39243+00	1	Bla	1	[{"added": {}}]	14	1
10	2018-03-12 19:23:44.943518+00	2	Dr. Natı Musalı	2	[{"changed": {"fields": ["address"]}}]	19	1
11	2018-03-12 19:23:55.57595+00	1	Dr. Rəşad İsmayılzadə	2	[{"changed": {"fields": ["address"]}}]	19	1
12	2018-03-14 16:30:32.781991+00	1	it is short description about doctor	1	[{"added": {}}]	21	1
13	2018-03-14 16:31:24.93072+00	1	burada sehife haqqinda qısa məlumat olacaq	1	[{"added": {}}]	22	1
14	2018-03-14 16:31:39.188684+00	1	burada sehife haqqinda qısa məlumat olacaq	2	[{"changed": {"fields": ["description"]}}]	21	1
15	2018-03-14 16:32:18.708663+00	1	burada sehife haqqinda qısa məlumat olacaq	1	[{"added": {}}]	23	1
16	2018-04-15 17:14:09.537378+00	1	Dr. Rəşad İsmayılzadə	2	[{"changed": {"fields": ["image"]}}]	19	2
17	2018-04-15 17:16:39.109455+00	1	Doğuş və Qeysəriyyə	1	[{"added": {}}]	12	2
18	2018-04-15 17:16:59.044608+00	2	Ginekolojik Xəstəliklər	1	[{"added": {}}]	12	2
19	2018-04-15 17:17:21.422024+00	3	Ginekolojik Əməliyyatlar	1	[{"added": {}}]	12	2
20	2018-04-15 17:17:42.215473+00	4	Sonsuzluq və Süni Mayalanma	1	[{"added": {}}]	12	2
21	2018-04-15 17:18:37.457564+00	2	Ginekolojik Xəstəliklər	2	[{"changed": {"fields": ["icon"]}}]	12	2
22	2018-04-15 17:18:49.9129+00	2	Ginekolojik Xəstəliklər	2	[{"changed": {"fields": ["icon"]}}]	12	2
23	2018-04-15 17:19:59.987665+00	1	Doğuş və Qeysəriyyə	1	[{"added": {}}]	11	2
24	2018-04-15 17:20:30.465498+00	2	Kesar	1	[{"added": {}}]	11	2
25	2018-04-15 17:20:47.852167+00	3	Laparoskopik Əməliyyat	1	[{"added": {}}]	11	2
26	2018-04-15 17:21:12.742181+00	4	Süni Mayalanma	1	[{"added": {}}]	11	2
27	2018-04-15 17:27:56.956364+00	3	demo	1	[{"added": {}}]	16	2
28	2018-04-15 17:30:03.427323+00	4	Okaber Istersenmi	1	[{"added": {}}]	16	2
29	2018-04-15 17:31:15.85177+00	1	Doğuş və Qeysəriyyə	1	[{"added": {}}]	15	2
30	2018-04-15 17:32:01.641307+00	1	Haqqımızda	1	[{"added": {}}]	20	2
31	2018-04-15 17:33:15.733339+00	2	Hamiləlik	1	[{"added": {}}, {"added": {"name": "Header sub-menu item", "object": "Hamil\\u0259lik t\\u0259qvimi"}}]	20	2
32	2018-04-15 17:35:37.914939+00	2	Hamiləlik	2	[{"added": {"name": "Header sub-menu item", "object": "Ay Ay hamil\\u0259lik"}}, {"added": {"name": "Header sub-menu item", "object": "Daun sindromu skriningi (2li ,3l\\u00fc v\\u0259 4l\\u00fc testl\\u0259r)"}}, {"added": {"name": "Header sub-menu item", "object": "NST"}}, {"added": {"name": "Header sub-menu item", "object": "Hamil\\u0259likd\\u0259 infeksiyalar"}}, {"added": {"name": "Header sub-menu item", "object": "Hamil\\u0259likl\\u0259 v\\u0259 yana\\u015f\\u0131 x\\u0259st\\u0259likl\\u0259r"}}, {"added": {"name": "Header sub-menu item", "object": "Hamil\\u0259liyin t\\u0259zyiqli x\\u0259st\\u0259likl\\u0259ri"}}]	20	2
33	2018-04-15 17:37:54.16234+00	2	Hamiləlik	2	[{"added": {"name": "Header sub-menu item", "object": "Hamil\\u0259likd\\u0259 \\u015f\\u0259k\\u0259r"}}, {"changed": {"fields": ["parent_sub_item", "order"], "name": "Header sub-menu item", "object": "Hamil\\u0259likl\\u0259 v\\u0259 yana\\u015f\\u0131 x\\u0259st\\u0259likl\\u0259r"}}, {"changed": {"fields": ["order"], "name": "Header sub-menu item", "object": "Hamil\\u0259likd\\u0259 infeksiyalar"}}, {"changed": {"fields": ["order"], "name": "Header sub-menu item", "object": "Hamil\\u0259liyin t\\u0259zyiqli x\\u0259st\\u0259likl\\u0259ri"}}, {"changed": {"fields": ["order"], "name": "Header sub-menu item", "object": "Daun sindromu skriningi (2li ,3l\\u00fc v\\u0259 4l\\u00fc testl\\u0259r)"}}, {"changed": {"fields": ["order"], "name": "Header sub-menu item", "object": "Hamil\\u0259lik t\\u0259qvimi"}}, {"changed": {"fields": ["order"], "name": "Header sub-menu item", "object": "Ay Ay hamil\\u0259lik"}}]	20	2
34	2018-04-15 17:38:55.796648+00	1	Haqqımızda	2	[{"added": {"name": "Header sub-menu item", "object": "Uzman Dr. R\\u0259\\u015fad \\u0130smay\\u0131lzad\\u0259"}}, {"added": {"name": "Header sub-menu item", "object": "Uzman Dr. Nati Musal\\u0131"}}]	20	2
35	2018-04-15 17:39:02.358583+00	1	Haqqımızda	2	[]	20	2
36	2018-04-15 17:39:22.848118+00	2	Hamiləlik	2	[{"changed": {"fields": ["order"], "name": "Header sub-menu item", "object": "Hamil\\u0259likl\\u0259 v\\u0259 yana\\u015f\\u0131 x\\u0259st\\u0259likl\\u0259r"}}, {"changed": {"fields": ["order"], "name": "Header sub-menu item", "object": "Ay Ay hamil\\u0259lik"}}]	20	2
37	2018-04-16 10:10:09.405257+00	2	Ginekolojik Əməliyyatlar	1	[{"added": {}}]	13	2
38	2018-04-16 10:10:13.8954+00	2	Aşılama	1	[{"added": {}}]	15	2
39	2018-04-16 10:10:27.296238+00	1	Doğuş və Qeysəriyyə	3		15	2
40	2018-04-16 10:13:13.103696+00	1	Aşılama	1	[{"added": {}}, {"added": {"name": "flatpage image", "object": "FlatpageImage object (1)"}}]	17	2
41	2018-04-16 10:18:41.936423+00	3	Sonsuzluq və Süni Mayalanma	1	[{"added": {}}, {"added": {"name": "Header sub-menu item", "object": "Sonsuzluq n\\u0259dir v\\u0259 m\\u00fcalic\\u0259si n\\u0259dir"}}, {"added": {"name": "Header sub-menu item", "object": "A\\u015f\\u0131lama"}}, {"added": {"name": "Header sub-menu item", "object": "S\\u00fcni mayalanma n\\u0259dir v\\u0259 kim\\u0259 edilir"}}, {"added": {"name": "Header sub-menu item", "object": "S\\u00fcni mayalanmada yenilikl\\u0259r"}}]	20	2
42	2018-04-16 10:22:33.500943+00	3	Sonsuzluq və Süni Mayalanma	2	[{"changed": {"fields": ["url"], "name": "Header sub-menu item", "object": "A\\u015f\\u0131lama"}}]	20	2
43	2018-04-16 10:23:17.780156+00	3	Sonsuzluq və Süni Mayalanma	2	[{"changed": {"fields": ["url"], "name": "Header sub-menu item", "object": "A\\u015f\\u0131lama"}}]	20	2
44	2018-04-16 10:26:39.698152+00	2	Ay ay hamiləlik	1	[{"added": {}}]	17	2
45	2018-04-16 10:27:21.114226+00	2	Ay ay hamiləlik	2	[{"changed": {"fields": ["content"]}}]	17	2
46	2018-04-16 10:34:20.868289+00	3	ICSI nədir ?	1	[{"added": {}}]	17	2
47	2018-04-16 10:35:04.939342+00	3	Sonsuzluq və Süni Mayalanma	2	[{"changed": {"fields": ["url"], "name": "Header sub-menu item", "object": "S\\u00fcni mayalanmada yenilikl\\u0259r"}}]	20	2
48	2018-04-16 10:38:00.505031+00	4	Daun sindromu skrininqi  və ikili,üçlü və dördlü testlər	1	[{"added": {}}]	17	2
49	2018-04-16 10:40:42.794877+00	4	Daun sindromu skrininqi  və ikili,üçlü və dördlü testlər	2	[{"changed": {"fields": ["cover"]}}]	17	2
50	2018-04-16 10:42:49.139039+00	4	Daun sindromu skrininqi  və ikili,üçlü və dördlü testlər	2	[{"changed": {"fields": ["cover"]}}]	17	2
51	2018-04-16 10:48:24.432722+00	5	Hamiləlikdə infeksiyalar	1	[{"added": {}}]	17	2
52	2018-04-16 10:48:41.328885+00	2	Hamiləlik	2	[{"changed": {"fields": ["url"], "name": "Header sub-menu item", "object": "Hamil\\u0259likd\\u0259 infeksiyalar"}}, {"changed": {"fields": ["url"], "name": "Header sub-menu item", "object": "Daun sindromu skriningi (2li ,3l\\u00fc v\\u0259 4l\\u00fc testl\\u0259r)"}}]	20	2
53	2018-04-16 10:52:04.800933+00	6	Mikroçip üsulu ilə Süni mayalanma	1	[{"added": {}}]	17	2
54	2018-04-16 10:56:20.932011+00	3	Sonsuzluq və Süni Mayalanma	2	[{"added": {"name": "Header sub-menu item", "object": "ICSI n\\u0259dir?"}}, {"added": {"name": "Header sub-menu item", "object": "Mikro\\u00e7ip \\u00fcsulu il\\u0259 S\\u00fcni mayalanma"}}, {"changed": {"fields": ["url"], "name": "Header sub-menu item", "object": "S\\u00fcni mayalanmada yenilikl\\u0259r"}}]	20	2
55	2018-04-16 10:56:56.443722+00	3	Sonsuzluq və Süni Mayalanma	2	[{"changed": {"fields": ["parent_sub_item"], "name": "Header sub-menu item", "object": "ICSI n\\u0259dir?"}}, {"changed": {"fields": ["parent_sub_item"], "name": "Header sub-menu item", "object": "Mikro\\u00e7ip \\u00fcsulu il\\u0259 S\\u00fcni mayalanma"}}]	20	2
56	2018-04-16 10:57:28.366676+00	3	Sonsuzluq və Süni Mayalanma	2	[{"changed": {"fields": ["url"], "name": "Header sub-menu item", "object": "Mikro\\u00e7ip \\u00fcsulu il\\u0259 S\\u00fcni mayalanma"}}]	20	2
57	2018-04-16 11:13:06.898469+00	7	Miomalar (fibromioma)	1	[{"added": {}}]	17	2
58	2018-04-16 11:21:20.821177+00	4	Ginekolojik Xəstəliklər	1	[{"added": {}}, {"added": {"name": "Header sub-menu item", "object": "Menstruasiya (ayba\\u015f\\u0131) pozqunlu\\u011fu"}}, {"added": {"name": "Header sub-menu item", "object": "Yumurtal\\u0131\\u011f\\u0131n polikistoz sindromu"}}, {"added": {"name": "Header sub-menu item", "object": "Endometrioz"}}, {"added": {"name": "Header sub-menu item", "object": "Yumurtal\\u0131q kistalar\\u0131"}}, {"added": {"name": "Header sub-menu item", "object": "Myomalar"}}, {"added": {"name": "Header sub-menu item", "object": "U\\u015faql\\u0131q boynu x\\u0259st\\u0259likl\\u0259ri"}}, {"added": {"name": "Header sub-menu item", "object": "Erroziya"}}, {"added": {"name": "Header sub-menu item", "object": "Menopauza"}}, {"added": {"name": "Header sub-menu item", "object": "Laparoskopiya"}}, {"added": {"name": "Header sub-menu item", "object": "Histeroskopiya"}}, {"added": {"name": "Header sub-menu item", "object": "A\\u00e7\\u0131q \\u0259m\\u0259liyyatlar"}}, {"added": {"name": "Header sub-menu item", "object": "Uroginekolojik \\u0259m\\u0259liyyatlar"}}]	20	2
59	2018-04-16 11:25:00.463316+00	8	NON -STRESS TEST (NST)	1	[{"added": {}}]	17	2
60	2018-04-16 11:25:19.391024+00	2	Hamiləlik	2	[{"changed": {"fields": ["url"], "name": "Header sub-menu item", "object": "NST"}}]	20	2
61	2018-04-16 11:28:31.163523+00	9	Sonsuzluq nədir?	1	[{"added": {}}]	17	2
62	2018-04-16 11:29:08.634707+00	3	Sonsuzluq və Süni Mayalanma	2	[{"changed": {"fields": ["url"], "name": "Header sub-menu item", "object": "Sonsuzluq n\\u0259dir v\\u0259 m\\u00fcalic\\u0259si n\\u0259dir"}}]	20	2
63	2018-04-16 11:58:43.269863+00	10	Süni mayalanma nədir ?	1	[{"added": {}}]	17	2
64	2018-04-16 12:00:06.650089+00	3	Sonsuzluq və Süni Mayalanma	2	[{"changed": {"fields": ["url"], "name": "Header sub-menu item", "object": "S\\u00fcni mayalanma n\\u0259dir v\\u0259 kim\\u0259 edilir"}}]	20	2
65	2018-04-17 03:25:11.673148+00	2	Hamiləlik	2	[{"changed": {"fields": ["title"], "name": "Header sub-menu item", "object": "Hamil\\u0259likd\\u0259 infeksiyalar(not ready)"}}, {"changed": {"fields": ["title"], "name": "Header sub-menu item", "object": "Hamil\\u0259liyin t\\u0259zyiqli x\\u0259st\\u0259likl\\u0259ri(not ready)"}}, {"changed": {"fields": ["title"], "name": "Header sub-menu item", "object": "Hamil\\u0259lik t\\u0259qvimi(not ready)"}}, {"changed": {"fields": ["title"], "name": "Header sub-menu item", "object": "Ay Ay hamil\\u0259lik(not ready)"}}, {"changed": {"fields": ["title"], "name": "Header sub-menu item", "object": "Hamil\\u0259likl\\u0259 v\\u0259 yana\\u015f\\u0131 x\\u0259st\\u0259likl\\u0259r(not ready)"}}, {"changed": {"fields": ["title"], "name": "Header sub-menu item", "object": "Hamil\\u0259likd\\u0259 \\u015f\\u0259k\\u0259r(not ready)"}}]	20	2
66	2018-04-17 03:26:41.614784+00	4	Ginekolojik Xəstəliklər	2	[{"changed": {"fields": ["title"], "name": "Header sub-menu item", "object": "Menstruasiya (ayba\\u015f\\u0131) pozqunlu\\u011fu(not ready)"}}, {"changed": {"fields": ["title"], "name": "Header sub-menu item", "object": "Yumurtal\\u0131\\u011f\\u0131n polikistoz sindromu(not ready)"}}, {"changed": {"fields": ["title"], "name": "Header sub-menu item", "object": "Endometrioz(not ready)"}}, {"changed": {"fields": ["title"], "name": "Header sub-menu item", "object": "Yumurtal\\u0131q kistalar\\u0131(not ready)"}}, {"changed": {"fields": ["title"], "name": "Header sub-menu item", "object": "U\\u015faql\\u0131q boynu x\\u0259st\\u0259likl\\u0259ri(not ready)"}}, {"changed": {"fields": ["title"], "name": "Header sub-menu item", "object": "Erroziya(not ready)"}}, {"changed": {"fields": ["title"], "name": "Header sub-menu item", "object": "Menopauza(not ready)"}}, {"changed": {"fields": ["title"], "name": "Header sub-menu item", "object": "Laparoskopiya(not ready)"}}, {"changed": {"fields": ["title"], "name": "Header sub-menu item", "object": "Histeroskopiya(not ready)"}}, {"changed": {"fields": ["title"], "name": "Header sub-menu item", "object": "A\\u00e7\\u0131q \\u0259m\\u0259liyyatlar(not ready)"}}, {"changed": {"fields": ["title"], "name": "Header sub-menu item", "object": "Uroginekolojik \\u0259m\\u0259liyyatlar(not ready)"}}]	20	2
67	2018-04-17 03:27:31.117608+00	2	Hamiləlik	2	[{"changed": {"fields": ["title"], "name": "Header sub-menu item", "object": "Hamil\\u0259likd\\u0259 infeksiyalar"}}]	20	2
68	2018-04-17 03:28:42.036969+00	2	Hamiləlik	2	[{"changed": {"fields": ["url"], "name": "Header sub-menu item", "object": "Ay Ay hamil\\u0259lik(not ready)"}}]	20	2
69	2018-04-17 03:28:56.313419+00	2	Hamiləlik	2	[{"changed": {"fields": ["title"], "name": "Header sub-menu item", "object": "Ay Ay hamil\\u0259lik"}}]	20	2
\.


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: db_user
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 69, true);


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: db_user
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	user
3	auth	group
4	auth	permission
5	contenttypes	contenttype
6	sessions	session
7	sites	site
8	flatpages	flatpage
9	ginekolog_app	menusubitems
10	ginekolog_app	socialnetworkaddresses
11	ginekolog_app	countingservices
12	ginekolog_app	services
13	ginekolog_app	category
14	ginekolog_app	picture
15	ginekolog_app	post
16	ginekolog_app	video
17	ginekolog_app	myflatpage
18	ginekolog_app	flatpageimage
19	ginekolog_app	doctors
20	ginekolog_app	menubaseitems
21	ginekolog_app	picturepage
22	ginekolog_app	postpage
23	ginekolog_app	videopage
\.


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: db_user
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 23, true);


--
-- Data for Name: django_flatpage; Type: TABLE DATA; Schema: public; Owner: db_user
--

COPY public.django_flatpage (id, url, title, content, enable_comments, template_name, registration_required) FROM stdin;
1	/asilama/	Aşılama	<p>Aşılama<br />\r\nAşılama ,sonsuzluq zamanı istifadə edilən bir m&uuml;alicə &uuml;suludur. Aşılama zamanı , kişidən alınan sperma<br />\r\nbir kanula vasitəsi ilə uşaqlıq daxilinə inyeksiya edilir ,Yəni kanula ilə uşaqlıq boynundan ke&ccedil;ərək,<br />\r\nsperma uşaqlıq boşluğuna buraxılır. Beləliklə spermanın yumurta h&uuml;ceyrəsini d&ouml;lləmə ehtimalı artır.<br />\r\nAşılamadan &ouml;ncə spermanın təmizlənməsi və x&uuml;susi bəzi proseduralardan ke&ccedil;irilməsi keyfiyyətli<br />\r\nspermaları ayırmağa və aşılamanın tutma ehtimalının artmasına səbəb olur.</p>\r\n\r\n<p>Aşılama etmədən əvvəl ;<br />\r\n- Qadınlarda boruların a&ccedil;ıq olub olmadığını yoxlamaq &uuml;&ccedil;&uuml;n Histerosalpingografiya filmi<br />\r\n&ccedil;əkilməlidir. Aşılama sadəcə uşaqlıq boruları a&ccedil;ıq olan bir xanıma edilə bilər.<br />\r\n- Yumurtalıqların g&uuml;c&uuml;n&uuml; yoxlamaq &uuml;&ccedil;&uuml;n hormon analizləri verilməlidir.&nbsp; Yumurtaliq rezervi zəf olan bir<br />\r\nxəstəyə aşılamadan savayı s&uuml;ni mayalanma etmək daha faydalıdır.<br />\r\nXanımda Prolaktin və TSH m&uuml;tləq yoxlanılmalıdır.<br />\r\nKişidə spermiogramma baxılmalıdır.<br />\r\nAşılama kimə edilir ?<br />\r\n- Sperm sayısı az olan və ya hərəkətli sperm sayısı yetərsiz olan hallarda (kişi faktoru )<br />\r\n- Qadında yumurtlamanın olmaması vəya yetərsiz olması (Ovulatuar Fakt&ouml;r)<br />\r\nBu halda ilk başda yumurtalıq induksiyası edilib, follikul yetişdirilir. Daha sonra m&uuml;vəffəqiyyəti<br />\r\nartırmaq &uuml;&ccedil;&uuml;n aşılama tətbiq edilir<br />\r\n- Səbəbi bilinməyən sonsuzluq<br />\r\n- İmpatensiya problem olan kişilərdə &nbsp;<br />\r\n- Uşaqlıq ağzı ilə əlaqəli problem olan xanımlarda (servİkal factor )<br />\r\nAşılamanın &uuml;st&uuml;nl&uuml;kləri<br />\r\nAşılama vasitəsilə sperma uşaqlıq boşluğuna , boruların girəcəyinin &ccedil;ox yaxınına buraxılır. Beləliklə ,<br />\r\nsperma uşaqlıq yolunun turşu m&uuml;hitindən və uşaqlıq boynu selikli tıxacından qurtulmuş olur. Eyni<br />\r\nzamanda sperma, bəzi xanımlarda sperma əleyhinə yaranabilən antikorlarla təmasdan qurtulur.<br />\r\nAşılama necə edilir ?<br />\r\nAşılama iki c&uuml;r aparıla bilər. Birincisi , xanımda ovulasiya problem yoxdursa,follikulometriya ilə follikulun<br />\r\nb&ouml;y&uuml;məsinə nəzarət edilib, follikul &ccedil;atladıqdan 36 saat sonra aşılama etmək olar .İkinci &uuml;sul, x&uuml;susən<br />\r\nfollikul yetişməsində problem olan xanımlarda , ilk başda yumurtalıq induksiyası ilə follikul b&ouml;y&uuml;d&uuml;l&uuml;r<br />\r\n.Follikulun &ouml;l&ccedil;&uuml;s&uuml; 18 mm a &ccedil;atdıqdan sonra HCG ilə &ccedil;atladılır.HCG vurulduqdan 36 saat sonra aşılama<br />\r\nedilir.<br />\r\nAşılama &ouml;z&uuml; ağrısız prosesdir.X&uuml;susi Kanula uşaqlıq boynundan ke&ccedil;irildikdən sonra, &ouml;ncədən<br />\r\nhazırlanmış sperma bu kanula vasitəsilə uşaqlıq boşluğuna yeridilir.<br />\r\nAşılamanın tutma ehtimalı 20%- dən artıq deyil. Biz klinikamızda bir xəstəyə 2dəfədən artıq aşılama<br />\r\netmirik. 2 dəfə ardıcıl aşılamadan nəticə almayan xəstələrdə digər m&uuml;alicə &uuml;sullarına ke&ccedil;irik .</p>	f		f
2	/ay-ay-hamilelik/	Ay ay hamiləlik	<p>Hamiləliyin birinci ayı (1-4 həftə)<br />\r\nTibbi olaraq hamiləliyi son g&ouml;rd&uuml;y&uuml;n&uuml;z aybaşının ilk g&uuml;n&uuml;ndən hesablamağa başlayırıq. Bu ayda<br />\r\nbədəninizdə hamiləlik &uuml;&ccedil;&uuml;n hazırlıqlar gedir.<br />\r\nAybaşı g&ouml;rmənizlə paralel olaraq yumurtalıqlarınızın birində artıq yeni follikul se&ccedil;ilir və b&ouml;y&uuml;məyə<br />\r\nbaşlayır. M&uuml;əyyən &ouml;l&ccedil;&uuml;yə &ccedil;atdıqdan sonra (təxminən aybaşının 14-c&uuml; g&uuml;n&uuml;ndə) ovulyasiya baş verir və<br />\r\nfollikulun i&ccedil;ərisindəki yumurtah&uuml;ceyrə qarın boşluğuna atılır, uşaqlıq borusunun ucları ilə tutulur və<br />\r\nuşaqlıq borusuna daxil edilir. Burada onu g&ouml;zləyən &ccedil;oxsaylı spermalardan biri ilə mayalanma baş verir.<br />\r\nMayalanmış yumurta bir ne&ccedil;ə g&uuml;n ərzində b&ouml;l&uuml;nərək uşaqlıq boşluğuna d&uuml;ş&uuml;r və uşaqlıq divarına yapışır.<br />\r\nHamiləliyin ikinci ayı (5-8 həftə)<br />\r\nBu ayda uşaqlıq boşluğuna yerləşmiş və d&ouml;llənmiş tək h&uuml;ceyrədən ibarət olan zigotdan embryo<br />\r\nformalaşmağa başlayır. Mayalanmadan 21-22 g&uuml;n sonra artıq &uuml;rək d&ouml;y&uuml;nt&uuml;ləri əmələ gəlir. Sıra ilə sinir<br />\r\nborusu, ağciyərlər, bağırsaqlar, sidik kisəsi və digər orqanlar əmələ gəlir. Bu ayın əvvəlində &ouml;l&ccedil;&uuml;s&uuml; 1.5mm<br />\r\nolan embryo sonda artıq 12.5mm uzunluğundadır. Bu ayda &ouml;z&uuml;n&uuml;z&uuml; xarici zərərli təsirlərdən<br />\r\n(infeksiyalar, dərmanlar, ş&uuml;alanma və s) maksimum qorumalısınız.<br />\r\nArtıq aybaşı gecikməsi baş verib. Bulantı, qusma, mədə yanğısı, tez-tez sidiyə getmə, yuxusuzluq,<br />\r\nhalsızlıq, k&ouml;p, qarında ağrılar, s&uuml;d vəzilərində b&ouml;y&uuml;mə və həssaslıq əsas şikayətlərdir. Məhz bu ayda ilk<br />\r\ndəfə həkimə m&uuml;raciət edib k&ouml;rpənin &uuml;rək d&ouml;y&uuml;nt&uuml;lərini eşidə bilərsiniz. Daha sonra həkiminiz sizə lazımi<br />\r\nanalizləri yazacaq.<br />\r\nHamiləliyin &uuml;&ccedil;&uuml;nc&uuml; ayı (9-12 həftə)<br />\r\nBu ayda k&ouml;rpənin bədəni tam formalaşmağa başlayır. Əl-ayaq barmaqları və dırnaqları əmələ gəlir.<br />\r\nƏvvəl daxili sonra isə xarici cinsiyyət orqanları formalaşır. Artıq ananın hələ hiss etmədiyi ki&ccedil;ik hərəkətlər<br />\r\nbaşlayıb. &Uuml;&ccedil;&uuml;nc&uuml; ayda k&ouml;rpənin boyu 5-8 cm, &ccedil;əkisi isə 50-70 qramdır.<br />\r\nƏvvəlki aylarda əmələ gəlmiş şikayətlər bu ayın sonuna qədər tədricən azalır. 12-ci həftənin sonunda<br />\r\nuşaqlıq b&ouml;y&uuml;yərək kişik &ccedil;anaqdan qarın boşluğuna &ccedil;ıxır.<br />\r\n12-ci həftədə həkim m&uuml;ayinəsi və ilk trimestr testləri vacibdir. (Ətraflı bax....)<br />\r\nHamiləliyin d&ouml;rd&uuml;nc&uuml; ayı (13-16 həftə)<br />\r\nK&ouml;rpənizin bu ayda artıq b&uuml;t&uuml;n orqanları formalaşmışdır. Barmağını əmməyə başlayır. Hərəkətləri artır<br />\r\nvə başı artıq &ccedil;ənəsinə doğru sıxılmış vəziyyətini dəyişir. Sa&ccedil;ları uzanmağa başlayır və b&uuml;t&uuml;n bədənini<br />\r\n&ouml;rtən Lanugo adlanan incə t&uuml;kc&uuml;klər də bu ayda yaranmağa başlayır. Bu ayda k&ouml;rpənin cinsiyyətini USM<br />\r\nilə təyin etmək olar. D&ouml;rd&uuml;nc&uuml; ayda k&ouml;rpənin boyu 11cm, &ccedil;əkisi isə 100-200 qramdır.<br />\r\nUşaqlıq artıq qarın boşluğuna doğru b&ouml;y&uuml;y&uuml;r və qarnın aşağı hissəsində yumru şəklində əlinizə gəlir. Bu<br />\r\nayda əsas şikayət qasıqlarda ağrılardan olur ki, bu da uşaqlıq bağlarının gərilməsinə bağlı olub, normal<br />\r\nhesab edilir. S&uuml;d vəziləriniz də b&ouml;y&uuml;y&uuml;r, &uuml;zərində damarlar daha aydın nəzərə &ccedil;arpır və d&ouml;ş gilələrinin<br />\r\nrəngi t&uuml;ndləşir. Bu aydan etibarən hamilələr &uuml;&ccedil;&uuml;n olan x&uuml;susi paltarlara ehtiyac duya bilərsiniz.</p>\r\n\r\n<p>&Uuml;&ccedil;l&uuml; &ndash;d&ouml;rdl&uuml; test analizləri bu həftədə edilir.<br />\r\nHamiləliyin beşinci ayı (17-20 həftə)<br />\r\nBu ayda x&uuml;susi ilə təkrar hamiləliklərdə k&ouml;rpənizin hərəkətlərini hiss etməyə başlayacaqsınız. Dərialtı piy<br />\r\ntəbəqəsi əmələ gəlir. Artıq bu ayda eşitmə siniri və daxili qulaq da tam formalaşır ki, bu da k&ouml;rpənizin<br />\r\nsizin səsinizi eşitdiyi mənasına gəlir. Bu aydan etibarən b&ouml;yrəklərin inkişafı tamamlanır, artıq k&ouml;rpəniz<br />\r\nsidiyini edir, eyni zamanda udqunma aktı da formalaşır. Beşinci ayda k&ouml;rpənin boyu 22-24cm, &ccedil;əkisi isə<br />\r\n300 qramdır.<br />\r\nBu ayda &ouml;z&uuml;n&uuml;zdə də ciddi dəyişikliklər hiss edəcəksiniz. Ən əsası bu ayın sonlarından etibarən<br />\r\nk&ouml;rpənizin hərəkətlərini artıq hiss edəcəksiniz. Bu hərəkətlər əvvəlcə y&uuml;ng&uuml;l olub tədricən artır .<br />\r\nUşaqlığınız da b&ouml;y&uuml;y&uuml;r və g&ouml;bək səviyyənizə &ccedil;atır, buna bağlı olaraq mədə yanması və qəbizlik,<br />\r\ntəngnəfəslik şikayətləriniz də arta bilər. Qan d&ouml;vranındakı dəyişikliklərə bağlı olaraq təzyiq aşağı<br />\r\nolmağa meyilli olur. Oturub -duranda və ya uzanıb-qalxanda qarnın aşağı hissəsində və beldə normal<br />\r\nhesab edilən dartılma şəklində ağrılar olar bilər. Bu aydan etibarən qanazlığınız yarana bilər, ehtiyac<br />\r\nolarsa həkiminiz sizə uyğun m&uuml;alicə təyin edəcək.<br />\r\nHamiləliyin altıncı ayı (21-24 həftə)<br />\r\nBu ayda k&ouml;rpəniz i&ccedil;ində &uuml;zd&uuml;y&uuml; d&ouml;lyanı mayeni udmağa başlayır. Dadbilmə formalaşır. Bağırsaqlarda<br />\r\nmekonium, yəni ilk nəcis əmələ gəlməyə başlayır. Bu aydan etibarən əzələlər daha aktiv inkişaf edir və<br />\r\nk&ouml;rpəniz istəyərək hərəkətlər edir, bu hərəkətlər doğum sonrası hərəkətlər &uuml;&ccedil;&uuml;n məşq xarakteri daşıyır.<br />\r\nBu ayın sonlarında ağciyərlər inkişafa başlayır və tənəff&uuml;s hərəkətləri formalaşır. Bu ayda oğlan<br />\r\nuşaqlarında xayalar qarın i&ccedil;indən xayalığa doğru d&uuml;şməyə başlayır, qız uşaqlarında isə uşaqlıq və<br />\r\nyumurtalıqlar formalaşıb. Altıncı ayın sonunda k&ouml;rpənizin boyu təxminən 30cm, &ccedil;əkisi isə 600 qramdır.<br />\r\nBu ayda təzyiqiniz normaya g&ouml;rə daha aşağı ola bilər. Qan azlığınız arta bilər, hamilələrdə dəmir ehtiyacı<br />\r\nhamilə olmayanlara g&ouml;rə daha &ccedil;oxdur. Tənəff&uuml;s sayınız artır. Bu aydan qarında yalan&ccedil;ı ağrılar başlaya<br />\r\nbilər.</p>\r\n\r\n<p>Hamiləliyin yeddinci ayı (25-28 həftə)<br />\r\nBu ayda k&ouml;rpənizin əl və ayaq izləri, dırnaqları əmələ gəlməyə başlayır. &Ouml;z&uuml;n&uuml; toxunaraq kəşf edir. Qaş<br />\r\nvə kirpikləri əmələ gəlir, sa&ccedil;ları uzanır və sıxlaşır. Dərialtı piy təbəqəsi qalınlaşır. Bu ayın sonunda<br />\r\ndoğulacaq uşaq yenidoğulmuş uşağa bənzəyir. 28-ci həftədə g&ouml;zlərini a&ccedil;ıb- bağlaya bilir və ana qarnında<br />\r\n30-60 dəqiqə yatır. Yeddindi ayın sonunda k&ouml;rpənizin boyu təxminən 37cm, &ccedil;əkisi isə 1000 qramdır.<br />\r\nUşaqlıq g&ouml;bəklə d&ouml;ş qəfəsi arasındadır. Arterial təzyiq əvvəlki aylara nisbətən artmışdır. K&ouml;rpənin<br />\r\nhərəkətləri əvvəlki aylara nisbətən artmışdır, x&uuml;susi lə oturaq və ya uzanmış vəziyyətdə daha &ccedil;ox hiss<br />\r\nedilir. Bu ayda yalan&ccedil;ı doğuş ağrıları artır, amma bu ağrılar qeyri-ritmik xarakter daşıyır və davamlı<br />\r\nolmur.<br />\r\nBu ayda hamiləlik şəkərini aşkarlamaq &uuml;&ccedil;&uuml;n şəkər y&uuml;kləməsi testi və Rh/Rh uyuşmazlığı olan xəstələrdə<br />\r\nx&uuml;susi analiz g&ouml;t&uuml;r&uuml;lməli, iynə vurulmalıdır.</p>\r\n\r\n<p>Hamiləliyin səkkizinci ayı (29-33 həftə)</p>\r\n\r\n<p>K&ouml;rpəniz s&uuml;rətlə b&ouml;y&uuml;məyə davam edir. Baş &ouml;l&ccedil;&uuml;s&uuml;n&uuml;n nisbəti bədənlə m&uuml;nasibdir. Artıq bu ayda &ccedil;ox<br />\r\nhallarda normal doğuşa uyğun vəziyyəti- baş aşağı vəziyyəti alır. Tənəff&uuml;s hərəkətlərinə diafraqma da<br />\r\nqoşulur. Oğlan uşaqlarında xayalar xayalıqlara d&uuml;ş&uuml;r. Bu ayda k&ouml;rpənin hərəkətlərini nisbi olaraq azalır.<br />\r\nArtıq hamiləliyin &uuml;&ccedil;&uuml;nc&uuml; trimestrindəsiniz. Qarnınınızən b&ouml;y&uuml;məsi və mexaniki təsirinə bağlı olaraq bel<br />\r\nağrısı, mədə yanması, qəbizlik, tez-tez sidiyə getmə, hemorroid, ayaqlarda varikoz damarlar, yorğunluq<br />\r\nkimi şikayətləriniz artacaq.</p>\r\n\r\n<p>Hamiləliyin doqqquzuncu ayı (34-37 həftə)<br />\r\nBu ayın sonundan etibarən artıq gerisayım başlayır. K&ouml;rpənizin dərialtı yağ təbəqəsi qalınlaşır, &ccedil;əkisi<br />\r\ns&uuml;rətlə artır. Dəri qoruyucu təbəqə verniks ilə &ouml;rt&uuml;lm&uuml;şd&uuml;r. Başı ki&ccedil;ik &ccedil;anağa doğru enir və vizual olaraq<br />\r\nqarın `aşağı d&uuml;şm&uuml;ş kimi g&ouml;rsənir.<br />\r\nK&ouml;rpənin başının &ccedil;anağa enməsi ilə əlaqədar olaraq əvvəlki aylarda olan &ccedil;ox şikayətlər azalır. Artıq doğuş<br />\r\nəlamətləri (ağrılar, su gəlməsi və ya nişan gəlməsi ) hər an ola bilər. Bu ayda həkim m&uuml;ayinəsi sıxlaşır.</p>\r\n\r\n<p>Hamiləliyin onuncu ayı (38-42 həftə)<br />\r\nNormal hamiləlikdə doğuş adətən 38-40 həftələr arasında baş verir. Bəzi hallarda isə doğuş 41-42 &ndash;ci<br />\r\nhəftələrdə baş verir. Əgər hamiləlik 42 həftədən &ccedil;ox davam edərsə vaxtı &ouml;tm&uuml;ş hamiləlik hesab edilir,<br />\r\nbu halda k&ouml;rpənin ana qarnında &ouml;l&uuml;m təhl&uuml;kəsi artır. Bu səbəbdən həkiminizə m&uuml;raciət etməniz<br />\r\nvacibdir.</p>	f		f
3	/icsi/	ICSI nədir ?	<p>&nbsp;&nbsp;&nbsp;&nbsp;S&uuml;ni mayalanamanın m&uuml;vəffəqiyyətini artırmaq &uuml;&ccedil;&uuml;n hər g&uuml;n yeni &uuml;sullar ,yeni dərmanlar tətbiq edilməyə<br />\r\nbaşlayır. Bu yeniliklərdən biridə İCSİ olan mikroinyeksiya metodudur . S&uuml;ni Mayalanma 2 metodla həyata<br />\r\nke&ccedil;irilə bilər. Birincisi , klassik İVF olub , digəri isə mikroinyeksiya metodudur. Hər iki &uuml;sulda , bildiyimiz kimi,<br />\r\nm&uuml;əyyən dərmanlarla follikullar yetışdırılıb, daha sonra yumurtalar toplanır ,kişidən spermalar alınır. Klassik<br />\r\n&uuml;sul ilə mikroinyeksiya metodu arasında fərq bu andan etibarən başlayır . Klassik &uuml;sulda ,sperm h&uuml;ceyrələri<br />\r\nyumurtanın ətrafında buraxılır və spermaların yumurtanı d&ouml;lləməsi g&ouml;zlənilir. Mikroinyeksiya metodunda<br />\r\nisə sağlam sperm h&uuml;ceyrəsi yumurta h&uuml;ceyrəsi i&ccedil;inə mikroskop altında yeridilir. Bu andan etibarən yenə hər<br />\r\niki &uuml;sul eynidir.<br />\r\nİCSİ kimlərə tətbiq edilir ?<br />\r\n- Sperm sayısı az olan<br />\r\n- Sperm hərəkətliliyi az olan<br />\r\n- Sperm h&uuml;ceyrələrində morfoloji anomaliyalar( quruluş anomaliyası )<br />\r\n- Daha &ouml;ncəki s&uuml;ni mayalanma zamanı d&ouml;llənmənin az olduğu hallarda<br />\r\n- Sperm h&uuml;ceyrələri TESE metodu ilə əldə edilib isə İCSi tətbiq edilir.<br />\r\nBizim s&uuml;ni mayalanma laboratoriyasında həm klassik İVF, həmdə Mikroinyeksiya ( İCSİ ) metodu tətbiq<br />\r\nedilir. Bunu &uuml;&ccedil;&uuml;n əlavə bir &ouml;dəniş alınmır.</p>	f		f
9	/sonsuzluq-nedir/	Sonsuzluq nədir?	<p>&nbsp;&nbsp;&nbsp;&nbsp;Bir c&uuml;tl&uuml;y&uuml;n bir il m&uuml;ddəti ilə he&ccedil; bir qorunma metodu olmadan d&uuml;zənli cinsi əlaqədə olmasına<br />\r\nbaxmayaraq hamiləliyin meydana gəlməməsi və ya hamiləliyin sona qədər davam etməməsinə sonsuzluq<br />\r\ndeyilir. C&uuml;tlərin təqribi 10-15 % - də sonsuzluq aşkarlanır . Burda əhəmiyyətli n&ouml;qtə, ke&ccedil;ən m&uuml;ddət ən az 1<br />\r\nil olmalıdır.<br />\r\nBizim cəmiyyətdə sonsuzluq s&ouml;z&uuml;ndən x&uuml;susən qadınlar arasında &ccedil;ox qorxulur və sonsuzluq diaqnozu bir<br />\r\npsixolojik d&uuml;şk&uuml;nl&uuml;yə səbəb olur. Xanımlar sonsuzluq diagnozu qoyulsa , artıq he&ccedil; bir zaman uşaq sahibi ola<br />\r\nbilməyəcəklərindən qorxurlar ,bunun bir stiqma kimi &ouml;zlərinə yapışdığını d&uuml;ş&uuml;n&uuml;rlər. Amma bu qətiyyən<br />\r\nbelə deyil. Sonsuzluğun m&uuml;alicəsi var. Sonsuzluq m&uuml;alicəsi m&uuml;vəffəqiyyətnini bir n&ouml;mrəli faktoru xəstənin<br />\r\nmaariflənməsi və həkimə g&uuml;vənməsidir. İndi gəlin, sonsuzluq səbəbləri və m&uuml;alicəsinə bir g&ouml;z ataq.<br />\r\nBir c&uuml;tl&uuml;y&uuml;n uşaq sahibi ola bilməsi &uuml;&ccedil;&uuml;n:<br />\r\n1. Qadında yumurtlama normal olmalıdır.<br />\r\n2. Yumurtlama olduğu zaman , uşaqlıq boruları bu yumurtanı tuta bilməli və yumurta uşaqlıq bourusu<br />\r\ni&ccedil;ində hərəkət etməlidir. Yəni sadəcə uşaqlıq borusunun a&ccedil;ıq olması kifayət deyil.<br />\r\n3. Yumurtlama zamani cinsi əlaqədə olmalıdır.<br />\r\n4. Sperm sayısı və hərəkətliliyi kifayət edəcək səviyyədə olmalıdır.<br />\r\n5. Spermlərin uşaqlıq borularına hərəkət etməsinə he&ccedil; bir mane olmali deyil.<br />\r\n6. Uşaqlıq borularında sperm və yumurta h&uuml;ceyrəsi birləşməli və d&ouml;l h&uuml;ceyrəsi (embrio) yaranmalıdır.<br />\r\n7. Meydana gələn embrio uşaqlıq boşluğuna gəlməli və implantasiya olmalıdır. Eyni zamanda<br />\r\nembrionun daşıdığı genetik materyal sağlam olmalıdır.<br />\r\nBu mərhələlərin hər hansi birində problem yarandıqda hamiləlik meydana gəlmir və ya sonuna qədər<br />\r\n&ccedil;atmır .<br />\r\nSonsuzluğun səbəbləri 3 əsas başlıq altında toplana bilər.<br />\r\n-Qadına aid səbəblər<br />\r\n-Kişiyə aid səbəblər<br />\r\n-Səbəbi aşkarlanmayan sonsuzluq<br />\r\nG&ouml;r&uuml;nd&uuml;y&uuml; kimi , sonsuzluq səbəbləri həm qadına həm kişiyə bağlı ola bilər. Bu səbəblə sonsuzluqdan<br />\r\nəziyyət &ccedil;əkən c&uuml;tl&uuml;klərdə , m&uuml;ayinəyə həm kişi, həm də qadın gəlməlidir və hər ikisi də<br />\r\ndəyərləndirilməlidir. Bəzi hallarda hər ikisində də problem ola bilər.<br />\r\nQadına aid Sonsuzluq səbəbləri<br />\r\n- Ovulasiya problemləri : Ovulasiyanin olmaması,follikulun yetişməməsi , keyfiyyətli follikulun əldə<br />\r\nedilməməsi<br />\r\n- Uşaqlıq borularına bağlı problemlər : uşaqlıq boruların doğuşdan olmaması, boruların bağlı olması<br />\r\nvə s.<br />\r\n- Uşaqlıq problemləri : uşaqlıqda morfoloji problemlər, submukoz miomalar, Asherman sindromu<br />\r\n(uşaqlıq daxili yapışıqlıqlar )</p>\r\n\r\n<p>- Digər xəstəliklər Endometrioz , genital organların inkişaf problemləri , cinsi və psixoloji problemlər<br />\r\n(məs . vaginismus )</p>\r\n\r\n<p>Kişiyə aid sonsuzluq səbəbləri<br />\r\n- Sperm sayısındə və ya hərəkətliliyində yetərsizlik<br />\r\n- Spermdə morfolojik problemlər.<br />\r\n- Spermlərin hərəkət etdiyi kanallarda tıxanıqlıq<br />\r\n- Varikosel , hidrosel və s.</p>\r\n\r\n<p>Sonsuzluğa səbəb ola biləcək digər faktorlar arasında siqaret, stres və k&ouml;kl&uuml;k g&ouml;stərilə bilər.<br />\r\nSonsuzluqdan əziyyət &ccedil;əkən c&uuml;tl&uuml;klər həkimə m&uuml;raciət etdikləri zaman , səbəbi aşkarlamaq &uuml;&ccedil;&uuml;n bəzi<br />\r\nanalizlər verməlidirlər .<br />\r\nBunlar FSH , LH,E2 Prolaktin və TSH qadında ,spermioqramma kişidədir. Əlavə olaraq qadında uşaqlıq<br />\r\nborularını yoxlamaq &uuml;&ccedil;&uuml;n histerosalpingografiya &ccedil;əkilməlidir. Bunlar hər c&uuml;tl&uuml;kdə m&uuml;tləqdir. Bundan savayı,<br />\r\nhər xəstəyə individual olaraq spesifik bir analizlər istənilə bilər.</p>	f		f
4	/daun-sindromu-skrininqi/	Daun sindromu skrininqi  və ikili,üçlü və dördlü testlər	<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">&nbsp;&nbsp;&nbsp;&nbsp;Daun sindromu ən sıx qarşılaşılan &nbsp;xromosomal patologiyadır. İlk dəfə 1866-ci ildə tərif edilmişdir , lakin səbəbi &nbsp;1959-cu ildə aydınlaşdırılmışdır. Xəstəlik alim John Langdon Daunun adını daşıyır.</span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Daun sindromun sıxlığı ortalama &nbsp;800 d&ouml;ldə birdir. Yəni doğulan 800 -1000 uşaqdan biri Daun sindromludur.Daun sindromlu uşaqların % 85&rdquo;i bir yaşına qədər &nbsp;və 50%-i 50 ildən &ccedil;ox yaşayır. </span></p>\r\n\r\n<p dir="ltr"><strong>Daun sindromu səbəbi </strong></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Hər insanda 23 c&uuml;t yəni &nbsp;46 xromosom var. Otosomal adlanan 22 c&uuml;t xromosom &nbsp;1-dən etibarən n&ouml;mrələnib. 23-c&uuml; c&uuml;tl&uuml;k isə seks xromosomlar ( x və y ) adlanır və insanın cinsiyyətini təyin edir. XX olduğu halda qadın, XY olduğu halda kişi cinsi olur. Qadında yumurtah&uuml;ceyrə , kişidə sperm &nbsp;46 xromosom yerinə meyoz b&ouml;l&uuml;nmə nəticəsində sadəcə 23 xromosom daşıyır. Yumurta h&uuml;ceyrəsi 22 X , sperma 22 x və ya 22 y paketinə sahibdir. Yumurta h&uuml;ceyrəsi sperma ilə mayalandıqda ana və atanın xromosomları birləşir &nbsp;və d&ouml;l h&uuml;ceyrəsində təkrar 46 xromosom meydana gəlir.Əgər meyoz b&ouml;l&uuml;nməsi sırasında xəta meydana gələr, yumurta və ya spermada səhvliklə 1 yerinə 2 ədəd 21 ci xoromozom olarsa, mayalanma baş verdikdən sonra d&ouml;ldə 2 yerinə 3 ədəd 21 ci xoromozom olar. &nbsp;Daun sindromu 21 ci xoromozomun 2 yerinə 3 ədəd olmasıdır. Buna g&ouml;rə Daun sindromu eyni zamanda Trisomiya 21 adlanır. </span></p>\r\n\r\n<p dir="ltr"><strong>Daun sindromu risk faktorları </strong></p>\r\n\r\n<ol>\r\n\t<li dir="ltr" style="list-style-type:decimal">\r\n\t<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:13.999999999999998pt">Ana yaşı &nbsp;: </span><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Ana yaşı artdıqca &nbsp;xromosomların meyoz prosesi boyunca səhv b&ouml;l&uuml;nmə ehtimalı artır.Beləliklə, Daun sindromu riski &nbsp;qadının yaşı artdıqca artır. Kişi yaşının he&ccedil; bir əhəmiyyəti yoxdur .</span></p>\r\n\t</li>\r\n</ol>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:13.999999999999998pt">Ana yaşı &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Daun sindromlu uşaq sahibi olma riski </span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:13.999999999999998pt">20 &nbsp;</span><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1/1600</span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">25 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1/1300</span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">30 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1/1000</span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">35 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1/365 </span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">40 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1/90</span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">45 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1/30 </span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol start="2">\r\n\t<li dir="ltr" style="list-style-type:decimal">\r\n\t<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">&Ouml;ncəki hamiləlikdə Daun sindromlu &nbsp;uşaq doğmaq bu hamiləlikdə təkrar Daun sindromlu uşağa hamilə qalma ehtimalını sadəcə 1 faiz artırır.Yəni əgər 1 ci hamiləlikdə Daun sindromlu uşaq doğulmuşdursa, &ccedil;ox qorxmağa dəyməz .</span></p>\r\n\t</li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol start="3">\r\n\t<li dir="ltr" style="list-style-type:decimal">\r\n\t<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Ana və ya ata 21 ci xromosom translokasiyasının daşıyıcısıdırsa , &nbsp;Daun sindromlu uşaq olma ehtimalı artır. </span></p>\r\n\t</li>\r\n</ol>\r\n\r\n<p><br />\r\n&nbsp;</p>\r\n\r\n<p dir="ltr"><strong>Skrininq testləri (</strong><strong>ikili,&uuml;&ccedil;l&uuml; və d&ouml;rdl&uuml; testlər)</strong></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Daun sindromu &nbsp;ən &ccedil;ox rast gəlinən xromosom patologiyası olduğu &uuml;&ccedil;&uuml;n və m&uuml;alicəsi olmadığı &uuml;&ccedil;&uuml;n &nbsp;hamiləliyin ilk 20 həftəsi i&ccedil;ində aşkarlanmağa &ccedil;alışılır. </span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Bəs Daun sindromun diaqnozu necə qoyulur ? Ana bətnində Daun sindromun dəqiq diaqnozunu qoymaq &uuml;&ccedil;&uuml;n , kariotip analizləri etmək lazımdır. Kariotip analizləri diagnostik olub, dəqiq diaqnoz verirlər. &nbsp;Ən &ccedil;ox istifadə ediləni Amniosintezdir. Amniosintez hamiləliyin 15-20 ci həftələri arasında aparılır. Lakin bu testin &ccedil;ox b&ouml;y&uuml;k iki mənfi x&uuml;susiyyəti var. </span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">1 . &nbsp;Amniosintez sirasında &nbsp;1/300 ehtimalı ilə d&uuml;ş&uuml;k riski m&ouml;vcuddur .1/1000 riski olan Daun sindromu &uuml;&ccedil;&uuml;n 1/300 riski olan amniosintez etmək &ccedil;ox məntiqli g&ouml;rsənmir .</span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">2. &nbsp;Amniosintez bahalıdır .</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Bu &nbsp;iki &ccedil;atışmazlıq səbəbi ilə ,hal hazırda hər hamiləyə amniosintez əvəzinə skrininq testlər təklif edilir. Skrininq testləri Daun sindromun dəqiq diaqnozunu qoymayıb , hamilənin Daun sindromlu uşaq sahibi olma riskini təyin &nbsp;edir. Yəni dəqiq diaqnoz vermir, lakin riskli hamilələri təyin edir. Beləliklə, gərəksiz amniosintezlərdən qa&ccedil;ınılır və xəstə bilgi sahibi olur. Skrininq testləri amniosintezdən dəfələrlə ucuz olub, invaziv deyillər.Yəni skrininq testlər d&uuml;ş&uuml;k riski daşımır. &nbsp;&nbsp;Hal hazırda istifadə edilən skrininq testlər 2-li test, 3-l&uuml; və ya 4-l&uuml; testdir .</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p dir="ltr"><strong>İkili test </strong></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">İkili test Daun sindromu &nbsp;skrininqi &uuml;&ccedil;&uuml;n 11 həftə 6 g&uuml;n- 14 həftə arası aparılan bir testdir. Bu test sırasında ultrasəs m&uuml;ayinəsi ilə &nbsp;uşağın </span><strong>burun s&uuml;m&uuml;y&uuml;</strong><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt"> &nbsp;və </span><strong>ənsə qalınlığı</strong><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt"> &ouml;l&ccedil;&uuml;l&uuml;r. Burun s&uuml;m&uuml;y&uuml; ki&ccedil;ik və ya yoxdursa, eyni zamanda ənsə qalınlığı artıbsa &nbsp;Daun sindromu riski artır Eyni zamanda hamilə qan verib </span><strong>, PAPP-A</strong><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt"> və</span><strong> HCG</strong><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt"> baxılır.. &nbsp;Daha sonra b&uuml;t&uuml;n bu datalar &nbsp;spesifik proqramdan ke&ccedil;ib bizə Daun sindromun riskini verır. İkili testin &nbsp;həssaslığı 93%- dir .</span></p>\r\n\r\n<p dir="ltr"><u>&nbsp;&nbsp;İkili test sırasında ultrasəs m&uuml;ayinəsi olduğu &uuml;&ccedil;&uuml;n dəqiqliyi &uuml;&ccedil;l&uuml; və ya d&ouml;rdl&uuml; testə g&ouml;rə daha y&uuml;ksəkdir. Yəni ikili test daha &uuml;st&uuml;nd&uuml;r. &nbsp;2-li test vermiş hamilə 3-l&uuml; və ya 4-l&uuml; test verməli deyil. </u></p>\r\n\r\n<p><br />\r\n&nbsp;</p>\r\n\r\n<p dir="ltr"><strong>3- l&uuml; və 4-l&uuml; testlər</strong></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Hamilə &nbsp;2- li test &uuml;&ccedil;&uuml;n gecikdiyi zaman, 3 - l&uuml; və ya 4- l&uuml; test verilə bilə bilər. 3 -l&uuml; və 4-l&uuml; testdə ultrasəs m&uuml;ayinəsi yoxdur &nbsp;və sadəcə ana qanından baxılır. 3-l&uuml; test sırasında HCG , E3 və AFP , 4 l&uuml; test sırasında HCG ,E3 ,AFP və inhibin baxılır. &nbsp;Qan nəticələri proqramdan ke&ccedil;irilib , bizə Daun sindromun riski verir. 3 -l&uuml; testin əhssaslığı 75% , 4- l&uuml; testin həssaslığı 80% - dir.</span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Son 10 il ərzində &nbsp;&nbsp;Daun sindromun diaqnostikasında &nbsp;&nbsp;yeni testlər təklif olunmuşdur. Bu test ana qanında &nbsp;d&ouml;l DNA -nın aşkarlanmasına əsaslanır. İnvaziv olmayan bu test sırasında hamilədən qan alınır və qanda d&ouml;l h&uuml;ceyrələri aşkarlanır. Beləliklə d&ouml;ldəki olan genetik patologiyalar yoxlana bilər . Bu test &nbsp;dəqqi diaqnoz verməsədə , dəqiqliyi ( həssaslığı ) &ccedil;ox y&uuml;ksəkdir . (99%) Tək mənfi x&uuml;susiyyəti hal- hazırda &ccedil;ox baha olmasıdır. Xəstəxanalarda. NİPT, Prenatal və s. adlarla təklif olunur </span></p>	f		f
5	/hamiləlikdə-infeksiyalar/	Hamiləlikdə infeksiyalar	<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">&nbsp;&nbsp;&nbsp;&nbsp;Hamiləlikdə infeksiyaların gedişi &nbsp;&ccedil;ox vaxt hamilə olmayanalara bənzəyir. Bəzi hallarda daha ağır ke&ccedil;ə bilir. &nbsp;Lakin hamilələrdə infeksiyalara yanaşma və m&uuml;alicə bəzi hallarda problem təşkil edir. Buna səbəb infeksiyaların m&uuml;alicəsində istifadə ediləcək bəzi dərmanların d&ouml;l &uuml;&ccedil;&uuml;n zərərli ola biləcəyi kimi , infeksiyalara səbəb olan mikroblar cifti ke&ccedil;ərək d&ouml;ldə ciddi eybəcərliklərə və &nbsp;xəstəliklərə yol a&ccedil;masıdır. </span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Hamiləlik zamanı &nbsp;problemlərə yol a&ccedil;an əsas &nbsp;TORCH infeksiyalarıdır. &Ccedil;&uuml;nki &nbsp;bu patogenlər d&ouml;lə ke&ccedil;ib d&ouml;ldə eybəcərliklərə və xəstəliklərə yol a&ccedil;a bilirlər.Torch infeksiylarina Toxoplasmosis, Rubella, Cytomegalovirus, Herpesdir və digər infeksiyalardır. İndi bir-bir bu infeksiyaları daha yaxından &ouml;yrənək. </span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Toxoplazma</span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Toxoplasmosis, məcburi h&uuml;ceyrə i&ccedil;i &nbsp;bir parazit olan toxoplasma gondinin səbəb olduğu infeksiyadır. </span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Doğumsal (konjenital) toksoplasma infeksiyasının sıxlığı &nbsp;1-10 /10000 dir. </span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Risk faktorları : yaxşı bişirilməmiş ət məhsulları , Eyni zamanda &nbsp;it, pişik və ey heyvanların nəcisi bulaşmış tərəvəz, meyvə əsas mənbəyidir. </span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Toksoplazma paraziti cift vasitəsilə &nbsp;d&ouml;lə ke&ccedil;ır. Hamiləlik həftəsi b&ouml;y&uuml;d&uuml;kcə parazitin ke&ccedil;mə ehtimalı artır. Lakin &nbsp;hamiləlik b&ouml;y&uuml;d&uuml;kcə infeksiyanın şiddəti azalır. Yəni 34-c&uuml; həftədə infeksiyanın ke&ccedil;mə ehtimalı , &nbsp;12 həftədən daha &ccedil;oxdur. Amma 12 -ci həftədə meydana gələ biləcək infeksiya daha ağır və d&ouml;l &uuml;&ccedil;&uuml;n zərərlidir. </span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Klinika: Hamilə qadında &nbsp;y&uuml;ksək hərarət, halsızlıq, yorğunluq, limfodenopatiya, &nbsp;fotofobiya və boyunda ağrılı limfa d&uuml;y&uuml;nləri m&ouml;vcud olur. &nbsp;Hamiləlik abort, vaxtından əvvəl doğuş və ya ana bətnində &ouml;l&uuml; d&ouml;l ilə nəticələnə bilər. D&ouml;ldə meydana gələn konjenital toksoplazmozun &nbsp;85%- i asimptomatikdir və ya non-spesifikdir. Ana bətnində ən sıx yaranan patologiya korioretinitdir. Bundan əlavə, karlıq, hidrosefaliya, mental gerilik, &nbsp;hepatit, pnevmoniya meydana gələ bilər. </span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Ultrasəs m&uuml;ayinəsi sırasında , &nbsp;beyin mədəciklərin genişlənməsi ( ventrikulomeqaliya ) , mikrsefaliya, beyin və qaraciyərdə &nbsp;kalsifikasiyalar, qarında maye izləndi. </span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Diaqnoz : &nbsp;hamilədə infeksiyanın diaqnozunu qoymaq &uuml;&ccedil;&uuml;n &nbsp;əkilmə alına bilər. Eyni zamanda serolojik təhlillər də yardımcı olacaqdır. </span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">İg G m&uuml;bət &nbsp;, İg M mənfidirsə , bu ke&ccedil;irilmiş infeksiyanı g&ouml;stərir və he&ccedil; bir qorxusu yoxdur . &nbsp;İg G mənfi , İg M m&uuml;sbətdirsə , bu aktiv infeksiyanı g&ouml;stərir. Aktiv infeksiya d&ouml;l &uuml;&ccedil;&uuml;n zərərli ola biləcək olan haldır.İg M infeksiyadan bir həftə sonra meydana &ccedil;ıxır və bir il m&uuml;sbət qalır. &nbsp;İg G və İg M hər ikisi m&uuml;sbətdirsə, bu ya xronik infeksiyanın aktivləşməsi ya da, &uuml;st&uuml;ndən 1 -2 ay artıq ke&ccedil;miş aktiv infeksiyadır. </span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Avidite testi &nbsp;: həm İg G, həm İg &nbsp;M- in m&uuml;sbət olduğu halda infeksiyanın xronik və ya aktiv olduğunu g&ouml;stərmək i&ccedil;in edilir. </span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">&Uuml;mumiyyətlə hamilədə he&ccedil; bir şikayət yoxdursa, rutin Toksoplazmoz analizlərin alınmasına ehtiyac yoxdur. Yəni hər hamilədə toksoplazma analizinin baxılmasına ehtiyac yoxdur. </span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">D&ouml;lə infeksiyanın ke&ccedil;mə ş&uuml;bhəsi m&ouml;vcuddursa, amniosintez ilə amnion maye alınıb, PZR ilə toksoplazma baxılmalıdır. </span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Toksoplazmozun m&uuml;alicəsi , d&ouml;lə ke&ccedil;id olub- olmamasından asılı olaraq dəyişir. &nbsp;M&uuml;alicədə Spiromisin antibiotiki istifadə edilir.</span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">26- ci həftədən &ouml;ncə d&ouml;ldə toksoplazma infeksiyası amniosintez &nbsp;ilə aşkarlanarsa və ya serologiyası pozitiv olub, ultrasəs m&uuml;ayinəsi sırasında ciddi fəsadlar izlənərsə, hamiləliyi sonlandırmaq &nbsp;variantı təklif edilə bilər. </span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Rubella (Məxmərək)</span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Togovirus ailəsindən olan Rubella virusu səpkili infeksiyaya səbəb &nbsp;olur . Xəstə insanlar virusu tənəff&uuml;s yolu ilə yayırlar. Virusa qarşı peyvənd olmayan və ya immuniteti olmayan xəstələr risk &nbsp;qrupundadır. </span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Virus ciftlə d&ouml;lə ke&ccedil;ə bilər.Ən &ccedil;ox yoluxma hamiləliyin 1-ci trimestrda &nbsp;baş verir. </span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Tənəff&uuml;s yolu ilə ke&ccedil;ən infeksiya, &nbsp;2-3 həftəlik prodromal perioddan sonra, simptomatik hala gəlir. &nbsp;Halsızlıq, hərarət, baş ağrısı və konyuktivit lə birlikdə &uuml;zdən başlayıb , bədən , əl və ayaqlara yayılan &nbsp;və 3 g&uuml;n davam edən makuler səpkilər meydana gəlir. Qulaq altı ,ənsə altı b&ouml;y&uuml;m&uuml;ş limfa d&uuml;y&uuml;nləri xarakteristikdir .</span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Hamiləlik zamanı d&uuml;ş&uuml;k və vaxtından əvvəl doğuma səbəb &nbsp;ola bilər. </span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Bətndaxili d&ouml;lə təsiri : Səpkilər, hamiləliyin 12-ci g&uuml;ndən 12-ci həftəyə qədər &nbsp;meydana gəlibsə , d&ouml;l infeksiyadan təsirlənəcək və 81% anadangəlmə Rubella sindromu meydana gələcək .Anadangəlmə Rubella sindromu &nbsp;&nbsp;Rubella virusunun bətndaxili d&ouml;vrdə d&ouml;ldə yaratdığı anomaliyalardan ibarətdir və korioretinit, katarakta, hidrosefaliya, əqli gerilik , karlıq və &uuml;rək xəstəliklərindən ibarətdir. </span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Rubella virusundan təsirlənmiş d&ouml;ldə ultrasəs m&uuml;ayinəsi sırasında &nbsp;mikrosefaliya, beyində su yığılması, katarakta, &uuml;rək qapaqlarında daralma və digər &uuml;rək defektləri izlənə bilər. </span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Diaqnoz: &nbsp;Anada diaqnozu qanda Rubella İg G və İg M &nbsp;baxılaraq qoyulur. D&ouml;ldə dəqiq diaqnoz qoymaq &uuml;&ccedil;&uuml;n &nbsp;amniosintezlə alınan d&ouml;lyanı mayedə PZR ilə virus DNT- si baxılmalıdır. &nbsp;</span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Rubella İg G hamiləlikdən əvvəl baxılmalıdır. İg G mənfi olanlara peyvənd t&ouml;vsiyyə edilir. Peyvənd vurulduqdan sonra &nbsp;qadın 3 ay qorunmalıdır. Hamiləlik vaxtı Rubella İg G baxılmasının &ccedil;ox bir mənası yoxdur .</span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">İg G m&uuml;bət &nbsp;, İg M mənfidirsə , bu ke&ccedil;irilmiş infeksiyanı g&ouml;stərir və he&ccedil; bir qorxusu yoxdur . &nbsp;İg G mənfi , İg M m&uuml;sbətdirsə , bu aktiv infeksiyanı g&ouml;stərir. Aktiv infeksiya d&ouml;l &uuml;&ccedil;&uuml;n zərərli ola biləcək olan haldır. İg M infeksiyadan bir həftə sonra meydana &ccedil;ıxır və bir il m&uuml;sbət qalır. &nbsp;İg G və İg M hər ikisi m&uuml;sbətdirsə, bu ya xronik infeksiyanın aktivləşməsi ya da , &uuml;st&uuml;ndən 1 -2 ay artıq ke&ccedil;miş aktiv infeksiyadır. </span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Avidite testi &nbsp;: həm İg G həm İg M in m&uuml;sbət olduğu &nbsp;halda infeksiyanın xronik və ya aktiv olduğunu g&ouml;stərmək i&ccedil;in edilir. </span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Əgər d&ouml;l &nbsp;virusdan təsirlənibsə və ciddi anomaliyalar varsa hamiləliyi sonlandırmaq t&ouml;vsiyyə edilə bilər. </span></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Sitomegalovirus (CMV)</span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Sitomegalovirus &nbsp;ana bətnində d&ouml;l infeksiyaların ən sıx səbəbidir . &nbsp;Qadınların təqribi 80% -ində sitomegalovirusa qarşı &nbsp;immunitet var və sadəcə 1.2 % hallarda infeksiya meydana gəlir. </span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Hamilədə aktiv primer infeksiya olduğu təqdirdə &nbsp;ciftlə ke&ccedil;iş ehtimalı %15 dir. Təkrar infeksiya ke&ccedil;irən xanımlarda d&ouml;lə ke&ccedil;mə ehtimalı cəmisi 0.5 % - dir. Əgər Hbe Ag m&uuml;bətdirsə , d&ouml;lə ke&ccedil;mə ehtimalı %15-20 dir. Bundan əlavə olaraq, uşaqlıq yolundan və ana s&uuml;d&uuml; ilə ke&ccedil;mə m&uuml;mk&uuml;d&uuml;r. </span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Qadında infeksiya meydana gəldiyi zaman,y&uuml;ksək hərarət, halsızlıq, &nbsp;limfa d&uuml;y&uuml;nlərinin şişməsi baş verir. </span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Əgər virus d&ouml;lə ke&ccedil;ərsə , &nbsp;d&uuml;ş&uuml;k və bətndaxili inkişaf ləngiməsi &nbsp;də ola bilər. Bundan əlavə olaraq, d&ouml;ldə mikrosefaliya, &nbsp;karlıq, əqli inkişaf geriliyi , qaraciyər və dalağın b&ouml;y&uuml;məsi , sarılıq ola bilər. </span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Anada &nbsp;diaqnoz İG g və ig M baxılaraq qoyulur. &nbsp;D&ouml;ldə isə diaqnoz &uuml;&ccedil;&uuml;n d&ouml;lyanı maye amniosintezlə alınıb, PZR ilə virusa baxılır. </span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">İg &nbsp;G m&uuml;bət &nbsp;, İg M mənfidirsə , bu ke&ccedil;irilmiş infeksiyanı g&ouml;stərir və he&ccedil; bir qorxusu yoxdur . &nbsp;İg G mənfi , İG M m&uuml;sbətdirsə , bu aktiv infeksiyanı g&ouml;stərir.Aktiv infeksiya d&ouml;l &uuml;&ccedil;&uuml;n zərərli ola biləcək olan haldır. İg M infeksiyadan bir həftə sonra meydana &ccedil;ıxır və bir il m&uuml;sbət qalır. &nbsp;İg G və İg M hər ikisi m&uuml;sbətdirsə, bu ya xronik infeksiyanın aktivləşməsi yada , &uuml;st&uuml;ndən 1 -2 ay ke&ccedil;miş aktiv infeksiyadır. </span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Avidite testi &nbsp;: həm İg M, həm ig M in m&uuml;sbət olduğu &nbsp;halda infeksiyanın xronik və ya aktiv olduğunu g&ouml;stərmək i&ccedil;in edilir. </span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Anada əgər &nbsp;primer infeksiyadırsa, Gansiklovir &nbsp;dərmanı istifadə edilə bilər.</span></p>\r\n\r\n<p><br />\r\n&nbsp;</p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Herpes Simplex Virusu &nbsp;(HSV)</span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">İki tip Herpes Simplex virusu bilinməktədir. TİP 1 ağız ətrafı və dodaqları tutur. Tip2 cinsiyyət dodaqlarının herpesi kimi tanınır . Amma genital infeksiyaların 30%-i &nbsp;Tip 1 HSV tərəfindən t&ouml;rədilir. Əhalinin 30%-də HSV İg G m&uuml;sbətdir. Yəni bu virusla qarşılaşıb. D&ouml;ldə eybəcərliklərə əsasən tip 2 HSV səbəb olur. </span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Bu virus uzun m&uuml;ddət bədəndə asimptomatik vəziyyətdə qala bilir. He&ccedil; &nbsp;bir şikayət vermədən bədəndə duran virus, əlverişli şərait yaranarsa u&ccedil;uq meydana gətirir. </span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">M&uuml;alicəsində Asiklovir dərmanı istifadə edilir. &nbsp;Asiklovirin d&ouml;lə he&ccedil; bir zərərli g&ouml;stərilməmişdir. </span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Qadın ilk dəfə bu virusla hamiləlikdə &nbsp;ilk 3 ayda qarşılaşıbsa d&uuml;ş&uuml;k riski artmır. Vaxtindan əvvəl doğuş riski x&uuml;susən hamiləliyin sonuna doğru artır .</span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Virus d&ouml;lə &nbsp;ancaq hamilə bu virusla ilk dəfə hamiləlikdə yoluxubsa &nbsp;zərər verir. Təkrar infeksiyalar zamanı d&ouml;lə zərər gəlmə ehtimalı &ccedil;ox c&uuml;zidir. </span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Virus d&ouml;lə ke&ccedil;ərsə, d&ouml;ldə , bətndaxili inkişaf ləngiməsi , beyində maye toplanması , &nbsp;beyin və cift daxili kalsifikasiyalar meydana gələ bilər. Virus d&ouml;ldə g&ouml;zdə konyuktivit və retinit , &nbsp;pnevmoniya, meningit yarada bilər. </span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Aktiv genital hepres yaraları olan hamilələrdə normal doğuş t&ouml;vsiyyə edilmir. Qeysəriyə kəsiyi tam ke&ccedil;idin qarşısını almasa da riski &ccedil;ox azaldır. </span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Diaqnoz Herpes simplex virus tip 1 və tip 2 İg M və İgG &nbsp;ilə qoyulur.</span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">İg G m&uuml;bət &nbsp;, İg M mənfidirsə , &nbsp;bu ke&ccedil;irilmiş infeksiyanı g&ouml;stərir və he&ccedil; bir qorxusu yoxdur . &nbsp;İg G mənfi , İg M m&uuml;sbətdirsə , bu aktiv infeksiyanı g&ouml;stərir. Aktiv infeksiya d&ouml;l &uuml;&ccedil;&uuml;n zərərli ola biləcək olan haldır.İg M infeksiyadan bir həftə sonra meydana &ccedil;ıxır və bir il m&uuml;sbət qalır. &nbsp;İg G və İg M hər ikisi m&uuml;sbətdirsə, bu ya xronik infeksiyanın aktivləşməsi yada , &uuml;st&uuml;ndən 1 -2 ay artıq ke&ccedil;miş aktiv infeksiyadır. </span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Avidite testi &nbsp;: həm ig G həm ig M in m&uuml;sbət olduğu &nbsp;halda infeksiyanın xronik və ya aktiv olduğunu g&ouml;stərmək i&ccedil;in edilir. </span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Hamiləlikdə Su &Ccedil;i&ccedil;əyi (Varicella Zoster)</span></p>\r\n\r\n<p dir="ltr"><span style="background-color:transparent; color:#000000; font-family:Calibri; font-size:11pt">Su &ccedil;i&ccedil;əyinə Varisella Zoster virusu &nbsp;səbəb olur. Virus təmas və ya tənəffus yolu ilə yayılır. </span></p>\r\n\r\n<p dir="ltr"><span style="background-color:#ffffff; color:#1d2129; font-family:Calibri; font-size:11pt">0-12 həftə arası olan bir hamiləmiz su&ccedil;i&ccedil;əyi ke&ccedil;irərsə uşaqda(d&ouml;ldə) bir problem olma ehtimalı 0.4 %-dir . Yəni &ccedil;ox- &ccedil;ox azdır. Abort edilməz !!! &nbsp;Sadəcə ultrasəs ilə nəzarət edilər.&nbsp;</span><br />\r\n<span style="background-color:#ffffff; color:#1d2129; font-family:Calibri; font-size:11pt">12-20 həftə arası hamiləmiz Su &Ccedil;i&ccedil;əyi ke&ccedil;irərsə uşaqda problem olma ehtimalı 2 % -dir. &nbsp;Ən riskli period bu olmasına baxmayaraq , cəmi %2- dir. Abort edilməz , lakin ultrasəs ilə ciddi şəkildə nəzarət edilər.</span><br />\r\n<span style="background-color:#ffffff; color:#1d2129; font-family:Calibri; font-size:11pt">20 həftədən b&ouml;y&uuml;k hamilələrimizdə uşaqda bir &nbsp;problem olma ehtimalı isə yoxdur !!!! Yəni tamaməm uşaq &uuml;&ccedil;&uuml;n təhl&uuml;kəsizdir !! Abort edilməz !!!!</span></p>\r\n\r\n<p dir="ltr"><span style="background-color:#ffffff; color:#1d2129; font-family:Calibri; font-size:11pt">Ana Su &Ccedil;i&ccedil;əyi ke&ccedil;irirsə, &nbsp;dəstək m&uuml;alicəsi təyin edilir .</span></p>\r\n\r\n<p dir="ltr"><span style="background-color:#ffffff; color:#1d2129; font-family:Calibri; font-size:11pt">Virus d&ouml;lə ke&ccedil;ibsə , ultrasəs m&uuml;ayinəsi zamanı, &nbsp;əl və ayaqlarda qısalıq ,mikrosefaliya, hidrosefaliya, katarakta &nbsp;və inkişaf ləngiməsi izlənir. </span></p>\r\n\r\n<p><br />\r\n<br />\r\n&nbsp;</p>	f		f
6	/mikrocip-ile-suni-mayalanma/	Mikroçip üsulu ilə Süni mayalanma	<p>&nbsp;&nbsp;&nbsp;&nbsp;Hal hazırda sonsuzluğu olan və s&uuml;ni mayalanmaya m&uuml;raciət edənlərin yarısında səbəb kişi faktorudur .<br />\r\nSperm sayısında yetərsizlik və hərəkətsizliyi hamiləlik şansını azaldır. Lakin son zamanlarda Harvard Tibb<br />\r\nFakultəsində aşkarlanan microchip &uuml;sulu bu problem &uuml;&ccedil;&uuml;n &ccedil;ox &uuml;mid verici bir həll yolu kimi g&ouml;rsənir.<br />\r\nSperm h&uuml;ceyrələrindəki genetk material bəzi hallarda &ccedil;ox ciddi &ccedil;əkildə təhriş ola bilər. Bu da sonsuzluğa<br />\r\nsəbəb olur . Eyni zamanda aşılama və ya s&uuml;ni mayalanma zamanı m&uuml;vəffəqiyyətin d&uuml;şməsinə səbəb ola<br />\r\nbilər. Sperm Dna fragmentasiya nisbəti və ya indexi adlanan , və Sperm h&uuml;ceyrələrində genetik materialın<br />\r\nzədələnmə nisbətini g&ouml;stərən analiz &ccedil;ox bahalı olub, &ccedil;ox az mərkəzdə aparılır. Bizim s&uuml;ni mayalanma<br />\r\nlaboratoriyamızda bu test də edilir .<br />\r\nMicrochip metodu tətbiq edildiyi hallarda, sperm h&uuml;ceyrələri arasında DNA ( genetik ) material ən sağlam<br />\r\nolanlar se&ccedil;ilir. Beləliklə s&uuml;ni mayalanma və ya aşılama zamanı m&uuml;vəffəqiyyət nisbəti artır. Microchip &uuml;sulu<br />\r\nhəm s&uuml;ni mayalanma zamanı istifadə edilə bildiyi kimi , həm də aşılama zamanı da &ccedil;ox m&uuml;vəffəqiyyətlidir.<br />\r\nMikro&ccedil;ip &uuml;sulu zamanı sperma mikro kanallardan ke&ccedil;ərkən m&uuml;əyyən proseslərə məruz qalıb keyfiyyətli və<br />\r\nDNA materialı sağlam olanlar ayrılır. Bu ayrılma zamanı spermalar zərərli olan oksidativ molekullar ortaya<br />\r\n&ccedil;ıxmır .<br />\r\nYeni bir &uuml;sul olan bu microchip metodu ancaq d&uuml;nyadaki yenilikləri izləyən laboratoriyalarda tətbiq edilir.<br />\r\nBizim s&uuml;ni mayalanma laboratoriyasında bu &uuml;sul geniş istifadə edilib, &uuml;z g&uuml;ld&uuml;r&uuml;c&uuml; nəticələr verir.</p>	f		f
7	/miomalar/	Miomalar (fibromioma)	<p>&nbsp;&nbsp;&nbsp;&nbsp;Miomalar uşaqlıq əzələsindən meydana gəlir. Mioma ilə kist arasında fərq<br />\r\n,mioma uşaqlığın, kista isə yumurtalığın xoş xassəli şişidir.<br />\r\nMiomalar hər 4-5 qadından birində rastlanır. Sıxlıqla eyni anda bir ne&ccedil;ə mioma<br />\r\naşkarlanır. Miomalar ən &ccedil;ox 30-40 yaşlarında rastlanır. Klimaks sonrası<br />\r\nmiomalar adətən ki&ccedil;ilir.<br />\r\nMiomaların meydana gəlməsinin səbəbi tam bilinməsədə ,estrogenin<br />\r\nmiomaların b&ouml;y&uuml;məsinə səbəb olduğu bilinir. Hamiləlikdə ,qanda estrogen<br />\r\nsəviyyəsi artdığı &uuml;&ccedil;&uuml;n miomalar b&ouml;y&uuml;y&uuml;r. Menopauza sonrası qandaki estrogen<br />\r\nsəviyyəsi azaldığı &uuml;&ccedil;&uuml;n miomalar ki&ccedil;ilir.<br />\r\nMiomalar qanaxma , ağrı, d&uuml;ş&uuml;k, sonsuzluq, qarında ələ gələn şişliyə səbəb<br />\r\nolurlar. Amma bir &ccedil;ox mioma he&ccedil; bir şikayət biruzə vermir. Bu səbəblə,<br />\r\nqadınların 75%-i miomasının olduğunu bilmirlər.<br />\r\nMiomaların yerləşməsinə g&ouml;rə bir ne&ccedil;ə n&ouml;v&uuml; m&ouml;vcuddur:<br />\r\n1. Subseroz- Uşaqlıq əzələsinin ən bayır hissəsində.Əksər hallarda he&ccedil; bir<br />\r\nşikayətə səbəb olmurlar. Lakin &ccedil;ox b&ouml;y&uuml;sələr, qasıqlarda ağrı, sıx sıx sidiyə<br />\r\n&ccedil;ıxma və qonşu organlara təzyiqə bağlı şikayətlərə səbəb ola bilərlər.<br />\r\n2. İntramural- Uşaqlıq əzələsnin i&ccedil;ində yerləşirlər. Qanaxma , ağrı, d&uuml;ş&uuml;k ,<br />\r\nciftin vaxtından əvvəl ayrılmasına səbəb ola bilərlər.<br />\r\n3. Submukoz- Ən dərində, uşaqlıq boşluğunun i&ccedil;ində yerləşib. &Ccedil;ox vaxtı ciddi<br />\r\nqanaxmaya səbəb olurlar. Submukoz miomalar, hamiləliyin meydana gəlməsi<br />\r\nvə s&uuml;ni mayalanma &uuml;&ccedil;&uuml;n ciddi problem yaradırlar.<br />\r\nMiomalar saplı olduqları halda ,&ouml;z ətrafında burulması nəticəsində ani, &ccedil;ox<br />\r\nşiddətli ağrıya səbəb olurlar.<br />\r\nBəzən miomalar sidik axarına basqı yaradıb, b&ouml;yrəklərdə ciddi zərərə yol a&ccedil;a<br />\r\nbilirlər.<br />\r\nMiomalar , x&uuml;susən submukoz olduğu təqdirdə , d&uuml;ş&uuml;yə səbəb ola bilərlər.<br />\r\nSubseroz miomalar d&uuml;ş&uuml;yə səbəb olmur.<br />\r\nYenədə submukoz miomalar ,uşaqlıq boşluğunu doldurduğu &uuml;&ccedil;&uuml;n və d&ouml;l&uuml;n<br />\r\nimplantasiyasına mane olduğu &uuml;&ccedil;&uuml;n sonsuzluğa səbəb ola bilərlər.<br />\r\nHamiləlik planlayan bir xəstədə ,mioma aşkarlanarsa əməliyyat olmalıdırmı?<br />\r\nBu miomanın yerləşdiyi yerdən və &ouml;l&ccedil;&uuml;s&uuml;ndən asılıdır. Miomalar hamiləlikdə<br />\r\nestrogen təsiri altında b&ouml;y&uuml;y&uuml;rlər və ciddi ağrıya səbəb ola bilərlər. Eyni<br />\r\nzamanda intramural miomalar vaxtından əvvəl doğuşa, ciftin ayrılmasına səbəb<br />\r\nola bilərlər. Buna g&ouml;rə əksər hallarda, hamiləlik planlayan xəstələrə,<br />\r\nm&uuml;əyyən &ouml;l&ccedil;&uuml;dən b&ouml;y&uuml;k intramural və submukoz miomaları əməliyyatla<br />\r\n&ccedil;ıxarılmasını t&ouml;vsiyyə edirik .<br />\r\nMioması olan hamilələr, əgər mioma doğuş yolunu tutmur və ya doğuşa mane<br />\r\nolmursa, normal doğuş edə bilərlər. Əgər qeysəriyyə qərarı alınıbsa,<br />\r\nəməliyyat sırasında miomanın &ccedil;ıxarılması ciddi qanaxma riski səbəbi ilə<br />\r\nt&ouml;vsiyyə edilmir.<br />\r\nMioma əməliyyatı ke&ccedil;irən xəstə, əgər mioma subserozdursa, yəni əzələnin<br />\r\n&ccedil;ox ki&ccedil;ik &ccedil;&ouml;l hissəsinə tikiş atılıbsa , normal doğuş edə bilər. Digər hallarda<br />\r\nuşaqlığın tikiş sahəsindən cırılma ehtimalını g&ouml;zə alaraq , qeysəriyyə olmalıdır.<br />\r\nMiomaların xər&ccedil;əngə d&ouml;nmə ehtimalı &ccedil;ox-&ccedil;ox azdır ya da &uuml;mumiyyətlə sıfırdır.<br />\r\nMiomaların m&uuml;alicəsi cərrahidir. Əməliyyat həm a&ccedil;ıq &uuml;sulla, həmdə<br />\r\nlaparoskopik aparıla bilər.Biz ş&ouml;bəmizdə, xəstənin istəyindən ,miomanın<br />\r\n&ouml;l&ccedil;&uuml;s&uuml;ndən və yerləşdiyi yerdən asılı olaraq həm laparoskopik, həm də a&ccedil;ıq<br />\r\nəməliyyatlar icra edirik.Gənc xəstələrdə , &ouml;zəlliklə ana olmaq istəyi davam</p>\r\n\r\n<p>edən xəstələrdə uşaqlığı qorumaq bizim &uuml;&ccedil;&uuml;n &ccedil;ox vacibdir. İndiyə kimi ,mioma<br />\r\nsəbəbi ilə əməliyyat edib ,uşaqlığı qorumaq istədiyimiz b&uuml;t&uuml;n xəstələrdə<br />\r\nbuna nail olmuşuq. Orqan qoruyucu cərrahiyyə bizim &uuml;&ccedil;&uuml;n &ccedil;ox vacibdir. Lakin<br />\r\nyaşı x&uuml;susən 50-dən y&uuml;ksək olan xanımlarda uşaqlığın tam &ccedil;ıxarılmasını<br />\r\nt&ouml;vsiyyə edirik .<br />\r\nSon illərdə, radyolojik bir metod olan mioma embolizasiyası ilə m&uuml;əyyən n&ouml;v<br />\r\nmiomaları m&uuml;alicə etmək olur. Lakin &ouml;zəlliklə qeyd etmək istəyirəm ki , bu<br />\r\nsadəcə bəzi miomalar &uuml;&ccedil;&uuml;n m&uuml;mk&uuml;nd&uuml;r.<br />\r\nXalq arasında &ccedil;ox yayılmış soğan suyu və ya digər xalq təbabəti &uuml;sulları ilə<br />\r\nmiomalar m&uuml;alicə olunmur.</p>	f		f
8	/nst/	NON -STRESS TEST (NST)	<p>&nbsp;&nbsp;&nbsp;&nbsp;D&ouml;l&uuml;n ana bətnində &uuml;rək d&ouml;y&uuml;nt&uuml;lərin aşkarlanıb qeyd edilməsidir .Yəni bir n&ouml;v&uuml; uşağın ana qarnındaki<br />\r\nElektrokardiogrammasıdır .<br />\r\nNST necə &ccedil;əkilir ?<br />\r\nAnanın qarnı ətrafında iki qayışla NST- nin qəbulediciləri bağlanır. Beləliklə uşağın &uuml;rək d&ouml;y&uuml;nt&uuml;ləri bu<br />\r\nqəbuledicilərdən biri vasitəsi ilə alətə &ouml;t&uuml;r&uuml;l&uuml;b qeyd edilir. Digər qəbuledici uşaqlıqda olan sancıları və<br />\r\nyığılmaları qeyd edir . NST qətiyyən ağrı vermir və uşağa he&ccedil; bir ziyanı yoxdur.<br />\r\nNST nə vaxt &ccedil;əkilməlidir ?<br />\r\nNST 28-ci həftədən sonra &ccedil;əkilə bilsədə , m&uuml;əyyən g&ouml;stərişlər i&ccedil;ində bəzi hallarda &ccedil;əkilir.<br />\r\n- Bətn daxili inkişaf ləngiməsi (uşağın yetəri qədər b&ouml;y&uuml;məməsi )<br />\r\n- Preeklampsiya ( hamiləliyin təzyiqli xəstəliyi )<br />\r\n- Hamiləliyin 40 həftəni ke&ccedil;məsi<br />\r\n- Uşaq hərəkətlərində azalma<br />\r\n- Diabet<br />\r\n- &Ouml;ncəki hamiləlikdə &ouml;l&uuml; doğuş hekayəsi<br />\r\n- &Uuml;rək xəstəlikləri<br />\r\n- D&ouml;ldə anemiya</p>\r\n\r\n<p>Yetişkin insanın dəqiqədə &uuml;rək d&ouml;y&uuml;nt&uuml;s&uuml; sayısı ortalama 60 -100 arası dəyişməkdədir .Ana bətnindəki<br />\r\nuşağın &uuml;rək d&ouml;y&uuml;nt&uuml; sayısı isə dəqiqədə 120 ilə 160 arası dəyişir.<br />\r\nUşağın ana bətnindəki hərəkətləri , d&ouml;l&uuml;n &uuml;rək hərəkətlərində m&uuml;əyyən artışa səbəb olur. B&ouml;y&uuml;k<br />\r\ninsandan fərqli olaraq uşağın &uuml;rək atım sayısı sabit deyil və saniyələr i&ccedil;ində dəyişikliklər g&ouml;stərir , məsələn<br />\r\n120 -130 vurğu/dəq arasında dəyişir. Buna variabillik deyilir.<br />\r\nEyni zamanda ana bətnində hərəkət edən uşağın &uuml;rək d&ouml;y&uuml;nt&uuml; sayısında ani artışlar olur. Məsələn &uuml;rək<br />\r\nd&ouml;y&uuml;nt&uuml; sayısı 120-130 ikən bir anda hərəkətlə 145 -150 vurğu/dəq qədər artır Bunun adı akselerasiyadır.<br />\r\nNST də akselerasiyalar varsa, bu uşağın &uuml;mumi vəziyətinin yaxşı olduğunu g&ouml;stərir. Yox əgər NST da<br />\r\nvariabillik olduğu halda akselerasiyalar yoxdursa, qorxmağa ehtiyac yoxdur . B&ouml;y&uuml;k ehtimal ilə uşağınız yatır<br />\r\nyada siz acsınız. M&uuml;əyyən m&uuml;ddət sonra yemək yeyib, NST- ni təkrarlamaq lazımdır.<br />\r\nNST da həkimlərin ən qorxduğu variabilliyin itməsidir. Yəni zaman i&ccedil;ində uşaq &uuml;rək d&ouml;y&uuml;nt&uuml;lərin<br />\r\nsayısının oynamayib sabit davam etməsi ,uşaqda ciddi hipoksiyanın g&ouml;stəricisi ola bilər. Bu bət daxili inkişaf<br />\r\nləngiməsi , preeklampsiya zamanı, ciftin vaxtından əvvəl ayrılması zamanı meydana gələ bilər.<br />\r\nVariabilliyin itməsi ilə bərabər uşağın &uuml;rək d&ouml;y&uuml;nt&uuml;lərində azalma olursa bunun adı deselerasiyadır. Və<br />\r\nd&ouml;ldə distressi g&ouml;stərir. Bu təcili Qeysəriyyə Kəsiyinə g&ouml;stərişdir.</p>\r\n\r\n<p>Qısacası,NST ginekoloqlar &uuml;&ccedil;&uuml;n riskli hamiləliklərin nəzarətində əvəzolunmaz yardımcıdır. NST nin<br />\r\nistifadəyə girməsi ilə bərabər ana bətnində uşaq &ouml;l&uuml;mləri &ccedil;ox-&ccedil;ox azalmışdır. Bunu &uuml;&ccedil;&uuml;n NST d&uuml;zg&uuml;n<br />\r\noxunmalıdır.Əks- halda hamiləliyə gərəksiz m&uuml;daxilələr ola bilər.</p>	f		f
10	/suni-mayalanma-nedir/	Süni mayalanma nədir ?	<p>&nbsp;&nbsp;&nbsp;&nbsp;Laboratoriya şəraitində yumurta h&uuml;ceyrəsi ilə sperm h&uuml;ceyrəsinin bir araya gətirilərək mayalanmaya<br />\r\nməruz buraxılmasına s&uuml;ni mayalanma deyilir. Əslində rus dilində olan `iskustvennoye opladotvoreniye`<br />\r\ns&ouml;z&uuml;n&uuml;n tərc&uuml;məsi olan s&uuml;ni mayalanma terminini , prosesi tam izah etmədiyi &uuml;&ccedil;&uuml;n &ccedil;ox sevmirik . Sanki<br />\r\nmeydana gələn uşaq s&uuml;nidir kimi bir təsəvv&uuml;r yaranıb, xəstələri qorxudur , gərəksiz suallar yaradır. İngilis<br />\r\ndilində olan `in vitro fertilisation` termini sanki daha a&ccedil;ıqlayıcı olub ,organizmdan kənarda mayalanma<br />\r\nmənasına gəlir. Son zamanlar s&uuml;ni mayalanma laboratoriyalarında İCSİ (mikroinyeksiya) prosesi həyata<br />\r\nke&ccedil;irilir. İCSİ zamanı sperm h&uuml;ceyrəsi mikroskop altında yumurta h&uuml;ceyrəsinin i&ccedil;inə inyeksiya edilir.<br />\r\nS&uuml;ni mayalanma , normal yoldan uşaq sahibi ola bilməyən c&uuml;tl&uuml;klərə tətbiq edilən ,sonsuzluq m&uuml;alicəsində<br />\r\nson evrədir. S&uuml;ni mayalanma m&uuml;təxəssis komanda tərəfindən həyata ke&ccedil;ırılən m&uuml;rəkkəb bir prosesdir. Bu<br />\r\nm&uuml;alicəyə başlamadan əvvəl c&uuml;tl&uuml;y&uuml;n b&uuml;t&uuml;n addımlar haqqında bilgiləndirilməsi m&uuml;alicənin<br />\r\nm&uuml;vəffəqiyyəti cəhətdən &ccedil;ox vacibdir. M&uuml;alicəyə başlamadan əvvəl c&uuml;tl&uuml;kdə həqiqətən s&uuml;ni mayalanmaya<br />\r\ng&ouml;stəriş olub olmadığı dəqiqləşdirilməlidir . Bəzi xəstələrdə s&uuml;ni mayalanmaya ehtiyac qalmadan , daha ucuz<br />\r\nm&uuml;alicə &uuml;sulu olan aşılama ilə problem həll edilə bilər .<br />\r\nS&uuml;ni mayalanmanın bir ne&ccedil;ə etapı var. Birincisi ,yumurtalıqlarda eyni zamanlı bir ne&ccedil;ə follikulun b&ouml;y&uuml;məsi<br />\r\n&uuml;&ccedil;&uuml;n yumurtalıqlarin kontrollu hiperstimulasiyasıdır. Folikullar istədiyimiz &ouml;l&ccedil;&uuml;lərə &ccedil;atdıqdan sonra ,<br />\r\n&ccedil;atlamalarını həyata ke&ccedil;iririk. Ovulasiya meydana gəldikdən sonra yumurtaların toplanması (OPU)baş verir.<br />\r\nOPU ağrılı bir proses olduğu &uuml;&ccedil;&uuml;n anesteziya(narkoz )altında həyata ke&ccedil;irilir.<br />\r\nToplanmış yumurtalar daha sonra embriologiya laboratoriyasına verilir. İStədiyimiz keyfiyyətdə olan<br />\r\nyumurtalar mayalandırlır və daha sonra x&uuml;susi alətlər i&ccedil;ində b&ouml;y&uuml;məsi &uuml;&ccedil;&uuml;n g&ouml;zlədilir. Daha sonra<br />\r\nmeydana gələn inkişafa g&ouml;rə 3 c&uuml; və ya 5 ci g&uuml;n embrİoları uşaqlığa k&ouml;&ccedil;&uuml;r&uuml;l&uuml;r. Buna Embrio transferi<br />\r\ndeyilir. Embrio transferindən 12 g&uuml;n sonra qanda hamiləlik testi yoxlanılır.<br />\r\nBəzi hallarda ,yumurtalıq hiperstimulasiyanin qarşısını almaq &uuml;&ccedil;&uuml;n və ya başqa səbəblərdən alınmış<br />\r\nyumurtalar və ya embriolar dondurulur. Dondurulma texnologiyası &ccedil;ox inkişaf etdiyi &uuml;&ccedil;&uuml;n artıq embriolar<br />\r\nbundan zərər g&ouml;rm&uuml;r .Daha sonra endometrium istədiyimiz şəkildə hazırlandıqdan sonra bu dondurulmuş<br />\r\nembriolar k&ouml;&ccedil;&uuml;r&uuml;l&uuml;r.Dondurulmuş embrio transferi ilə fresh embrio transferi arasında nəticələr baxımdan<br />\r\nciddi fərq aşkarlanmamışdır.</p>	f		f
\.


--
-- Name: django_flatpage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: db_user
--

SELECT pg_catalog.setval('public.django_flatpage_id_seq', 10, true);


--
-- Data for Name: django_flatpage_sites; Type: TABLE DATA; Schema: public; Owner: db_user
--

COPY public.django_flatpage_sites (id, flatpage_id, site_id) FROM stdin;
1	1	1
2	2	1
3	3	1
4	4	1
5	5	1
6	6	1
7	7	1
8	8	1
9	9	1
10	10	1
\.


--
-- Name: django_flatpage_sites_id_seq; Type: SEQUENCE SET; Schema: public; Owner: db_user
--

SELECT pg_catalog.setval('public.django_flatpage_sites_id_seq', 10, true);


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: db_user
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2018-03-11 10:33:19.785025+00
2	auth	0001_initial	2018-03-11 10:33:19.877082+00
3	admin	0001_initial	2018-03-11 10:33:19.912563+00
4	admin	0002_logentry_remove_auto_add	2018-03-11 10:33:19.929448+00
5	contenttypes	0002_remove_content_type_name	2018-03-11 10:33:19.962306+00
6	auth	0002_alter_permission_name_max_length	2018-03-11 10:33:19.974969+00
7	auth	0003_alter_user_email_max_length	2018-03-11 10:33:19.991455+00
8	auth	0004_alter_user_username_opts	2018-03-11 10:33:20.010081+00
9	auth	0005_alter_user_last_login_null	2018-03-11 10:33:20.027255+00
10	auth	0006_require_contenttypes_0002	2018-03-11 10:33:20.030602+00
11	auth	0007_alter_validators_add_error_messages	2018-03-11 10:33:20.044851+00
12	auth	0008_alter_user_username_max_length	2018-03-11 10:33:20.075548+00
13	auth	0009_alter_user_last_name_max_length	2018-03-11 10:33:20.093222+00
14	sites	0001_initial	2018-03-11 10:33:20.104611+00
15	flatpages	0001_initial	2018-03-11 10:33:20.131281+00
16	ginekolog_app	0001_initial	2018-03-11 10:33:20.164231+00
17	ginekolog_app	0002_auto_20180306_0135	2018-03-11 10:33:20.207114+00
18	ginekolog_app	0003_auto_20180306_0144	2018-03-11 10:33:20.225391+00
19	ginekolog_app	0004_auto_20180306_0255	2018-03-11 10:33:20.249546+00
20	ginekolog_app	0005_auto_20180306_0309	2018-03-11 10:33:20.267572+00
21	ginekolog_app	0006_remove_menubaseitems_item_url	2018-03-11 10:33:20.278912+00
22	ginekolog_app	0007_auto_20180306_0419	2018-03-11 10:33:20.292061+00
23	ginekolog_app	0008_auto_20180306_0435	2018-03-11 10:33:20.30692+00
24	ginekolog_app	0009_auto_20180306_0438	2018-03-11 10:33:20.34809+00
25	ginekolog_app	0010_auto_20180306_0443	2018-03-11 10:33:20.383201+00
26	ginekolog_app	0011_auto_20180306_1419	2018-03-11 10:33:20.41498+00
27	ginekolog_app	0012_auto_20180307_1224	2018-03-11 10:33:20.469371+00
28	ginekolog_app	0013_auto_20180307_1248	2018-03-11 10:33:20.491234+00
29	ginekolog_app	0014_auto_20180307_1248	2018-03-11 10:33:20.510658+00
30	ginekolog_app	0015_auto_20180307_1257	2018-03-11 10:33:20.529668+00
31	ginekolog_app	0016_auto_20180307_1324	2018-03-11 10:33:20.582107+00
32	ginekolog_app	0017_auto_20180307_1336	2018-03-11 10:33:20.653582+00
33	ginekolog_app	0018_auto_20180307_1340	2018-03-11 10:33:20.674161+00
34	ginekolog_app	0019_category_description	2018-03-11 10:33:20.692755+00
35	ginekolog_app	0020_auto_20180307_1353	2018-03-11 10:33:20.706379+00
36	ginekolog_app	0021_countingservices_more	2018-03-11 10:33:20.717879+00
37	ginekolog_app	0022_picture_post	2018-03-11 10:33:20.767174+00
38	ginekolog_app	0023_auto_20180308_1217	2018-03-11 10:33:20.862489+00
39	ginekolog_app	0024_auto_20180308_1254	2018-03-11 10:33:20.879731+00
40	ginekolog_app	0025_auto_20180308_1308	2018-03-11 10:33:20.903689+00
41	ginekolog_app	0026_auto_20180308_1333	2018-03-11 10:33:20.952646+00
42	ginekolog_app	0027_footeritems_childs	2018-03-11 10:33:20.978937+00
43	ginekolog_app	0028_auto_20180308_1509	2018-03-11 10:33:21.004563+00
44	ginekolog_app	0029_auto_20180308_1514	2018-03-11 10:33:21.050442+00
45	ginekolog_app	0030_auto_20180309_1148	2018-03-11 10:33:21.101189+00
46	ginekolog_app	0031_auto_20180309_1154	2018-03-11 10:33:21.127712+00
47	ginekolog_app	0032_category_slug	2018-03-11 10:33:21.150982+00
48	ginekolog_app	0033_video_slug	2018-03-11 10:33:21.174072+00
49	ginekolog_app	0034_auto_20180309_2313	2018-03-11 10:33:21.226304+00
50	sessions	0001_initial	2018-03-11 10:33:21.243624+00
51	sites	0002_alter_domain_unique	2018-03-11 10:33:21.259793+00
52	ginekolog_app	0035_picturepage_postpage_videopage	2018-03-14 16:27:28.776741+00
\.


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: db_user
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 52, true);


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: db_user
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
q0wjmp6npox21cxz6tijarzy9pymm0ao	YjYzMjJmMTA3ZDllY2Q1OWVkZjU0MmVjYzIwMzZkODljZjQxOGNiZTp7Il9hdXRoX3VzZXJfaGFzaCI6ImZhZWVjNTE2NTYzMjU2Y2QxMTYyNjMxYzk4Y2Y2ZGIxZWI1Y2NkMGIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2018-03-25 14:04:00.41982+00
ldfp2m9hz86srsf1c2fq7nx0obyldp2t	YjYzMjJmMTA3ZDllY2Q1OWVkZjU0MmVjYzIwMzZkODljZjQxOGNiZTp7Il9hdXRoX3VzZXJfaGFzaCI6ImZhZWVjNTE2NTYzMjU2Y2QxMTYyNjMxYzk4Y2Y2ZGIxZWI1Y2NkMGIiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2018-03-26 07:05:05.28336+00
euwauqqq0hopxv69eav5a1zlbrwtwnpn	NmNjZDUxM2NhNzVjNTFhYWIzZDc1NWYwYmE0ZjUwNDk4NWU4MWIxNDp7Il9hdXRoX3VzZXJfaGFzaCI6ImZhZWVjNTE2NTYzMjU2Y2QxMTYyNjMxYzk4Y2Y2ZGIxZWI1Y2NkMGIiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2018-03-26 19:23:22.184239+00
jpumi3ql3yofm82ovlyh09wdw76mq8f0	NjAxMzA4YTIzMzkwYWJhOWJkOTgyZTdmNzNiNDNmZjdlM2VkNTAzYTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiZmFlZWM1MTY1NjMyNTZjZDExNjI2MzFjOThjZjZkYjFlYjVjY2QwYiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2018-03-28 16:29:05.817362+00
8kejhj49ah2hr614xy4p81ltmctngio7	NWNhZTAxNmE3NjFmMzg1MjY3M2UzMWNiZjE0MzAxYzQzYTBjZDA4NTp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5YTlhNDJhNTY2YmU5ZmRkMzJkYjA2MmFkMTQzMjIyYjkwMmU0ZWU5In0=	2018-04-29 17:08:18.729919+00
ls2m6r1nokfk6dwlwj708g0kr1jxohsi	NWNhZTAxNmE3NjFmMzg1MjY3M2UzMWNiZjE0MzAxYzQzYTBjZDA4NTp7Il9hdXRoX3VzZXJfaWQiOiIyIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI5YTlhNDJhNTY2YmU5ZmRkMzJkYjA2MmFkMTQzMjIyYjkwMmU0ZWU5In0=	2018-04-30 12:05:11.922261+00
\.


--
-- Data for Name: django_site; Type: TABLE DATA; Schema: public; Owner: db_user
--

COPY public.django_site (id, domain, name) FROM stdin;
1	example.com	example.com
2	46.101.109.121	domain_name
\.


--
-- Name: django_site_id_seq; Type: SEQUENCE SET; Schema: public; Owner: db_user
--

SELECT pg_catalog.setval('public.django_site_id_seq', 2, true);


--
-- Data for Name: ginekolog_app_category; Type: TABLE DATA; Schema: public; Owner: db_user
--

COPY public.ginekolog_app_category (id, title, "order", created_at, updated_at, description, slug) FROM stdin;
1	Demo	0	2018-03-12 07:06:13.631959+00	2018-03-12 07:06:13.634075+00	<p>test desc</p>	demo
2	Ginekolojik Əməliyyatlar	0	2018-04-16 10:10:09.36785+00	2018-04-16 10:10:09.399728+00	<p>Ginekolojik Əməliyyatlar</p>	ginekolojik-emeliyyatlar
\.


--
-- Name: ginekolog_app_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: db_user
--

SELECT pg_catalog.setval('public.ginekolog_app_category_id_seq', 2, true);


--
-- Data for Name: ginekolog_app_countingservices; Type: TABLE DATA; Schema: public; Owner: db_user
--

COPY public.ginekolog_app_countingservices (id, title, icon, count, created_at, updated_at, "order", more) FROM stdin;
1	Doğuş və Qeysəriyyə	icons/1523827199_9849694_statistic1.svg	1000	2018-04-15 17:19:59.985416+00	2018-04-15 17:19:59.985436+00	0	t
2	Kesar	icons/1523827230_4631896_statistic2.svg	2018	2018-04-15 17:20:30.464284+00	2018-04-15 17:20:30.464306+00	0	f
3	Laparoskopik Əməliyyat	icons/1523827247_8497384_statistic3.svg	1105	2018-04-15 17:20:47.850334+00	2018-04-15 17:20:47.850364+00	0	f
4	Süni Mayalanma	icons/1523827272_7398596_statistic4.svg	500	2018-04-15 17:21:12.740484+00	2018-04-15 17:21:12.740518+00	0	t
\.


--
-- Name: ginekolog_app_countingservices_id_seq; Type: SEQUENCE SET; Schema: public; Owner: db_user
--

SELECT pg_catalog.setval('public.ginekolog_app_countingservices_id_seq', 4, true);


--
-- Data for Name: ginekolog_app_doctors; Type: TABLE DATA; Schema: public; Owner: db_user
--

COPY public.ginekolog_app_doctors (id, name, surname, profession, phone_number, image, created_at, updated_at, about, slug, status, profile_image, address, email, description, work_grafic) FROM stdin;
2	Natı	Musalı	Uzman Ginekoloq	(+994) 051 650 38 50	doctors/1520792957_9191728_slider-bg.png	2018-03-11 14:27:42.788737+00	2018-03-12 19:23:44.940322+00	<p>burada ətraflı məlumat olacaq</p>	nati_musali	t	doctors/1520792862_7879095_doctor2.png	Baku medical plaza, Babək filialı ,  <br>  Babək prospekti 92N	natimusalı@gmail.com	burada qısa məlumat olacaq	Bazar ertəsi - Şənbə <br>09:00 - 17:00
1	Rəşad	İsmayılzadə	Uzman Ginekoloq	(+994) 051 667 97 30	doctors/1523826849_5293279_Doctor-Background1-1500x630.jpg	2018-03-11 14:21:29.27882+00	2018-04-15 17:14:09.534+00	<p>burada ətraflı məlumat olacaq&nbsp;&nbsp;&nbsp;&nbsp;</p>	resad_i̇smayilzade	t	doctors/1520796707_3906097_doctor-profile031.jpg	Baku medical plaza, Babək filialı ,  <br>  Babək prospekti 92N	rashadismayilzada@gmail.com	burada qısa məlumat olacaq	Bazar ertəsi - Şənbə <br>09:00 - 17:00
\.


--
-- Name: ginekolog_app_doctors_id_seq; Type: SEQUENCE SET; Schema: public; Owner: db_user
--

SELECT pg_catalog.setval('public.ginekolog_app_doctors_id_seq', 2, true);


--
-- Data for Name: ginekolog_app_flatpageimage; Type: TABLE DATA; Schema: public; Owner: db_user
--

COPY public.ginekolog_app_flatpageimage (id, image, created_at, updated_at, flat_page_id) FROM stdin;
1	flatpage/1523887993_0854404_mikroenjeksiyon.jpg	2018-04-16 10:13:13.086175+00	2018-04-16 10:13:13.086212+00	1
\.


--
-- Name: ginekolog_app_flatpageimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: db_user
--

SELECT pg_catalog.setval('public.ginekolog_app_flatpageimage_id_seq', 1, true);


--
-- Data for Name: ginekolog_app_menubaseitems; Type: TABLE DATA; Schema: public; Owner: db_user
--

COPY public.ginekolog_app_menubaseitems (id, title, "order", created_at, updated_at) FROM stdin;
1	Haqqımızda	0	2018-04-15 17:32:01.637884+00	2018-04-15 17:39:02.355832+00
3	Sonsuzluq və Süni Mayalanma	2	2018-04-16 10:18:41.929773+00	2018-04-16 12:00:06.64395+00
4	Ginekolojik Xəstəliklər	3	2018-04-16 11:21:20.805633+00	2018-04-17 03:26:41.601283+00
2	Hamiləlik	1	2018-04-15 17:33:15.729999+00	2018-04-17 03:28:56.30988+00
\.


--
-- Name: ginekolog_app_menubaseitems_id_seq; Type: SEQUENCE SET; Schema: public; Owner: db_user
--

SELECT pg_catalog.setval('public.ginekolog_app_menubaseitems_id_seq', 4, true);


--
-- Data for Name: ginekolog_app_menusubitems; Type: TABLE DATA; Schema: public; Owner: db_user
--

COPY public.ginekolog_app_menusubitems (id, title, url, "order", created_at, updated_at, parent_sub_item_id, base_header_item_id) FROM stdin;
2	Ay Ay hamiləlik	/page/ay-ay-hamilelik/	6	2018-04-15 17:35:37.910526+00	2018-04-17 03:28:56.31185+00	\N	2
9	Uzman Dr. Rəşad İsmayılzadə	/about/resad_i̇smayilzade/	0	2018-04-15 17:38:55.794544+00	2018-04-15 17:38:55.794573+00	\N	1
10	Uzman Dr. Nati Musalı	/about/nati_musali/	1	2018-04-15 17:38:55.795721+00	2018-04-15 17:38:55.795734+00	\N	1
12	Aşılama	/page/asilama/	1	2018-04-16 10:18:41.933589+00	2018-04-16 10:23:17.778615+00	\N	3
3	Daun sindromu skriningi (2li ,3lü və 4lü testlər)	/page/daun-sindromu-skrininqi/	4	2018-04-15 17:35:37.91145+00	2018-04-16 10:48:41.327419+00	\N	2
14	Süni mayalanmada yeniliklər	/#/	3	2018-04-16 10:18:41.935465+00	2018-04-16 10:56:20.928462+00	\N	3
15	ICSI nədir?	/page/icsi/	4	2018-04-16 10:56:20.929917+00	2018-04-16 10:56:56.441355+00	14	3
16	Mikroçip üsulu ilə Süni mayalanma	/page/mikrocip-ile-suni-mayalanma/	5	2018-04-16 10:56:20.930911+00	2018-04-16 10:57:28.364936+00	14	3
21	Myomalar	/page/miomalar/	0	2018-04-16 11:21:20.812838+00	2018-04-16 11:21:20.812862+00	\N	4
4	NST	/page/nst/	0	2018-04-15 17:35:37.912135+00	2018-04-16 11:25:19.389653+00	\N	2
11	Sonsuzluq nədir və müalicəsi nədir	/page/sonsuzluq-nedir/	0	2018-04-16 10:18:41.931795+00	2018-04-16 11:29:08.632275+00	\N	3
13	Süni mayalanma nədir və kimə edilir	/page/suni-mayalanma-nedir/	2	2018-04-16 10:18:41.93467+00	2018-04-16 12:00:06.646497+00	\N	3
7	Hamiləliyin təzyiqli xəstəlikləri(not ready)	/demo/	3	2018-04-15 17:35:37.914172+00	2018-04-17 03:25:11.646842+00	\N	2
1	Hamiləlik təqvimi(not ready)	/demo/	5	2018-04-15 17:33:15.731492+00	2018-04-17 03:25:11.648624+00	\N	2
6	Hamiləliklə və yanaşı xəstəliklər(not ready)	/demo/	7	2018-04-15 17:35:37.913507+00	2018-04-17 03:25:11.652459+00	1	2
8	Hamiləlikdə şəkər(not ready)	/demo/	8	2018-04-15 17:37:54.161143+00	2018-04-17 03:25:11.654158+00	1	2
17	Menstruasiya (aybaşı) pozqunluğu(not ready)	/demo/	0	2018-04-16 11:21:20.80781+00	2018-04-17 03:26:41.602668+00	\N	4
18	Yumurtalığın polikistoz sindromu(not ready)	/demo/	0	2018-04-16 11:21:20.809524+00	2018-04-17 03:26:41.603934+00	\N	4
19	Endometrioz(not ready)	/demo/	0	2018-04-16 11:21:20.810768+00	2018-04-17 03:26:41.605018+00	\N	4
20	Yumurtalıq kistaları(not ready)	/demo/	0	2018-04-16 11:21:20.811809+00	2018-04-17 03:26:41.606095+00	\N	4
22	Uşaqlıq boynu xəstəlikləri(not ready)	/demo/	0	2018-04-16 11:21:20.813898+00	2018-04-17 03:26:41.607228+00	\N	4
23	Erroziya(not ready)	/demo/	0	2018-04-16 11:21:20.814995+00	2018-04-17 03:26:41.608276+00	\N	4
24	Menopauza(not ready)	/demo/	0	2018-04-16 11:21:20.816021+00	2018-04-17 03:26:41.609373+00	\N	4
25	Laparoskopiya(not ready)	/demo/	0	2018-04-16 11:21:20.817004+00	2018-04-17 03:26:41.610394+00	\N	4
26	Histeroskopiya(not ready)	/demo/	0	2018-04-16 11:21:20.818028+00	2018-04-17 03:26:41.611457+00	\N	4
27	Açıq əməliyyatlar(not ready)	/demo/	0	2018-04-16 11:21:20.819019+00	2018-04-17 03:26:41.6125+00	\N	4
28	Uroginekolojik əməliyyatlar(not ready)	/demo/	0	2018-04-16 11:21:20.820067+00	2018-04-17 03:26:41.613532+00	\N	4
5	Hamiləlikdə infeksiyalar	/page/hamiləlikdə-infeksiyalar/	2	2018-04-15 17:35:37.912855+00	2018-04-17 03:27:31.115975+00	\N	2
\.


--
-- Name: ginekolog_app_menusubitems_id_seq; Type: SEQUENCE SET; Schema: public; Owner: db_user
--

SELECT pg_catalog.setval('public.ginekolog_app_menusubitems_id_seq', 28, true);


--
-- Data for Name: ginekolog_app_myflatpage; Type: TABLE DATA; Schema: public; Owner: db_user
--

COPY public.ginekolog_app_myflatpage (flatpage_ptr_id, cover, created_at, updated_at) FROM stdin;
1	flatpage/1523887993_0412073_mikroenjeksiyon.jpg	2018-04-16 10:13:13.046765+00	2018-04-16 10:13:13.0468+00
2	flatpage/1523888799_6900818_hamile-8ay-mkle.gif	2018-04-16 10:26:39.692683+00	2018-04-16 10:27:21.108003+00
3	flatpage/1523889260_8564596_icsi-pipette.jpeg	2018-04-16 10:34:20.860772+00	2018-04-16 10:34:20.8608+00
4	flatpage/1523889769_1334715_daunsindromu.jpg	2018-04-16 10:38:00.496381+00	2018-04-16 10:42:49.134267+00
5	flatpage/1523890104_422435_timthumb.php.jpeg	2018-04-16 10:48:24.425486+00	2018-04-16 10:48:24.425506+00
6	flatpage/1523890324_7805784_timthumb2.jpeg	2018-04-16 10:52:04.787264+00	2018-04-16 10:52:04.787308+00
7	flatpage/1523891586_8864455_mioma.jpg	2018-04-16 11:13:06.890146+00	2018-04-16 11:13:06.890178+00
8	flatpage/1523892300_4523196_nst.jpg	2018-04-16 11:25:00.456291+00	2018-04-16 11:25:00.456312+00
9	flatpage/1523892511_1517446_cover.jpg	2018-04-16 11:28:31.15567+00	2018-04-16 11:28:31.1557+00
10	flatpage/1523894323_2617517_mayalanma.jpeg	2018-04-16 11:58:43.26464+00	2018-04-16 11:58:43.264661+00
\.


--
-- Data for Name: ginekolog_app_picture; Type: TABLE DATA; Schema: public; Owner: db_user
--

COPY public.ginekolog_app_picture (id, title, description, image, created_at, updated_at, category_belongs_to_id, doctor_belongs_to_id, slug) FROM stdin;
1	Bla	<p>bla bla</p>	picture/1520852789_385363_15235874_10154518053057529_2116079911309799972_o.jpg	2018-03-12 07:06:29.387797+00	2018-03-12 07:06:29.390512+00	1	1	bla
\.


--
-- Name: ginekolog_app_picture_id_seq; Type: SEQUENCE SET; Schema: public; Owner: db_user
--

SELECT pg_catalog.setval('public.ginekolog_app_picture_id_seq', 1, true);


--
-- Data for Name: ginekolog_app_picturepage; Type: TABLE DATA; Schema: public; Owner: db_user
--

COPY public.ginekolog_app_picturepage (id, cover, description, created_at, updated_at) FROM stdin;
1	page_covers/1521059432_778367_asas.png	burada sehife haqqinda qısa məlumat olacaq	2018-03-14 16:30:32.780091+00	2018-03-14 16:31:39.187049+00
\.


--
-- Name: ginekolog_app_picturepage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: db_user
--

SELECT pg_catalog.setval('public.ginekolog_app_picturepage_id_seq', 1, true);


--
-- Data for Name: ginekolog_app_post; Type: TABLE DATA; Schema: public; Owner: db_user
--

COPY public.ginekolog_app_post (id, title, content, cover, created_at, updated_at, author_id, category_id, slug) FROM stdin;
2	Aşılama	<p>Aşılama<br />\r\nAşılama ,sonsuzluq zamanı istifadə edilən bir m&uuml;alicə &uuml;suludur. Aşılama zamanı , kişidən alınan sperma<br />\r\nbir kanula vasitəsi ilə uşaqlıq daxilinə inyeksiya edilir ,Yəni kanula ilə uşaqlıq boynundan ke&ccedil;ərək,<br />\r\nsperma uşaqlıq boşluğuna buraxılır. Beləliklə spermanın yumurta h&uuml;ceyrəsini d&ouml;lləmə ehtimalı artır.<br />\r\nAşılamadan &ouml;ncə spermanın təmizlənməsi və x&uuml;susi bəzi proseduralardan ke&ccedil;irilməsi keyfiyyətli<br />\r\nspermaları ayırmağa və aşılamanın tutma ehtimalının artmasına səbəb olur.</p>\r\n\r\n<p>Aşılama etmədən əvvəl ;<br />\r\n- Qadınlarda boruların a&ccedil;ıq olub olmadığını yoxlamaq &uuml;&ccedil;&uuml;n Histerosalpingografiya filmi<br />\r\n&ccedil;əkilməlidir. Aşılama sadəcə uşaqlıq boruları a&ccedil;ıq olan bir xanıma edilə bilər.<br />\r\n- Yumurtalıqların g&uuml;c&uuml;n&uuml; yoxlamaq &uuml;&ccedil;&uuml;n hormon analizləri verilməlidir.&nbsp; Yumurtaliq rezervi zəf olan bir<br />\r\nxəstəyə aşılamadan savayı s&uuml;ni mayalanma etmək daha faydalıdır.<br />\r\nXanımda Prolaktin və TSH m&uuml;tləq yoxlanılmalıdır.<br />\r\nKişidə spermiogramma baxılmalıdır.<br />\r\nAşılama kimə edilir ?<br />\r\n- Sperm sayısı az olan və ya hərəkətli sperm sayısı yetərsiz olan hallarda (kişi faktoru )<br />\r\n- Qadında yumurtlamanın olmaması vəya yetərsiz olması (Ovulatuar Fakt&ouml;r)<br />\r\nBu halda ilk başda yumurtalıq induksiyası edilib, follikul yetişdirilir. Daha sonra m&uuml;vəffəqiyyəti<br />\r\nartırmaq &uuml;&ccedil;&uuml;n aşılama tətbiq edilir<br />\r\n- Səbəbi bilinməyən sonsuzluq<br />\r\n- İmpatensiya problem olan kişilərdə &nbsp;<br />\r\n- Uşaqlıq ağzı ilə əlaqəli problem olan xanımlarda (servİkal factor )<br />\r\nAşılamanın &uuml;st&uuml;nl&uuml;kləri<br />\r\nAşılama vasitəsilə sperma uşaqlıq boşluğuna , boruların girəcəyinin &ccedil;ox yaxınına buraxılır. Beləliklə ,<br />\r\nsperma uşaqlıq yolunun turşu m&uuml;hitindən və uşaqlıq boynu selikli tıxacından qurtulmuş olur. Eyni<br />\r\nzamanda sperma, bəzi xanımlarda sperma əleyhinə yaranabilən antikorlarla təmasdan qurtulur.<br />\r\nAşılama necə edilir ?<br />\r\nAşılama iki c&uuml;r aparıla bilər. Birincisi , xanımda ovulasiya problem yoxdursa,follikulometriya ilə follikulun<br />\r\nb&ouml;y&uuml;məsinə nəzarət edilib, follikul &ccedil;atladıqdan 36 saat sonra aşılama etmək olar .İkinci &uuml;sul, x&uuml;susən<br />\r\nfollikul yetişməsində problem olan xanımlarda , ilk başda yumurtalıq induksiyası ilə follikul b&ouml;y&uuml;d&uuml;l&uuml;r<br />\r\n.Follikulun &ouml;l&ccedil;&uuml;s&uuml; 18 mm a &ccedil;atdıqdan sonra HCG ilə &ccedil;atladılır.HCG vurulduqdan 36 saat sonra aşılama<br />\r\nedilir.<br />\r\nAşılama &ouml;z&uuml; ağrısız prosesdir.X&uuml;susi Kanula uşaqlıq boynundan ke&ccedil;irildikdən sonra, &ouml;ncədən<br />\r\nhazırlanmış sperma bu kanula vasitəsilə uşaqlıq boşluğuna yeridilir.<br />\r\nAşılamanın tutma ehtimalı 20%- dən artıq deyil. Biz klinikamızda bir xəstəyə 2dəfədən artıq aşılama<br />\r\netmirik. 2 dəfə ardıcıl aşılamadan nəticə almayan xəstələrdə digər m&uuml;alicə &uuml;sullarına ke&ccedil;irik</p>	post/1523887813_8880455_mikroenjeksiyon.jpg	2018-04-16 10:10:13.889048+00	2018-04-16 10:10:13.891464+00	1	2	asilama
\.


--
-- Name: ginekolog_app_post_id_seq; Type: SEQUENCE SET; Schema: public; Owner: db_user
--

SELECT pg_catalog.setval('public.ginekolog_app_post_id_seq', 2, true);


--
-- Data for Name: ginekolog_app_postpage; Type: TABLE DATA; Schema: public; Owner: db_user
--

COPY public.ginekolog_app_postpage (id, cover, description, created_at, updated_at) FROM stdin;
1	page_covers/1521059484_9277036_asas.png	burada sehife haqqinda qısa məlumat olacaq	2018-03-14 16:31:24.929082+00	2018-03-14 16:31:24.929108+00
\.


--
-- Name: ginekolog_app_postpage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: db_user
--

SELECT pg_catalog.setval('public.ginekolog_app_postpage_id_seq', 1, true);


--
-- Data for Name: ginekolog_app_services; Type: TABLE DATA; Schema: public; Owner: db_user
--

COPY public.ginekolog_app_services (id, title, content, icon, created_at, updated_at, "order") FROM stdin;
1	Doğuş və Qeysəriyyə	Creating Remarkable Poster Prints Through 4 Color Poster Printing	icons/1523826999_1065845_service1.svg	2018-04-15 17:16:39.107626+00	2018-04-15 17:16:39.107649+00	0
3	Ginekolojik Əməliyyatlar	Sed ut perspiciatis unde iste natus error sit voluptatem accusantium doloremque laudantium.	icons/1523827041_4204164_service3.svg	2018-04-15 17:17:21.42088+00	2018-04-15 17:17:21.420903+00	0
4	Sonsuzluq və Süni Mayalanma	Sed ut perspiciatis unde iste natus error sit voluptatem accusantium doloremque laudantium.	icons/1523827062_2140472_service4.svg	2018-04-15 17:17:42.21443+00	2018-04-15 17:17:42.214452+00	0
2	Ginekolojik Xəstəliklər	Sed ut perspiciatis unde iste natus error sit voluptatem accusantium doloremque laudantium.	icons/1523827129_911065_service2.svg	2018-04-15 17:16:59.043419+00	2018-04-15 17:18:49.911537+00	0
\.


--
-- Name: ginekolog_app_services_id_seq; Type: SEQUENCE SET; Schema: public; Owner: db_user
--

SELECT pg_catalog.setval('public.ginekolog_app_services_id_seq', 4, true);


--
-- Data for Name: ginekolog_app_socialnetworkaddresses; Type: TABLE DATA; Schema: public; Owner: db_user
--

COPY public.ginekolog_app_socialnetworkaddresses (id, title, url, "order", created_at, updated_at, doctor_belongs_to_id) FROM stdin;
1	facebook	https://www.facebook.com/Ginekologresadismayilzade/?hc_ref=ARS25pTNX3L5x-g7M7Ag5-e5sYkhbzOKBfvGd3qTytF-HR_BuVSVetdqcHkX7tlI_oU	0	2018-03-11 14:21:29.283975+00	2018-03-11 14:21:29.283992+00	1
2	instagram	https://www.instagram.com/	1	2018-03-11 14:21:29.286399+00	2018-03-11 14:21:29.286416+00	1
3	twitter	https://twitter.com/	2	2018-03-11 14:21:29.287125+00	2018-03-11 14:21:29.287139+00	1
4	linkedIn	https://www.linkedin.com/	3	2018-03-11 14:21:29.287788+00	2018-03-11 14:21:29.287804+00	1
5	facebook	https://www.facebook.com/natimusali/	0	2018-03-11 14:27:42.792178+00	2018-03-11 14:27:42.792193+00	2
6	instagram	https://www.instagram.com/	1	2018-03-11 14:27:42.793139+00	2018-03-11 14:27:42.793153+00	2
7	twitter	https://twitter.com/	2	2018-03-11 14:27:42.793867+00	2018-03-11 14:27:42.793881+00	2
8	linkedIn	https://www.linkedin.com/	3	2018-03-11 14:27:42.794545+00	2018-03-11 14:27:42.794558+00	2
\.


--
-- Name: ginekolog_app_socialnetworkaddresses_id_seq; Type: SEQUENCE SET; Schema: public; Owner: db_user
--

SELECT pg_catalog.setval('public.ginekolog_app_socialnetworkaddresses_id_seq', 8, true);


--
-- Data for Name: ginekolog_app_video; Type: TABLE DATA; Schema: public; Owner: db_user
--

COPY public.ginekolog_app_video (id, title, description, url, created_at, updated_at, doctor_belongs_to_id, category_belongs_to_id, slug) FROM stdin;
3	demo	<p>demo</p>	https://www.youtube.com/watch?v=n3yoR74dCQE	2018-04-15 17:27:56.952615+00	2018-04-15 17:27:56.954234+00	1	1	demo
4	Okaber Istersenmi	<p>demo&nbsp;&nbsp;&nbsp;&nbsp;</p>	https://www.youtube.com/watch?v=kIyRurQkdZw	2018-04-15 17:30:03.423037+00	2018-04-15 17:30:03.424878+00	1	1	okaber-istersenmi
\.


--
-- Name: ginekolog_app_video_id_seq; Type: SEQUENCE SET; Schema: public; Owner: db_user
--

SELECT pg_catalog.setval('public.ginekolog_app_video_id_seq', 4, true);


--
-- Data for Name: ginekolog_app_videopage; Type: TABLE DATA; Schema: public; Owner: db_user
--

COPY public.ginekolog_app_videopage (id, cover, description, created_at, updated_at) FROM stdin;
1	page_covers/1521059538_705954_asas.png	burada sehife haqqinda qısa məlumat olacaq	2018-03-14 16:32:18.707129+00	2018-03-14 16:32:18.707151+00
\.


--
-- Name: ginekolog_app_videopage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: db_user
--

SELECT pg_catalog.setval('public.ginekolog_app_videopage_id_seq', 1, true);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_flatpage django_flatpage_pkey; Type: CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.django_flatpage
    ADD CONSTRAINT django_flatpage_pkey PRIMARY KEY (id);


--
-- Name: django_flatpage_sites django_flatpage_sites_flatpage_id_site_id_0d29d9d1_uniq; Type: CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.django_flatpage_sites
    ADD CONSTRAINT django_flatpage_sites_flatpage_id_site_id_0d29d9d1_uniq UNIQUE (flatpage_id, site_id);


--
-- Name: django_flatpage_sites django_flatpage_sites_pkey; Type: CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.django_flatpage_sites
    ADD CONSTRAINT django_flatpage_sites_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: django_site django_site_domain_a2e37b91_uniq; Type: CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.django_site
    ADD CONSTRAINT django_site_domain_a2e37b91_uniq UNIQUE (domain);


--
-- Name: django_site django_site_pkey; Type: CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.django_site
    ADD CONSTRAINT django_site_pkey PRIMARY KEY (id);


--
-- Name: ginekolog_app_category ginekolog_app_category_pkey; Type: CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.ginekolog_app_category
    ADD CONSTRAINT ginekolog_app_category_pkey PRIMARY KEY (id);


--
-- Name: ginekolog_app_countingservices ginekolog_app_countingservices_pkey; Type: CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.ginekolog_app_countingservices
    ADD CONSTRAINT ginekolog_app_countingservices_pkey PRIMARY KEY (id);


--
-- Name: ginekolog_app_doctors ginekolog_app_doctors_pkey; Type: CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.ginekolog_app_doctors
    ADD CONSTRAINT ginekolog_app_doctors_pkey PRIMARY KEY (id);


--
-- Name: ginekolog_app_flatpageimage ginekolog_app_flatpageimage_pkey; Type: CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.ginekolog_app_flatpageimage
    ADD CONSTRAINT ginekolog_app_flatpageimage_pkey PRIMARY KEY (id);


--
-- Name: ginekolog_app_menubaseitems ginekolog_app_menubaseitems_pkey; Type: CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.ginekolog_app_menubaseitems
    ADD CONSTRAINT ginekolog_app_menubaseitems_pkey PRIMARY KEY (id);


--
-- Name: ginekolog_app_menusubitems ginekolog_app_menusubitems_pkey; Type: CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.ginekolog_app_menusubitems
    ADD CONSTRAINT ginekolog_app_menusubitems_pkey PRIMARY KEY (id);


--
-- Name: ginekolog_app_myflatpage ginekolog_app_myflatpage_pkey; Type: CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.ginekolog_app_myflatpage
    ADD CONSTRAINT ginekolog_app_myflatpage_pkey PRIMARY KEY (flatpage_ptr_id);


--
-- Name: ginekolog_app_picture ginekolog_app_picture_pkey; Type: CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.ginekolog_app_picture
    ADD CONSTRAINT ginekolog_app_picture_pkey PRIMARY KEY (id);


--
-- Name: ginekolog_app_picturepage ginekolog_app_picturepage_pkey; Type: CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.ginekolog_app_picturepage
    ADD CONSTRAINT ginekolog_app_picturepage_pkey PRIMARY KEY (id);


--
-- Name: ginekolog_app_post ginekolog_app_post_pkey; Type: CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.ginekolog_app_post
    ADD CONSTRAINT ginekolog_app_post_pkey PRIMARY KEY (id);


--
-- Name: ginekolog_app_postpage ginekolog_app_postpage_pkey; Type: CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.ginekolog_app_postpage
    ADD CONSTRAINT ginekolog_app_postpage_pkey PRIMARY KEY (id);


--
-- Name: ginekolog_app_services ginekolog_app_services_pkey; Type: CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.ginekolog_app_services
    ADD CONSTRAINT ginekolog_app_services_pkey PRIMARY KEY (id);


--
-- Name: ginekolog_app_socialnetworkaddresses ginekolog_app_socialnetworkaddresses_pkey; Type: CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.ginekolog_app_socialnetworkaddresses
    ADD CONSTRAINT ginekolog_app_socialnetworkaddresses_pkey PRIMARY KEY (id);


--
-- Name: ginekolog_app_video ginekolog_app_video_pkey; Type: CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.ginekolog_app_video
    ADD CONSTRAINT ginekolog_app_video_pkey PRIMARY KEY (id);


--
-- Name: ginekolog_app_videopage ginekolog_app_videopage_pkey; Type: CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.ginekolog_app_videopage
    ADD CONSTRAINT ginekolog_app_videopage_pkey PRIMARY KEY (id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: db_user
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: db_user
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: db_user
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: db_user
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: public; Owner: db_user
--

CREATE INDEX auth_user_groups_group_id_97559544 ON public.auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: public; Owner: db_user
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON public.auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: public; Owner: db_user
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON public.auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: public; Owner: db_user
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON public.auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: db_user
--

CREATE INDEX auth_user_username_6821ab7c_like ON public.auth_user USING btree (username varchar_pattern_ops);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: db_user
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: db_user
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_flatpage_sites_flatpage_id_078bbc8b; Type: INDEX; Schema: public; Owner: db_user
--

CREATE INDEX django_flatpage_sites_flatpage_id_078bbc8b ON public.django_flatpage_sites USING btree (flatpage_id);


--
-- Name: django_flatpage_sites_site_id_bfd8ea84; Type: INDEX; Schema: public; Owner: db_user
--

CREATE INDEX django_flatpage_sites_site_id_bfd8ea84 ON public.django_flatpage_sites USING btree (site_id);


--
-- Name: django_flatpage_url_41612362; Type: INDEX; Schema: public; Owner: db_user
--

CREATE INDEX django_flatpage_url_41612362 ON public.django_flatpage USING btree (url);


--
-- Name: django_flatpage_url_41612362_like; Type: INDEX; Schema: public; Owner: db_user
--

CREATE INDEX django_flatpage_url_41612362_like ON public.django_flatpage USING btree (url varchar_pattern_ops);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: db_user
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: db_user
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: django_site_domain_a2e37b91_like; Type: INDEX; Schema: public; Owner: db_user
--

CREATE INDEX django_site_domain_a2e37b91_like ON public.django_site USING btree (domain varchar_pattern_ops);


--
-- Name: ginekolog_app_category_slug_e784824e; Type: INDEX; Schema: public; Owner: db_user
--

CREATE INDEX ginekolog_app_category_slug_e784824e ON public.ginekolog_app_category USING btree (slug);


--
-- Name: ginekolog_app_category_slug_e784824e_like; Type: INDEX; Schema: public; Owner: db_user
--

CREATE INDEX ginekolog_app_category_slug_e784824e_like ON public.ginekolog_app_category USING btree (slug varchar_pattern_ops);


--
-- Name: ginekolog_app_doctors_slug_546d03d5; Type: INDEX; Schema: public; Owner: db_user
--

CREATE INDEX ginekolog_app_doctors_slug_546d03d5 ON public.ginekolog_app_doctors USING btree (slug);


--
-- Name: ginekolog_app_doctors_slug_546d03d5_like; Type: INDEX; Schema: public; Owner: db_user
--

CREATE INDEX ginekolog_app_doctors_slug_546d03d5_like ON public.ginekolog_app_doctors USING btree (slug varchar_pattern_ops);


--
-- Name: ginekolog_app_flatpageimage_flat_page_id_79f5ad5c; Type: INDEX; Schema: public; Owner: db_user
--

CREATE INDEX ginekolog_app_flatpageimage_flat_page_id_79f5ad5c ON public.ginekolog_app_flatpageimage USING btree (flat_page_id);


--
-- Name: ginekolog_app_menusubitems_base_header_item_id_d20c8c61; Type: INDEX; Schema: public; Owner: db_user
--

CREATE INDEX ginekolog_app_menusubitems_base_header_item_id_d20c8c61 ON public.ginekolog_app_menusubitems USING btree (base_header_item_id);


--
-- Name: ginekolog_app_menusubitems_parent_sub_item_id_221f3972; Type: INDEX; Schema: public; Owner: db_user
--

CREATE INDEX ginekolog_app_menusubitems_parent_sub_item_id_221f3972 ON public.ginekolog_app_menusubitems USING btree (parent_sub_item_id);


--
-- Name: ginekolog_app_picture_category_belongs_to_id_52519f9a; Type: INDEX; Schema: public; Owner: db_user
--

CREATE INDEX ginekolog_app_picture_category_belongs_to_id_52519f9a ON public.ginekolog_app_picture USING btree (category_belongs_to_id);


--
-- Name: ginekolog_app_picture_doctor_belongs_to_id_6ea0a1e0; Type: INDEX; Schema: public; Owner: db_user
--

CREATE INDEX ginekolog_app_picture_doctor_belongs_to_id_6ea0a1e0 ON public.ginekolog_app_picture USING btree (doctor_belongs_to_id);


--
-- Name: ginekolog_app_picture_slug_36365f53; Type: INDEX; Schema: public; Owner: db_user
--

CREATE INDEX ginekolog_app_picture_slug_36365f53 ON public.ginekolog_app_picture USING btree (slug);


--
-- Name: ginekolog_app_picture_slug_36365f53_like; Type: INDEX; Schema: public; Owner: db_user
--

CREATE INDEX ginekolog_app_picture_slug_36365f53_like ON public.ginekolog_app_picture USING btree (slug varchar_pattern_ops);


--
-- Name: ginekolog_app_post_author_id_a2920f90; Type: INDEX; Schema: public; Owner: db_user
--

CREATE INDEX ginekolog_app_post_author_id_a2920f90 ON public.ginekolog_app_post USING btree (author_id);


--
-- Name: ginekolog_app_post_category_id_46efe20f; Type: INDEX; Schema: public; Owner: db_user
--

CREATE INDEX ginekolog_app_post_category_id_46efe20f ON public.ginekolog_app_post USING btree (category_id);


--
-- Name: ginekolog_app_post_slug_cd95cdb6; Type: INDEX; Schema: public; Owner: db_user
--

CREATE INDEX ginekolog_app_post_slug_cd95cdb6 ON public.ginekolog_app_post USING btree (slug);


--
-- Name: ginekolog_app_post_slug_cd95cdb6_like; Type: INDEX; Schema: public; Owner: db_user
--

CREATE INDEX ginekolog_app_post_slug_cd95cdb6_like ON public.ginekolog_app_post USING btree (slug varchar_pattern_ops);


--
-- Name: ginekolog_app_socialnetwor_doctor_belongs_to_id_946cb307; Type: INDEX; Schema: public; Owner: db_user
--

CREATE INDEX ginekolog_app_socialnetwor_doctor_belongs_to_id_946cb307 ON public.ginekolog_app_socialnetworkaddresses USING btree (doctor_belongs_to_id);


--
-- Name: ginekolog_app_video_category_id_b1eb52bf; Type: INDEX; Schema: public; Owner: db_user
--

CREATE INDEX ginekolog_app_video_category_id_b1eb52bf ON public.ginekolog_app_video USING btree (category_belongs_to_id);


--
-- Name: ginekolog_app_video_doctor_belongs_to_id_2aee68e7; Type: INDEX; Schema: public; Owner: db_user
--

CREATE INDEX ginekolog_app_video_doctor_belongs_to_id_2aee68e7 ON public.ginekolog_app_video USING btree (doctor_belongs_to_id);


--
-- Name: ginekolog_app_video_slug_2cdb1e5f; Type: INDEX; Schema: public; Owner: db_user
--

CREATE INDEX ginekolog_app_video_slug_2cdb1e5f ON public.ginekolog_app_video USING btree (slug);


--
-- Name: ginekolog_app_video_slug_2cdb1e5f_like; Type: INDEX; Schema: public; Owner: db_user
--

CREATE INDEX ginekolog_app_video_slug_2cdb1e5f_like ON public.ginekolog_app_video USING btree (slug varchar_pattern_ops);


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk; Type: FK CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_flatpage_sites django_flatpage_site_flatpage_id_078bbc8b_fk_django_fl; Type: FK CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.django_flatpage_sites
    ADD CONSTRAINT django_flatpage_site_flatpage_id_078bbc8b_fk_django_fl FOREIGN KEY (flatpage_id) REFERENCES public.django_flatpage(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_flatpage_sites django_flatpage_sites_site_id_bfd8ea84_fk_django_site_id; Type: FK CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.django_flatpage_sites
    ADD CONSTRAINT django_flatpage_sites_site_id_bfd8ea84_fk_django_site_id FOREIGN KEY (site_id) REFERENCES public.django_site(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ginekolog_app_flatpageimage ginekolog_app_flatpa_flat_page_id_79f5ad5c_fk_ginekolog; Type: FK CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.ginekolog_app_flatpageimage
    ADD CONSTRAINT ginekolog_app_flatpa_flat_page_id_79f5ad5c_fk_ginekolog FOREIGN KEY (flat_page_id) REFERENCES public.ginekolog_app_myflatpage(flatpage_ptr_id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ginekolog_app_menusubitems ginekolog_app_menusu_base_header_item_id_d20c8c61_fk_ginekolog; Type: FK CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.ginekolog_app_menusubitems
    ADD CONSTRAINT ginekolog_app_menusu_base_header_item_id_d20c8c61_fk_ginekolog FOREIGN KEY (base_header_item_id) REFERENCES public.ginekolog_app_menubaseitems(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ginekolog_app_menusubitems ginekolog_app_menusu_parent_sub_item_id_221f3972_fk_ginekolog; Type: FK CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.ginekolog_app_menusubitems
    ADD CONSTRAINT ginekolog_app_menusu_parent_sub_item_id_221f3972_fk_ginekolog FOREIGN KEY (parent_sub_item_id) REFERENCES public.ginekolog_app_menusubitems(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ginekolog_app_myflatpage ginekolog_app_myflat_flatpage_ptr_id_041a26af_fk_django_fl; Type: FK CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.ginekolog_app_myflatpage
    ADD CONSTRAINT ginekolog_app_myflat_flatpage_ptr_id_041a26af_fk_django_fl FOREIGN KEY (flatpage_ptr_id) REFERENCES public.django_flatpage(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ginekolog_app_picture ginekolog_app_pictur_category_belongs_to__52519f9a_fk_ginekolog; Type: FK CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.ginekolog_app_picture
    ADD CONSTRAINT ginekolog_app_pictur_category_belongs_to__52519f9a_fk_ginekolog FOREIGN KEY (category_belongs_to_id) REFERENCES public.ginekolog_app_category(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ginekolog_app_picture ginekolog_app_pictur_doctor_belongs_to_id_6ea0a1e0_fk_ginekolog; Type: FK CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.ginekolog_app_picture
    ADD CONSTRAINT ginekolog_app_pictur_doctor_belongs_to_id_6ea0a1e0_fk_ginekolog FOREIGN KEY (doctor_belongs_to_id) REFERENCES public.ginekolog_app_doctors(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ginekolog_app_post ginekolog_app_post_author_id_a2920f90_fk_ginekolog; Type: FK CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.ginekolog_app_post
    ADD CONSTRAINT ginekolog_app_post_author_id_a2920f90_fk_ginekolog FOREIGN KEY (author_id) REFERENCES public.ginekolog_app_doctors(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ginekolog_app_post ginekolog_app_post_category_id_46efe20f_fk_ginekolog; Type: FK CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.ginekolog_app_post
    ADD CONSTRAINT ginekolog_app_post_category_id_46efe20f_fk_ginekolog FOREIGN KEY (category_id) REFERENCES public.ginekolog_app_category(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ginekolog_app_socialnetworkaddresses ginekolog_app_social_doctor_belongs_to_id_946cb307_fk_ginekolog; Type: FK CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.ginekolog_app_socialnetworkaddresses
    ADD CONSTRAINT ginekolog_app_social_doctor_belongs_to_id_946cb307_fk_ginekolog FOREIGN KEY (doctor_belongs_to_id) REFERENCES public.ginekolog_app_doctors(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ginekolog_app_video ginekolog_app_video_category_belongs_to__f319c1de_fk_ginekolog; Type: FK CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.ginekolog_app_video
    ADD CONSTRAINT ginekolog_app_video_category_belongs_to__f319c1de_fk_ginekolog FOREIGN KEY (category_belongs_to_id) REFERENCES public.ginekolog_app_category(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ginekolog_app_video ginekolog_app_video_doctor_belongs_to_id_2aee68e7_fk_ginekolog; Type: FK CONSTRAINT; Schema: public; Owner: db_user
--

ALTER TABLE ONLY public.ginekolog_app_video
    ADD CONSTRAINT ginekolog_app_video_doctor_belongs_to_id_2aee68e7_fk_ginekolog FOREIGN KEY (doctor_belongs_to_id) REFERENCES public.ginekolog_app_doctors(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

\connect postgres

SET default_transaction_read_only = off;

--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.8
-- Dumped by pg_dump version 9.6.8

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: DATABASE postgres; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON DATABASE postgres IS 'default administrative connection database';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- PostgreSQL database dump complete
--

\connect template1

SET default_transaction_read_only = off;

--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.8
-- Dumped by pg_dump version 9.6.8

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: DATABASE template1; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON DATABASE template1 IS 'default template for new databases';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database cluster dump complete
--

